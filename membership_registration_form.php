<link rel="stylesheet" type="text/css" href="assets/form-wizard/jquery.steps.css" />

<?php
include('common.php');
include_once '../apporioconfig/start_up.php';

$page_data1 = $db->db_get_array("SELECT * FROM ".TABLE_MEMBERSHIP." WHERE membership_status=1");        

$table = "membership_registration_form";


if($_SERVER['REQUEST_METHOD'] == "POST") {
    date_default_timezone_set('Asia/Kolkata');
    $timestamp = time();
    $create_date = date("d F Y-g:i A", $timestamp);
     $password = md5($_POST['password']);
    $arr = array(
      'membership_id'        => $_POST['membership_id'],
      'first_name'           => $_POST['first_name'],
      'last_name'            => $_POST['last_name'],
      'address'              => $_POST['address'],
      'place_of_birth'       => $_POST['place_of_birth'],
      'date_of_birth'        => $_POST['date_of_birth'],
      'nationality'          => $_POST['nationality'],
      'phone'                => $_POST['phone'],
      'pocket_portable'      => $_POST['pocket_portable'],
      'email'                => $_POST['email'],
      'fax'                  => $_POST['fax'],
      'family_sitution'      => $_POST['family_sitution'],
      'education'            => $_POST['education'],
      'job'            	     => $_POST['job'],
      'first_activity'       => $_POST['first_activity'],
      'second_activity'      => $_POST['second_activity'],
      'quantity'             => $_POST['quantity'],
      'amount'               => $_POST['amount'],
      'payment_type'         => $_POST['payment_type'],
      'donation'             => $_POST['donation'],
      'date_of_receipt'      => $create_date,
      'adherent'             => $_POST['adherent'],
      'received'             => $_POST['received'],
      'password'             => $password
    );
     
    $insert_id =  $db->insert($table,$arr,true);

    $db->redirect("thank-you");

  }

?>
<style>

label{font-weight:600 !important;}

.radion input{ float:left;}

.radion input[type=radio] {
    border: 0px;
    width: 2%;
    height: 1.2em;
}

#finish{ display:none !important;}


</style>

<article id="" class="">
	<div class="entry-content">
		<div class="top_background" style="border-style: none;">
			<div class="container">
				<div class=""  style="font-family:'Montserrat';font-weight:700;">
					<p class="sd-text-bg-white">	Membership Registration Form</p>
				</div>
			</div>
		</div>
	</div>
</article>
<div class="container">
	<div style="margin-top: 50px;"></div>
	<div class="wpb_column vc_column_container vc_col-sm-12">
		<div class="vc_column-inner ">
			<div class="wpb_wrapper">

			<script type="text/javascript">
function formvalidation() {
       var password = document.getElementById("password").value;
        var confirmPassword = document.getElementById("cfmpassword").value;
        if (password != confirmPassword) {
            alert("Password And Confirm Password do not match.");
            return false;
        }

 if( document.getElementById('membership_id').value == "" ) {
   alert('Please Select Membership Type');
   document.getElementById('membership_id').focus();
   return false;
  }
  if( document.getElementById('first_name').value == "" ) {
   alert('Please Enter First name');
   document.getElementById('first_name').focus();
   return false;
  }
  if( document.getElementById('last_name').value == "" ) {
   alert('Please Enter Last Name');
   document.getElementById('last_name').focus();
   return false;
  }
  if( document.getElementById('address').value == "" ) {
   alert('Please Enter Address');
   document.getElementById('address').focus();
   return false;
  }
 if( document.getElementById('place_of_birth').value == "" ) {
   alert('Please Select Place of Birth');
   document.getElementById('place_of_birth').focus();
   return false;
  }
 if( document.getElementById('date_of_birth').value == "" ) {
   alert('Please Select Date of Birth');
   document.getElementById('date_of_birth').focus();
   return false;
  }
 if( document.getElementById('nationality').value == "" ) {
   alert('Please Enter Nationality');
   document.getElementById('nationality').focus();
   return false;
  }
 if( document.getElementById('phone').value == "" ) {
   alert('Please Enter Phone Number');
   document.getElementById('phone').focus();
   return false;
  }
 if( document.getElementById('pocket_portable').value == "" ) {
   alert('Please Enter Pocket Portable');
   document.getElementById('pocket_portable').focus();
   return false;
  }
 if( document.getElementById('email').value == "" ) {
   alert('Please Enter Email');
   document.getElementById('email').focus();
   return false;
  }
 //if( document.getElementById('fax').value == "" ) {
   //alert('Please Enter Fax Number');
   //document.getElementById('fax').focus();
   //return false;
  //}
 

 if( document.getElementById('family_sitution1').checked == false &&  document.getElementById('family_sitution2').checked == false) {
   alert('Please Select Family Situation');
   document.getElementById('family_sitution1').focus();
   return false;
  }
if( document.getElementById('education1').checked == false && document.getElementById('education2').checked == false && document.getElementById('education3').checked == false && document.getElementById('education4').checked == false) {
  alert('Please Select Education');
  document.getElementById('education1').focus();
  return false;
 }

 if( document.getElementById('job').value == "" ) {
   alert('Please Enter Job');
   document.getElementById('job').focus();
   return false;
  }
 //if( document.getElementById('first_activity').value == "" ) {
   //alert('Please Enter Associate Mandate');
   //document.getElementById('first_activity').focus();
   //return false;
  //}
//if( document.getElementById('second_activity').value == "" ) {
  // alert('Please Enter Electoral Mandate');
   //document.getElementById('second_activity').focus();
   //return false;
  //}

 if( document.getElementById('quantity').value == "" ) {
   alert('Please Enter Quantity');
   document.getElementById('quantity').focus();
   return false;
  }


  if( document.getElementById('amount1').checked == false &&  document.getElementById('amount2').checked == false &&  document.getElementById('amount3').checked == false) {
    alert('Please Select Amount');
    document.getElementById('amount1').focus();
    return false;
   }



  if( document.getElementById('payment_type1').checked == false &&  document.getElementById('payment_type2').checked == false &&  document.getElementById('payment_type3').checked == false) {
    alert('Please Select Payment Type');
    document.getElementById('payment_type1').focus();
    return false;
   }

 if( document.getElementById('adherent').value == "" ) {
   alert('Please Enter Adherent');
   document.getElementById('adherent').focus();
   return false;
  }
 if( document.getElementById('received').value == "" ) {
   alert('Please Enter Received');
   document.getElementById('received').focus();
   return false;
  }

  if( document.getElementById('email').value == "" ) {
   alert('Please Enter Email');
   document.getElementById('email').focus();
   return false;
  }
  if( document.getElementById('password').value == "" ) {
   alert('Please Enter Password');
   document.getElementById('password').focus();
   return false;
  }
if( document.getElementById('cfmpassword').value == "" ) {
   alert('Please Enter Confirm Password');
   document.getElementById('cfmpassword').focus();
   return false;
  }

  return true;
}
</script>


			<div class="row">
                    <div class="">

                        <div class="panel panel-default">
                            
                            <div class="panel-body"> 
                                <form id="wizard-validation-form" action="" method="post" class="wpcf7-form" novalidate="novalidate" onSubmit="return formvalidation()">
                                    <div>
                                        <h3>Step </h3>
                                        <section>
                                         
                                         <div class="col-md-6 mb10">
						<label>choose plan </label>
						<select class="wpcf7-text required form-control" name="membership_id" id="membership_id">
<option  value="">---Please select your Plan---</option>
<?php foreach($page_data1 as $page_details2){ ?>
						   
						   	<option value="<?php echo $page_details2['membership_id'];?>"><?php echo $page_details2['membership_name'];?></option>
						   	<?php } ?>
						</select>
					</div>
					
					<div class="col-md-6 mb10">
						<label>E-Mail</label>
						<input type="email" name="email" id="email" value="" size="40" class="wpcf7-text required email form-control" aria-required="true" aria-invalid="false" placeholder="E-Mail" />
					</div>
					
					 <div class="col-md-6 mb10">
						<label>First Name</label>
						<input type="text" id="first_name" name="first_name" value="" size="40" class="wpcf7-text required form-control" aria-required="true" aria-invalid="false" placeholder="First Name" />
					</div>

					<div class="col-md-6 mb10">
						<label>Last Name</label>
						<input type="text" id="last_name" name="last_name" value="" size="40" class="wpcf7-text required form-control" aria-required="true" aria-invalid="false" placeholder="Last Name" />
					</div>
					
					<div class="col-md-6 mb10">
						<label>Address</label>
						<input type="text" id="address" name="address" value="" size="40" class="wpcf7-text required form-control" aria-required="true" aria-invalid="false" placeholder="Address" />
					</div>
					
					<div class="col-md-6 mb10">
						<label>Place Of Birth</label>
						<input type="text" name="place_of_birth" id="place_of_birth" value="" size="40" class="wpcf7-text required form-control" aria-required="true" aria-invalid="false" placeholder="Place Of Birth" />
					</div>

					<div class="col-md-6 mb10">
						<label>Date</label>
						<input type="date" name="date_of_birth" id="date_of_birth" value="" size="40" class="wpcf7-text required form-control start_time" aria-required="true" id="event_start_date" aria-invalid="false" placeholder="Date" />

					</div>
					
					<div class="col-md-6 mb10">
						<label>Nationality</label>
						<input type="text" name="nationality" id="nationality" value="" size="40" class="wpcf7-text required form-control" aria-required="true" aria-invalid="false" placeholder="Nationality" />
					</div>

					<div class="col-md-6 mb10">
						<label>Phone</label>
						<input type="text" name="phone" id="phone" value="" size="40" class="wpcf7-text required form-control" aria-required="true" aria-invalid="false" placeholder="Phone" />
					</div>
					
					<div class="col-md-6 mb10">
						<label>Password</label>
						<input type="password" name="password" id="password" value="" size="40" class="wpcf7-text required form-control" aria-required="true" aria-invalid="false" placeholder="Password" />
					</div>
					
					<div class="col-md-6 mb10">
						<label>Confirm Password</label>
						<input type="password" name="cfmpassword" id="cfmpassword" value="" size="40" class="wpcf7-text required form-control" aria-required="true" aria-invalid="false" placeholder=" Confirm Password" />
					</div>
					
					<div class="col-md-6 mb10">
						<label>Fax</label>
						<input type="text" name="fax" id='fax' value="" size="40" class="wpcf7-text" aria-required="true" aria-invalid="false" placeholder="Fax" />
					</div>
					
					
					
					
					
                                            
                                            
                                            
                                          
                                        </section>
                                        <h3>Step</h3>
                                        <section>
                                            
                                            <div class="col-md-6 mb10">
						<label>Mobile Phone</label>
						<input type="text" name="pocket_portable" id="pocket_portable" value="" size="40" class="wpcf7-text required form-control" aria-required="true" aria-invalid="false" placeholder="Mobile Phone" />
					    </div>
					    
					    <div class="col-md-6 mb10">
						<label>Job</label>
						<input type="text" name="job"  id="job" value="" size="40" class="wpcf7-text required form-control" aria-required="true" aria-invalid="false" placeholder="Job" />
					</div>
					
					<div class="col-md-6 col-xs-12 mb10">
            <br>
						<label>YOUR ACTIVITIES IN OTHER INSTITUTIONS (IF ANY)<br>Associate Mandate (if available)</label>
						<input type="text" name="first_activity" id="first_activity" value="" size="40" class="wpcf7-text form-control" aria-required="true" aria-invalid="false" placeholder="Associate Mandate (if available)" />
					</div>
					
					<div class="col-md-6 col-xs-12 mb10">
            <br>
						<label>YOUR ACTIVITIES IN THE POLITICAL PARTY (IF ANY)<br>Electoral Mandate (and there)</label>
						<input type="text" name="second_activity" id="second_activity" value="" size="40" class="wpcf7-text form-control" aria-required="true" aria-invalid="false" placeholder="Electoral Mandate (and there)" />
					</div>
					    
					    <div class="col-md-12 col-xs-12 mb10">
					  <label style="display:block;">Family Situation</label>
					  <div class="radion"><input type="radio" name="family_sitution" id="family_sitution1" value="1" class="">Single</div>
					  <div class="radion"><input type="radio" name="family_sitution" id="family_sitution2" value="2" class="">Married</div>
					</div>
					
					<div class="col-md-12 col-xs-12 mb10">
					  <label style="display:block;">Education</label>
					  <div class="radion"><input type="radio" id="education1" name="education" value="1" class="">First Primary</div>
					  <div class="radion"><input type="radio" id="education2" name="education" value="2" class="">Middle School</div>
					  <div class="radion"><input type="radio" id="education3" name="education" value="3" class="">High School</div>
					  <div class="radion"><input type="radio" id="education4" name="education" value="4" class="">University</div>
					</div>
					
					
					
					
					
					
					
					

                                        </section>
                                        
                                        
                                        
                                        
                                        <h3>Step</h3>
                                        <section>
                                        
                                            <div class="col-md-12 mb10">
						<label>Quantity</label>
						<input type="text" name="quantity" id="quantity" value="" size="40" class="wpcf7-text required form-control" aria-required="true" aria-invalid="false" placeholder="Quantity" />
					</div>
					
					
					<div class="col-md-12 mb10">
						<label>Donation</label>
						<input type="text" name="donation"  value="" size="40" class="wpcf7-text required form-control" aria-required="true" aria-invalid="false" placeholder="Donation" />
					</div>
					
					<div class="col-md-12 col-xs-12 mb10">
						<label>Amount</label><br>
							<div class="radion"><input type="radio" name="amount" id="amount1" value="1" class="">STUDENT 10 €</div>
				  		<div class="radion"><input type="radio" name="amount" id="amount2" value="2" class="">Normal 20 €</div>
				  		<div class="radion"><input type="radio" name="amount" id="amount3" value="3" class="">COMPANY / ASSOCIATION 50 €</div>
					</div>
					
					<div class="col-md-12 col-xs-12 mb10">
						<label>Payment Type</label><br>
						<div class="radion"><input type="radio" name="payment_type" id="payment_type1" value="1" class="">Cash</div>
				  		<div class="radion"><input type="radio" name="payment_type"  id="payment_type2" value="2" class="">check</div>
				  		<div class="radion"><input type="radio" name="payment_type" id="payment_type3"  value="3" class="">Transfer</div>
				  		<div class="radion"><input type="radio" name="payment_type" id="payment_type4"  value="4" class="">Credit Card</div>
					</div>
					
					
					<div class="col-md-6 mb10">
						<p style="margin-bottom: 50px; margin-top: 20px;">
						<input type="submit" value="Submit" class="wpcf7-form-control wpcf7-submit" />
						</p>
					</div>
					
					
					
					
                                        </section>
                                        
                                        
                                        
                                        
                                    </div>
                                </form> 
                            </div>  <!-- End panel-body -->
                        </div> <!-- End panel -->

                    </div> <!-- end col -->

                </div>
			</div>
		</div>
	</div>
</div>



<script type="text/javascript" src="admin/js/wickedpicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="admin/css/wickedpicker.min.css">

<script>
   $(function() {
       $('.start_time').datepicker({

         format: "yyyy-mm-dd"

     });
   });
   $(function() {
       $('.end_date').datepicker({

        format: "yyyy-mm-dd" });
   });

    $('.start').wickedpicker();

     $('.end').wickedpicker();

$('.time').bootstrapMaterialDatePicker({ date: false });

</script>


<script src="assets/form-wizard/jquery.steps.min.js" type="text/javascript"></script>

<script type="text/javascript" src="assets/jquery.validate/jquery.validate.min.js"></script>

<script src="assets/form-wizard/wizard-init.js" type="text/javascript"></script>