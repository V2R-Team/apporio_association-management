

	<div class="sd-page-top clearfix sd-display-none">
		<div class="container"> 
			<!-- page title -->
							<h1>
					CONTACT US				</h1>
				
		</div>
		<!-- container -->	
	</div>
	<!-- sd-page-top -->

	<article id="post-355" class="sd-full-width clearfix post-355 page type-page status-publish hentry"> 
		<div class="entry-content">
			<div class="vc_row wpb_row vc_row-fluid" style="border-style: none;"><div class="container"><div class="sd-centered-wrapper"><div class="wpb_column vc_column_container vc_col-sm-12"><div class="vc_column-inner "><div class="wpb_wrapper"><div class="ult-spacer spacer-58946288c4f77" data-id="58946288c4f77" data-height="110" data-height-mobile="50" data-height-tab="" data-height-tab-portrait="" data-height-mobile-landscape="" style="clear:both;display:block;"></div><div id="ultimate-heading-700958946288c50e6" class="uvc-heading ult-adjust-bottom-margin ultimate-heading-700958946288c50e6 uvc-7897 " data-hspacer="no_spacer"  data-halign="left" style="text-align:left"><div class="uvc-heading-spacer no_spacer" style="top"></div><div class="uvc-sub-heading ult-responsive"  data-ultimate-target='.uvc-heading.ultimate-heading-700958946288c50e6 .uvc-sub-heading '  data-responsive-json-new='{"font-size":"desktop:18px;","line-height":"desktop:18px;"}'  style="font-family:'Montserrat';font-weight:700;"></p>
<p class="sd-text-bg-white">CONTACT US</p>
<p></div></div><div class="ult-spacer spacer-58946288c51c3" data-id="58946288c51c3" data-height="110" data-height-mobile="50" data-height-tab="" data-height-tab-portrait="" data-height-mobile-landscape="" style="clear:both;display:block;"></div></div></div></div></div></div></div><!-- Row Backgrounds --><div class="upb_bg_img" data-ultimate-bg="url(http://skattf.skatdesign.netdna-cdn.com/wp-content/uploads/sites/15/2015/07/campaign-title-bg.jpg)" data-image-id="33|http://helpinghands1.skat.tf/wp-content/uploads/sites/15/2015/07/campaign-title-bg.jpg" data-ultimate-bg-style="vcpb-default" data-bg-img-repeat="no-repeat" data-bg-img-size="cover" data-bg-img-position="" data-parallx_sense="" data-bg-override="" data-bg_img_attach="scroll" data-upb-overlay-color="" data-upb-bg-animation="" data-fadeout="" data-bg-animation="left-animation" data-bg-animation-type="h" data-animation-repeat="" data-fadeout-percentage="" data-parallax-content="" data-parallax-content-sense="" data-row-effect-mobile-disable="true" data-img-parallax-mobile-disable="true" data-rtl="false"  data-custom-vc-row=""  data-vc="5.0.1"  data-is_old_vc=""  data-theme-support=""   data-overlay="false" data-overlay-color="" data-overlay-pattern="" data-overlay-pattern-opacity="" data-overlay-pattern-size=""    ></div><div class="vc_row wpb_row vc_row-fluid" style="border-style: none;"><div class="container"><div class="sd-centered-wrapper"><div class="wpb_column vc_column_container vc_col-sm-12"><div class="vc_column-inner "><div class="wpb_wrapper"><div class="ult-spacer spacer-58946288c798f" data-id="58946288c798f" data-height="70" data-height-mobile="70" data-height-tab="70" data-height-tab-portrait="" data-height-mobile-landscape="" style="clear:both;display:block;"></div><div class="vc_row wpb_row vc_inner vc_row-fluid"><div class="wpb_column vc_column_container vc_col-sm-8"><div class="vc_column-inner "><div class="wpb_wrapper"><div id="ultimate-heading-90658946288c8bad" class="uvc-heading ult-adjust-bottom-margin ultimate-heading-90658946288c8bad uvc-1195 " data-hspacer="no_spacer"  data-halign="left" style="text-align:left"><div class="uvc-heading-spacer no_spacer" style="top"></div><div class="uvc-main-heading ult-responsive"  data-ultimate-target='.uvc-heading.ultimate-heading-90658946288c8bad h3'  data-responsive-json-new='{"font-size":"desktop:24px;","line-height":"desktop:30px;"}' ><h3 style="font-family:'Montserrat';font-weight:700;margin-bottom:15px;">HAVE AN IDEA TO HELP? - CONTACT US</h3></div><div class="uvc-sub-heading ult-responsive"  data-ultimate-target='.uvc-heading.ultimate-heading-90658946288c8bad .uvc-sub-heading '  data-responsive-json-new='{"font-size":"desktop:21px;","line-height":"desktop:30px;"}'  style="font-family:'Karla';font-weight:normal;color:#29af8a;margin-bottom:25px;"></p>
<p>Pellentesque mollis eros quis massa interdum porta et vel nisi. Duis vel viverra quamam molesvulputate femy contenteu.</p>
<p></div></div><div role="form" class="wpcf7" id="wpcf7-f357-p355-o1" lang="en-US" dir="ltr">
<div class="screen-reader-response"></div>
<form action="http://helpinghands1.skat.tf/contact-us/#wpcf7-f357-p355-o1" method="post" class="wpcf7-form" novalidate="novalidate">
<div style="display: none;">
<input type="hidden" name="_wpcf7" value="357" />
<input type="hidden" name="_wpcf7_version" value="4.6.1" />
<input type="hidden" name="_wpcf7_locale" value="en_US" />
<input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f357-p355-o1" />
<input type="hidden" name="_wpnonce" value="2a108b8ef3" />
</div>
<p><span class="wpcf7-form-control-wrap your-name"><input type="text" name="your-name" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" placeholder="Name" /></span> </p>
<p><span class="wpcf7-form-control-wrap your-email"><input type="email" name="your-email" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email" aria-required="true" aria-invalid="false" placeholder="E-Mail" /></span> </p>
<p><span class="wpcf7-form-control-wrap your-message"><textarea name="your-message" cols="40" rows="10" class="wpcf7-form-control wpcf7-textarea" aria-invalid="false" placeholder="Message"></textarea></span> </p>
<p><span class="wpcf7-form-control-wrap captcha-897"><input type="text" name="captcha-897" value="" size="40" class="wpcf7-form-control wpcf7-captchar" autocomplete="off" aria-invalid="false" placeholder="Captcha" /></span></p>
<p><input type="hidden" name="_wpcf7_captcha_challenge_captcha-897" value="4100384546" /><img class="wpcf7-form-control wpcf7-captchac wpcf7-captcha-captcha-897" width="60" height="20" alt="captcha" src="../wp-content/uploads/sites/15/wpcf7_captcha/4100384546.png" /></p>
<p><input type="submit" value="Submit" class="wpcf7-form-control wpcf7-submit" /></p>
<div class="wpcf7-response-output wpcf7-display-none"></div></form></div><div id="ultimate-heading-427858946288cbe6c" class="uvc-heading ult-adjust-bottom-margin ultimate-heading-427858946288cbe6c uvc-4955 " data-hspacer="no_spacer"  data-halign="left" style="text-align:left"><div class="uvc-heading-spacer no_spacer" style="top"></div><div class="uvc-main-heading ult-responsive"  data-ultimate-target='.uvc-heading.ultimate-heading-427858946288cbe6c h3'  data-responsive-json-new='{"font-size":"desktop:24px;","line-height":"desktop:30px;"}' ><h3 style="font-family:'Montserrat';font-weight:700;margin-top:50px;margin-bottom:30px;">FIND US HERE</h3></div><div class="uvc-sub-heading ult-responsive"  data-ultimate-target='.uvc-heading.ultimate-heading-427858946288cbe6c .uvc-sub-heading '  data-responsive-json-new='{"font-size":"","line-height":""}'  style="font-weight:normal;"><br />
</div></div><div id='wrap_map_58946288cc0d9' class='ultimate-map-wrapper ult-adjust-bottom-margin ' style=' height:350px;'><div id='map_58946288cc0d9' data-map_override='0' class='ultimate_google_map wpb_content_element ' style='width:100%;height:350px;'></div></div><script type='text/javascript'>
			(function($) {
  			'use strict';
			var map_map_58946288cc0d9 = null;
			var coordinate_map_58946288cc0d9;
			var isDraggable = $(document).width() > 641 ? true : true;
			try
			{
				var map_map_58946288cc0d9 = null;
				var coordinate_map_58946288cc0d9;
				coordinate_map_58946288cc0d9=new google.maps.LatLng(40.71278, -74.00594);
				var mapOptions=
				{
					zoom: 14,
					center: coordinate_map_58946288cc0d9,
					scaleControl: true,
					streetViewControl: false,
					mapTypeControl: false,
					panControl: false,
					zoomControl: false,
					scrollwheel: false,
					draggable: isDraggable,
					zoomControlOptions: {
					  style: google.maps.ZoomControlStyle.SMALL
					}, mapTypeControlOptions: {
					  		mapTypeIds: [google.maps.MapTypeId.ROADMAP, 'map_style']
						}};var styles = [{"featureType":"landscape.man_made","elementType":"geometry","stylers":[{"color":"#f7f1df"}]},{"featureType":"landscape.natural","elementType":"geometry","stylers":[{"color":"#d0e3b4"}]},{"featureType":"landscape.natural.terrain","elementType":"geometry","stylers":[{"visibility":"off"}]},{"featureType":"poi","elementType":"labels","stylers":[{"visibility":"off"}]},{"featureType":"poi.business","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"poi.medical","elementType":"geometry","stylers":[{"color":"#fbd3da"}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#bde6ab"}]},{"featureType":"road","elementType":"geometry.stroke","stylers":[{"visibility":"off"}]},{"featureType":"road","elementType":"labels","stylers":[{"visibility":"off"}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#ffe15f"}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#efd151"}]},{"featureType":"road.arterial","elementType":"geometry.fill","stylers":[{"color":"#ffffff"}]},{"featureType":"road.local","elementType":"geometry.fill","stylers":[{"color":"black"}]},{"featureType":"transit.station.airport","elementType":"geometry.fill","stylers":[{"color":"#cfb2db"}]},{"featureType":"water","elementType":"geometry","stylers":[{"color":"#a2daf2"}]}];
						var styledMap = new google.maps.StyledMapType(styles,
					    	{name: "Styled Map"});var map_map_58946288cc0d9 = new google.maps.Map(document.getElementById('map_58946288cc0d9'),mapOptions);map_map_58946288cc0d9.mapTypes.set('map_style', styledMap);
 							 map_map_58946288cc0d9.setMapTypeId('map_style');
						var x = 'off';
						var marker_map_58946288cc0d9 = new google.maps.Marker({
						position: new google.maps.LatLng(40.71278, -74.00594),
						animation:  google.maps.Animation.DROP,
						map: map_map_58946288cc0d9,
						icon: 'http://skattf.skatdesign.netdna-cdn.com/wp-content/plugins/Ultimate_VC_Addons/modules/../assets/img/icon-marker-pink.png'
					});
					google.maps.event.addListener(marker_map_58946288cc0d9, 'click', toggleBounce);var infowindow = new google.maps.InfoWindow();
							infowindow.setContent('<div class="map_info_text" style=\'color:#000;\'></p> <p>Helping Hands</p> <p></div>');infowindow.open(map_map_58946288cc0d9,marker_map_58946288cc0d9);google.maps.event.addListener(marker_map_58946288cc0d9, 'click', function() {
								infowindow.open(map_map_58946288cc0d9,marker_map_58946288cc0d9);
						  	});}
			catch(e){};
			jQuery(document).ready(function($){
				google.maps.event.trigger(map_map_58946288cc0d9, 'resize');
				$(window).resize(function(){
					google.maps.event.trigger(map_map_58946288cc0d9, 'resize');
					if(map_map_58946288cc0d9!=null)
						map_map_58946288cc0d9.setCenter(coordinate_map_58946288cc0d9);
				});
				$('.ui-tabs').bind('tabsactivate', function(event, ui) {
				   if($(this).find('.ultimate-map-wrapper').length > 0)
					{
						setTimeout(function(){
							$(window).trigger('resize');
						},200);
					}
				});
				$('.ui-accordion').bind('accordionactivate', function(event, ui) {
				   if($(this).find('.ultimate-map-wrapper').length > 0)
					{
						setTimeout(function(){
							$(window).trigger('resize');
						},200);
					}
				});
				$(window).load(function(){
					setTimeout(function(){
						$(window).trigger('resize');
					},200);
				});
				$('.ult_exp_section').select(function(){
					if($(map_map_58946288cc0d9).parents('.ult_exp_section'))
					{
						setTimeout(function(){
							$(window).trigger('resize');
						},200);
					}
				});
				$(document).on('onUVCModalPopupOpen', function(){
					if($(map_map_58946288cc0d9).parents('.ult_modal-content'))
					{
						setTimeout(function(){
							$(window).trigger('resize');
						},200);
					}
				});
				$(document).on('click','.ult_tab_li',function(){
					$(window).trigger('resize');
					setTimeout(function(){
						$(window).trigger('resize');
					},200);
				});
			});
			function toggleBounce() {
			  if (marker_map_58946288cc0d9.getAnimation() != null) {
				marker_map_58946288cc0d9.setAnimation(null);
			  } else {
				marker_map_58946288cc0d9.setAnimation(google.maps.Animation.BOUNCE);
			  }
			}
			})(jQuery);
			</script></div></div></div><div class="wpb_column vc_column_container vc_col-sm-4"><div class="vc_column-inner "><div class="wpb_wrapper"><div class="ult-content-box-container " >		<div class="ult-content-box" style="background-color:#f1f5fa;box-shadow: px px px px rgb(247, 247, 247) none;padding:30px;margin-bottom:20px;-webkit-transition: all 700ms ease;-moz-transition: all 700ms ease;-ms-transition: all 700ms ease;-o-transition: all 700ms ease;transition: all 700ms ease;"  data-hover_box_shadow="none"    data-normal_margins="margin-bottom:20px;"   data-bg="#f1f5fa" ><div id="ultimate-heading-286458946288cccf1" class="uvc-heading ult-adjust-bottom-margin ultimate-heading-286458946288cccf1 uvc-1652 " data-hspacer="no_spacer"  data-halign="left" style="text-align:left"><div class="uvc-heading-spacer no_spacer" style="top"></div><div class="uvc-main-heading ult-responsive"  data-ultimate-target='.uvc-heading.ultimate-heading-286458946288cccf1 h3'  data-responsive-json-new='{"font-size":"desktop:24px;","line-height":"desktop:30px;"}' ><h3 style="font-family:'Montserrat';font-weight:700;margin-bottom:15px;">NEW YORK</h3></div><div class="uvc-sub-heading ult-responsive"  data-ultimate-target='.uvc-heading.ultimate-heading-286458946288cccf1 .uvc-sub-heading '  data-responsive-json-new='{"font-size":"desktop:14px;","line-height":"desktop:30px;"}'  style="font-family:'Montserrat';font-weight:700;"><span style="color: #435061;">40 MAINU STREET,<br />
SUITE 330, NEW YORK,<br />
NY 10013.</span></p>
<p>&nbsp;</p>
<p><span style="color: #435061;">TEL. 1-800-123-4567</span><br />
<span style="color: #435061;">FAX. 1-800-123-3456</span></p>
<p>&nbsp;</p>
<p><a href="http://helpinghands.skat.tf/faq/"><span style="text-decoration: underline;"><span style="color: #29af8a; text-decoration: underline;">NEED HELP? HAVE QUESTIONS?</span></span></a><br />
<a href="http://helpinghands.skat.tf/faq/"><span style="text-decoration: underline;"> <span style="color: #29af8a; text-decoration: underline;"> CHECK OUT OUR HELP CENTER.</span></span></a></div></div>		</div></div><div class="ult-content-box-container " >		<div class="ult-content-box" style="background-color:#f1f5fa;box-shadow: px px px px rgb(247, 247, 247) none;padding:30px;margin-bottom:20px;"  data-hover_box_shadow="none"    data-normal_margins="margin-bottom:20px;"   data-bg="#f1f5fa" ><div id="ultimate-heading-893258946288cd170" class="uvc-heading ult-adjust-bottom-margin ultimate-heading-893258946288cd170 uvc-2219 " data-hspacer="no_spacer"  data-halign="left" style="text-align:left"><div class="uvc-heading-spacer no_spacer" style="top"></div><div class="uvc-main-heading ult-responsive"  data-ultimate-target='.uvc-heading.ultimate-heading-893258946288cd170 h3'  data-responsive-json-new='{"font-size":"desktop:24px;","line-height":"desktop:30px;"}' ><h3 style="font-family:'Montserrat';font-weight:700;margin-bottom:15px;">LONDON</h3></div><div class="uvc-sub-heading ult-responsive"  data-ultimate-target='.uvc-heading.ultimate-heading-893258946288cd170 .uvc-sub-heading '  data-responsive-json-new='{"font-size":"desktop:14px;","line-height":"desktop:30px;"}'  style="font-family:'Montserrat';font-weight:700;"><span style="color: #435061;">23, SHERBROOKE WEST<br />
SUITE 330,LONDON,<br />
LN 10013.<br />
</span></p>
<p>&nbsp;</p>
<p><span style="color: #435061;">TEL. 1-800-123-4567</span><br />
<span style="color: #435061;">FAX. 1-800-123-3456</span></p>
<p>&nbsp;</p>
<p><a href="http://helpinghands.skat.tf/faq/"><span style="text-decoration: underline;"><span style="color: #29af8a; text-decoration: underline;">NEED HELP? HAVE QUESTIONS?</span></span></a><br />
<a href="http://helpinghands.skat.tf/faq/"><span style="text-decoration: underline;"> <span style="color: #29af8a; text-decoration: underline;"> CHECK OUT OUR HELP CENTER.</span></span></a></div></div>		</div></div></div></div></div></div><div class="ult-spacer spacer-58946288cd392" data-id="58946288cd392" data-height="70" data-height-mobile="70" data-height-tab="70" data-height-tab-portrait="" data-height-mobile-landscape="" style="clear:both;display:block;"></div></div></div></div></div></div></div>
		</div>
		<!-- entry-content --> 
	</article>
	<!-- sd-full-width -->
