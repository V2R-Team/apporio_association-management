<?php include('header.php');

switch($pages['page']) {
    case ""                         : include "home.php"; break;
    case "event"                    : include "event.php"; break;
    case "about"                    : include "about_us.php"; break;
    case "terms-of-use"             : include "about_us.php"; break;
    case "privacy-policy"           : include "about_us.php"; break;
    case "disclaimer"               : include "about_us.php"; break;
    case "ourcauses"                : include "about_us.php"; break;
    case "login"                    : include "login.php"; break;
    case "renew"                    : include "renew.php"; break;
    case "news"                     : include "news.php"; break;
    case "contact"                  : include "contactus.php"; break;
    case "membership-registration-form"               	: include "membership_registration_form.php"; break;
    case "thank-you"                  : include "thank_you.php"; break;
 
    default                         : include "404.php"; break;
}

include('footer.php');?>