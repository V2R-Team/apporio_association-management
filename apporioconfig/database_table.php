<?php

define('TABLE_ADMIN', 'admin');

define('TABLE_ACCOUNT', 'account_setting');

define('TABLE_COUNTRY', 'country');

define('TABLE_CITY', 'city');

define('TABLE_EVENT_CATEGORY', 'event_category');

define('TABLE_EVENT', 'event');

define('TABLE_NEWS_CATEGORY', 'news_category');

define('TABLE_NEWS', 'news');

define('TABLE_GALLERY', 'gallery');

define('TABLE_MEMBERSHIP', 'membership');

define('TABLE_MEMBERS', 'members');

define('TABLE_PAGES', 'pages');

define('TABLE_ACCOUNT_SETTING', 'account_setting');

define('TABLE_MEMBERSHIP_REGISTRATION', 'membership_registration_form');

?>
