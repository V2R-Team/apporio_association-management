<article id="" class="">
	<div class="entry-content">
		<div class="top_background" style="border-style: none;">
			<div class="container">
				<div class=""  style="font-family:'Montserrat';font-weight:700;">
					<p class="sd-text-bg-white">	Contact Us</p>
				</div>
			</div>
		</div>
	</div>
</article>
<div class="container">
	<div style="margin-top: 50px;"></div>
	<div class="wpb_column vc_column_container vc_col-sm-8">
		<div class="vc_column-inner ">
			<div class="wpb_wrapper">
				<div id="ultimate-heading-90658946288c8bad" class="uvc-heading ult-adjust-bottom-margin ultimate-heading-90658946288c8bad uvc-1195 " data-hspacer="no_spacer"  data-halign="left" style="text-align:left">
					<div class="uvc-heading-spacer no_spacer" style="top"></div>
					<div class="uvc-main-heading ult-responsive"  data-ultimate-target='.uvc-heading.ultimate-heading-90658946288c8bad h3'  data-responsive-json-new='{"font-size":"desktop:24px;","line-height":"desktop:30px;"}' >
						<h3 style="font-family:'Montserrat';font-weight:700;margin-bottom:15px;">HAVE AN IDEA TO HELP? - CONTACT US</h3>
					</div>
					
				</div>
				<div role="form" class="wpcf7" id="wpcf7-f357-p355-o1" lang="en-US" dir="ltr">
					<div class="screen-reader-response"></div>
					<form action="" method="post" class="wpcf7-form" novalidate="novalidate">
						<div style="display: none;">
							<input type="hidden" name="_wpcf7" value="357" />
							<input type="hidden" name="_wpcf7_version" value="4.6.1" />
							<input type="hidden" name="_wpcf7_locale" value="en_US" />
							<input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f357-p355-o1" />
							<input type="hidden" name="_wpnonce" value="2a108b8ef3" />
						</div>
						<p>
							<span class="wpcf7-form-control-wrap your-name"><input type="text" name="your-name" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" placeholder="Name" /></span> 
						</p>
						<p>
							<span class="wpcf7-form-control-wrap your-email"><input type="email" name="your-email" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email" aria-required="true" aria-invalid="false" placeholder="E-Mail" /></span> 
						</p>
						<p>
							<span class="wpcf7-form-control-wrap your-message"><textarea name="your-message" cols="40" rows="10" class="wpcf7-form-control wpcf7-textarea" aria-invalid="false" placeholder="Message"></textarea></span> 
						</p>
						
						<p style="margin-bottom: 50px;">
							<input type="submit" value="Submit" class="wpcf7-form-control wpcf7-submit" />
						</p>
						<div class="wpcf7-response-output wpcf7-display-none"></div>
					</form>
				</div>
			</div>
		</div>
	</div>
	
	<div class="wpb_column vc_column_container vc_col-sm-4">
		<div class="vc_column-inner ">
			<div class="wpb_wrapper">
				<div class="ult-content-box-container " >		
					<div class="address_side">
						<div id="" class="uvc-heading ult-adjust-bottom-margin ultimate-heading-286458946288cccf1" style="text-align:left">
							<h3 style="font-family:'Montserrat';font-weight:700;margin-bottom:15px;"><?php echo $account_settings['acc_city']?></h3>
							<div class="uvc-sub-heading ult-responsive"  data-ultimate-target='.uvc-heading.ultimate-heading-286458946288cccf1 .uvc-sub-heading ' data-responsive-json-new='{"font-size":"desktop:14px;","line-height":"desktop:30px;"}'  style="font-family:'Montserrat';font-weight:700;">
								<span style="color: #435061;"><?php echo nl2br($account_settings['acc_address'])?></span>
								<p>&nbsp;</p>
								<p>
									<span style="color: #435061;">Phone: <?php echo $account_settings['acc_phone']?></span><br />
									<span style="color: #435061;">Email: <a href="mailto:<?php echo $account_settings['acc_email']?>"> <?php echo $account_settings['acc_email']?></a></span>
								</p>
								<p>&nbsp;</p>
								<a href="#">
									<span>
										<span style="color: #29af8a;">NEED HELP? HAVE QUESTIONS?</span>
									</span>
								</a><br />
								<a href="#">
									<span> 
										<span style="color: #29af8a;"> CHECK OUT OUR HELP CENTER.</span>
									</span>
								</a>
							</div>
						</div>		
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-12" style="margin-bottom: 50px;">
		<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1797240.729698127!2d76.4877098114554!3d28.520137006391277!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x30635ff06b92b791%3A0xd78c4fa1854213a6!2sIndia!5e0!3m2!1sen!2sin!4v1486822179402" width="100%" height="450" frameborder="0" style="border:0"></iframe>
	</div>
</div>
			
