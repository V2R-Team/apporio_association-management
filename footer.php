<div class="sd-boxed-footer">
  <div class=""> 
    <!-- sd-newsletter -->
    <footer id="sd-footer" class=""> 
    
      <!-- footer widgets -->
      <div class="sd-footer-widgets sd-footer-widgets-4 sd-boxed-padding container">
        <div >
          <div class="row">
            <div class="col-md-4 col-sm-4 col-xs-12 sd-footer-sidebar-1">
              <div class="sd-footer-sidebar-1-content">
                <aside id="text-1" class="sd-footer-sidebar-widget clearfix widget_text">
                  <h4 class="sd-footer-widget-title">About Us</h4>
                  <div class="textwidget">
                    <p>
                      <?php 
									$page_about= $db->db_get_array("SELECT * FROM ".TABLE_PAGES." WHERE page_slug= 'about' ");
									
									echo substr($page_about[0]['description'], 0,100);
								?>
                    </p>
                    <a href="<?php echo BASE_URL;?>about" class="sd-more">Learn More</a> </div>
                </aside>
              </div>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-12 sd-footer-sidebar-4">
              <div class="sd-footer-sidebar-4-content">
                <aside id="text-2" class="sd-footer-sidebar-widget clearfix widget_text">
                  <h4 class="sd-footer-widget-title">Helping Hands</h4>
                  <br>
                  <?php echo nl2br($account_settings['acc_address'])?>
                  
                </aside>
                
              </div>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-12 sd-footer-sidebar-4">
              <div class="sd-footer-sidebar-4-content">
                <aside id="text-2" class="sd-footer-sidebar-widget clearfix widget_text">
                 
                  <div class="textwidget"> <br/>
                    <br/>
                    Phone : <?php echo $account_settings['acc_phone']?><br/>
                    Email : <a href="mailto:<?php echo $account_settings['acc_email']?>"> <?php echo $account_settings['acc_email']?></a></div>
                </aside>
                <aside id="sd_social_icons_widget-1" class="sd-footer-sidebar-widget clearfix sd_social_icons_widget">
                  <ul class="sd-social-icons-widget">
                    <li class="sd-social-widget-facebook"><a class="sd-link-trans" href="<?php echo $account_settings['acc_fb']?>" title="https://www.facebook.com/" rel="nofollow" target="_blank"><i class="fa fa-facebook"></i></a></li>
                    <li class="sd-social-widget-twitter"><a class="sd-link-trans" href="<?php echo $account_settings['acc_tw']?>" title="" rel="nofollow" target="_blank"><i class="fa fa-twitter"></i></a></li>
                    <li class="sd-social-widget-linkedin"><a class="sd-link-trans" href="<?php echo $account_settings['acc_linked']?>" title="" rel="nofollow" target="_blank"><i class="fa fa-linkedin"></i></a></li>
                    <li class="sd-social-widget-googleplus"><a class="sd-link-trans" href="<?php echo $account_settings['acc_go']?>" title="" rel="nofollow" target="_blank"><i class="fa fa-google-plus"></i></a></li>
                    <li class="sd-social-widget-youtube"><a class="sd-link-trans" href="<?php echo $account_settings['acc_youtube']?>" title="" rel="nofollow" target="_blank"><i class="fa fa-youtube-play"></i></a></li>
                    <li class="sd-social-widget-pinterest"><a class="sd-link-trans" href="<?php echo $account_settings['acc_pinterest']?>" title="" rel="nofollow" target="_blank"><i class="fa fa-pinterest"></i></a></li>
                  </ul>
                </aside>
              </div>
            </div>
          </div>
          <!-- row --> 
        </div>
        <!-- container --> 
      </div>
      <!-- sd-footer-widgets -->
      <div class="sd-copyright-wrapper clearfix ">
          <div class="sd-copyright sd-boxed-padding container"> <span class="sd-copyright-text">Copyright &copy; 2017 - <a href="#" title="Charity WordPress Themes" target="_blank"> Association Management </a></span>
            <nav class="sd-footer-menu">
              <ul id="menu-footer-menu" class="">
                <li id="menu-item-334" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-334"><a href="<?php echo BASE_URL;?>terms-of-use">Terms of Use</a></li>
                <li id="menu-item-335" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-335"><a href="<?php echo BASE_URL;?>privacy-policy">Privacy Policy</a></li>
                <li id="menu-item-341" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-341"><a href="<?php echo BASE_URL;?>disclaimer">Disclaimer</a></li>
              </ul>
            </nav>
            <!-- sd-footer-menu --> 
          </div>
          <!-- sd-copyright --> 
        
      
      <!-- sd-copyright-wrapper -->
      </div>
      </footer>
    <!-- footer end --> 
  </div>
  <!-- container --> 
</div>
<!-- sd-boxed-footer -->
</div>
<!-- sd-wrapper --> 
<script type="text/javascript">
			function revslider_showDoubleJqueryError(sliderID) {
				var errorMessage = "Revolution Slider Error: You have some jquery.js library include that comes after the revolution files js include.";
				errorMessage += "<br> This includes make eliminates the revolution slider libraries, and make it not work.";
				errorMessage += "<br><br> To fix it you can:<br>&nbsp;&nbsp;&nbsp; 1. In the Slider Settings -> Troubleshooting set option:  <strong><b>Put JS Includes To Body</b></strong> option to true.";
				errorMessage += "<br>&nbsp;&nbsp;&nbsp; 2. Find the double jquery.js include and remove it.";
				errorMessage = "<span style='font-size:16px;color:#BC0C06;'>" + errorMessage + "</span>";
					jQuery(sliderID).show().html(errorMessage);
			}
		</script> 
<script type='text/javascript' src='skat.tf/wp-includes/js/comment-reply.mine100.js?ver=4.7.2'></script> 
<script type='text/javascript' src='skat.tf/wp-content/plugins/contact-form-7/includes/js/jquery.form.mind03d.js?ver=3.51.0-2014.06.20'></script> 
<script type='text/javascript'>
/* <![CDATA[ */
var _wpcf7 = {"recaptcha":{"messages":{"empty":"Please verify that you are not a robot."}},"cached":"1"};
/* ]]> */
</script> 
<script type='text/javascript' src='skat.tf/wp-content/plugins/contact-form-7/includes/js/scripts1c9b.js?ver=4.6.1'></script> 
<script type='text/javascript' src='skat.tf/wp-content/plugins/woocommerce/assets/js/jquery-blockui/jquery.blockUI.min44fd.js?ver=2.70'></script> 
<script type='text/javascript'>
/* <![CDATA[ */
var woocommerce_params = {"ajax_url":"\/wp-admin\/admin-ajax.php","wc_ajax_url":"\/?wc-ajax=%%endpoint%%"};
/* ]]> */
</script> 
<script type='text/javascript' src='skat.tf/wp-content/plugins/woocommerce/assets/js/frontend/woocommerce.min61da.js?ver=2.6.2'></script> 
<script type='text/javascript' src='skat.tf/wp-content/plugins/woocommerce/assets/js/jquery-cookie/jquery.cookie.min330a.js?ver=1.4.1'></script> 
<script type='text/javascript'>
/* <![CDATA[ */
var wc_cart_fragments_params = {"ajax_url":"\/wp-admin\/admin-ajax.php","wc_ajax_url":"\/?wc-ajax=%%endpoint%%","fragment_name":"wc_fragments"};
/* ]]> */
</script> 
<script type='text/javascript' src='skat.tf/wp-content/plugins/woocommerce/assets/js/frontend/cart-fragments.min61da.js?ver=2.6.2'></script> 
<script type='text/javascript' src='skat.tf/wp-content/themes/helpinghands/framework/js/prettyphotoe100.js?ver=4.7.2'></script> 
<script type='text/javascript' src='skat.tf/wp-content/plugins/js_composer/assets/lib/bower/flexslider/jquery.flexslider-min972f.js?ver=5.0.1'></script> 
<script type='text/javascript' src='skat.tf/wp-content/themes/helpinghands/framework/js/custome100.js?ver=4.7.2'></script> 
<script type='text/javascript' src='skat.tf/wp-includes/js/hoverIntent.minc245.js?ver=1.8.1'></script> 
<script type='text/javascript'>
/* <![CDATA[ */
var megamenu = {"timeout":"300","interval":"100"};
/* ]]> */
</script> 
<script type='text/javascript' src='skat.tf/wp-content/plugins/megamenu/js/maxmegamenu531b.js?ver=2.3.4'></script> 
<script type='text/javascript' src='skat.tf/wp-includes/js/wp-embed.mine100.js?ver=4.7.2'></script> 
<script type='text/javascript' src='skat.tf/wp-content/plugins/js_composer/assets/js/dist/js_composer_front.min972f.js?ver=5.0.1'></script>
</body></html>