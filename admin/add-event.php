<?php
include('common.php');
include_once '../apporioconfig/start_up.php';
if ($admininfos['eventman_add_status'] != 1) {
  $db->redirect("home.php?pages=dashboard");
}
if(isset($_GET['edit_id'])) {
		$event_data = $db->db_get_array("SELECT * FROM ".TABLE_EVENT." WHERE event_id='".$_GET['edit_id']."' ");
		foreach($event_data as $event_res);
}

if($_SERVER['REQUEST_METHOD'] == "POST") {
 $event_location = $_POST['event_location'];
  
                        $address = str_replace(" ", "+", $event_location);
	                $url = "http://maps.google.com/maps/api/geocode/json?address=$address&sensor=false";
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
			$response = curl_exec($ch);
			curl_close($ch);
			$response_a = json_decode($response);
			$lat = $response_a->results[0]->geometry->location->lat;
			
			$long = $response_a->results[0]->geometry->location->lng;

  if(!isset($_GET['edit_id'])) {
  $slug = $db->slugify($_POST['event_name'], TABLE_EVENT, 'event_slug');
  $arr = array(
    'event_category_id'     => $_POST['event_category_id'],
    'event_name'            => $_POST['event_name'],
    'event_name_other'            => $_POST['event_name_other'],
    'event_description'     => $_POST['event_description'],
    'event_description_other'     => $_POST['event_description_other'],
    'madmin_id'             => $muser_id,
    'event_start_date'      => $_POST['event_start_date'],
    'event_end_date'        => $_POST['event_end_date'],
    'event_start_time'      => $_POST['event_start_time'],
    'event_end_time'        => $_POST['event_end_time'],
    'event_vanue'           => $_POST['event_vanue'],
    'event_address_one'     => $_POST['event_address_one'],
    'event_address_two'     => $_POST['event_address_two'],
    'event_country'         => $_POST['event_country'],
    'event_city'            => $_POST['event_city'],
    'event_location'        => $_POST['event_location'],
    'event_slug'            => $slug,
    'latitude'              => $lat,
    'longitude'            => $long,
  );

  $insert_id =  $db->insert(TABLE_EVENT,$arr,true);

  $condition = " `event_id` = '".$insert_id."' ";
  if($_FILES['event_banner']['name'] != "") {
    $ext = substr($_FILES['event_banner']['name'],-4);
    if($ext == ".jpg" || $ext == ".png" || $ext == ".gif" || $ext == "jpeg") {
      $ext = ($ext == "jpeg") ? ".jpg" : $ext;
      $dir    = "../uploads/event_banner";
      if(!is_dir($dir) || !file_exists($dir)) mkdir($dir,0755);
      $filename = 'uploads/event_banner/'.$insert_id.$ext;
      $filepath = "../".$filename;
      if(file_exists($filepath)) unlink($filepath);
      copy($_FILES['event_banner']['tmp_name'],$filepath);
    }
    $db->update(TABLE_EVENT,array('event_banner' => $filename),$condition);
  }
  $db->redirect("home.php?pages=view-event");
} else {
  $arr = array(
    'event_category_id'     => $_POST['event_category_id'],
    'event_name'            => $_POST['event_name'],
    'event_name_other'            => $_POST['event_name_other'],
    'event_description'     => $_POST['event_description'],
    'event_description_other'     => $_POST['event_description_other'],
    'event_start_date'      => $_POST['event_start_date'],
    'event_end_date'        => $_POST['event_end_date'],
    'event_start_time'      => $_POST['event_start_time'],
    'event_end_time'        => $_POST['event_end_time'],
    'event_vanue'           => $_POST['event_vanue'],
    'event_address_one'     => $_POST['event_address_one'],
    'event_address_two'     => $_POST['event_address_two'],
    'event_country'         => $_POST['event_country'],
    'event_city'            => $_POST['event_city'],
    'event_location'        => $_POST['event_location'],
    'latitude'              => $lat,
    'longitude'             => $long,
  );
  $condition = "event_id='".$_GET['edit_id']."' ";
  $db->update(TABLE_EVENT,$arr,$condition,true);
  $insert_id  =  $_GET['edit_id'];
  $condition = " `event_id` = '".$insert_id."' ";
  if($_FILES['event_banner']['name'] != "") {
    $ext = substr($_FILES['event_banner']['name'],-4);
    if($ext == ".jpg" || $ext == ".png" || $ext == ".gif" || $ext == "jpeg") {
      $ext = ($ext == "jpeg") ? ".jpg" : $ext;
      $dir    = "../uploads/event_banner";
      if(!is_dir($dir) || !file_exists($dir)) mkdir($dir,0755);
      $filename = 'uploads/event_banner/'.$insert_id.$ext;
      $filepath = "../".$filename;
      if(file_exists($filepath)) unlink($filepath);
      copy($_FILES['event_banner']['tmp_name'],$filepath);
    }
    $db->update(TABLE_EVENT,array('event_banner' => $filename),$condition);
  }
  $db->redirect("home.php?pages=view-event");

}
}
?>

<script type="text/javascript">
function change_clty(country_id){
  $.ajax({
   url:"ajax.php?action=change_city&country_id="+country_id,
    success:function(response){
      $("#event_city").html(response);
    }
  });
 }

function validatevent() {
    if(document.getElementById('event_category_id').value == "")
    {
      alert("Please Select Category");
      document.getElementById('event_category_id').focus();
      return false;
    }

    if(document.getElementById('event_name').value == "")
    {
      alert("Please Enter Event Title");
      document.getElementById('event_name').focus();
      return false;
    }
    
    if(document.getElementById('event_name_other').value == "")
    {
      alert("Please Enter Event Title");
      document.getElementById('event_name_other').focus();
      return false;
    }
    
    if(document.getElementById('event_description').value == "")
    {
      alert("Please Enter Event Description");
      document.getElementById('event_description').focus();
      return false;
    }
    
    if(document.getElementById('event_description_other').value == "")
    {
      alert("Please Enter Event Description");
      document.getElementById('event_description_other').focus();
      return false;
    }

    if(document.getElementById('event_banner').value == "")
    {
      alert("Please Select Event Banner");
      document.getElementById('event_banner').focus();
      return false;
    }

    if(document.getElementById('event_start_date').value == "")
    {
      alert("Please Enter Event Start Date");
      document.getElementById('event_start_date').focus();
      return false;
    }

    if(document.getElementById('event_end_date').value == "")
    {
      alert("Please Enter Event End Date");
      document.getElementById('event_end_date').focus();
      return false;
    }

    if(document.getElementsByName('event_start_time').value == "")
    {
      alert("Please Enter Event Start Time");
      document.getElementsByName('event_start_time').focus();
      return false;
    }

    if(document.getElementsByName('event_end_time').value == "")
    {
      alert("Please Enter Event End Time");
      document.getElementsByName('event_end_time').focus();
      return false;
    }
   
    if(document.getElementById('event_vanue').value == "")
    {
      alert("Please Enter Event Venue");
      document.getElementById('event_vanue').focus();
      return false;
    }

    if(document.getElementById('event_address_one').value == "")
    {
      alert("Please Enter Event Address 1");
      document.getElementById('event_address_one').focus();
      return false;
    }

    if(document.getElementById('event_country').value == "")
    {
      alert("Please Select Country");
      document.getElementById('event_country').focus();
      return false;
    }

    if(document.getElementById('event_city').value == "")
    {
      alert("Please Select City");
      document.getElementById('event_city').focus();
      return false;
    }

    if(document.getElementById('event_location').value == "")
    {
      alert("Please Enter Location");
      document.getElementById('event_location').focus();
      return false;
    }
    return true;
  }
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB21baMnSAvRrcZ3IKymum3_vJFN4hm7xU&v=3.exp&libraries=places"></script>

<script>
function initialize() {

var input = document.getElementById('event_location');

var autocomplete = new google.maps.places.Autocomplete(input);
}
google.maps.event.addDomListener(window, 'load', initialize);
</script>


  <!-- Page Content Start -->
  <!-- ================== -->

<div class="wraper container-fluid">
    <div class="page-title">
      <h3 class="title">Add Event</h3>
    </div>
    <div class="row">
      <div class="col-sm-12">
        <div class="panel panel-default">

          <div class="panel-body">
            <div class="form">
              <form class="cmxform form-horizontal tasi-form"  method="post"  onSubmit="return validatevent()" enctype="multipart/form-data" action="">

                <div class="form-group ">
                  <label class="control-label col-lg-2">Select Category *</label>
                  <div class="col-lg-10">
                     <select class="form-control" name="event_category_id" id="event_category_id">
                        <option value="">--Please Select Category --</option>
                        <?php $event_category = $db->db_get_array("SELECT event_category_id,event_category_name FROM ".TABLE_EVENT_CATEGORY." WHERE madmin_id='".$muser_id."' AND event_category_status = 1" );
                          foreach ($event_category as $event_categorys) {?>
                           <option <?php if($event_res['event_category_id'] == $event_categorys['event_category_id']) echo "selected=selected";?> value="<?php echo $event_categorys['event_category_id']?>"><?php echo $event_categorys['event_category_name']?></option>
                         <?php }?>
                    </select>
              </div>
                </div>
                <div class="form-group ">
                  <label class="control-label col-lg-2">Event Title*</label>
                  <div class="col-lg-10">
                    <input type="text" class="form-control" value="<?php echo $event_res['event_name']; ?>" placeholder="Event Title" name="event_name" id="event_name">
                  </div>
                </div>
                <div class="form-group ">
                  <label class="control-label col-lg-2">Event Titlein french*</label>
                  <div class="col-lg-10">
                    <input type="text" class="form-control" value="<?php echo $event_res['event_name_other']; ?>" placeholder="Event Title(french)" name="event_name_other" id="event_name_other">
                  </div>
                </div>
                
                

                <div class="form-group ">
                  <label class="control-label col-lg-2">Event Description *</label>
                  <div class="col-lg-10">
                    <textarea class="form-control" placeholder="Event Description" name="event_description" id="event_description"><?php echo $event_res['event_description']; ?></textarea>
                  </div>
                </div>

                <div class="form-group ">
                  <label class="control-label col-lg-2">Event Description in french*</label>
                  <div class="col-lg-10">
                    <textarea class="form-control" placeholder="Event Description(french)" name="event_description_other" id="event_description"><?php echo $event_res['event_description_other']; ?></textarea>
                  </div>
                </div>
                

                <div class="form-group ">
                  <label for="username" class="control-label col-lg-2">Event Banner*</label>
                  <div class="col-lg-10">

                    <div class="input-group image-preview">
                      <input type="text" value="<?php echo $event_res['event_banner']; ?>" id="event_banners" class="form-control image-preview-filename" disabled="disabled"> <!-- don't give a name === doesn't send on POST/GET -->
                      <span class="input-group-btn">
                          <!-- image-preview-clear button -->
                          <button type="button" class="btn btn-default image-preview-clear" style="display:none;">
                              <span class="glyphicon glyphicon-remove"></span> Clear
                          </button>
                          <!-- image-preview-input -->
                          <div class="btn btn-default image-preview-input">
                              <span class="glyphicon glyphicon-folder-open"></span>
                              <span class="image-preview-input-title">Browse</span>
                              <input type="file" value="<?php echo $event_res['event_banner']; ?>" accept="image/png, image/jpeg, image/gif" name="event_banner" id="event_banner" > <!-- rename it -->
                          </div>
                      </span>
                    </div>
                  </div>
                </div>
                  <div class="form-group ">
                  <label class="control-label col-lg-2">Event Date*</label>
                  <div class="col-lg-5">
                    <input type="text" class="form-control start_time" value="<?php echo $event_res['event_start_date']; ?>" placeholder="Event Start Date" name="event_start_date" id="event_start_date">
                  </div>
                  <div class="col-lg-5">
                    <input type="text" class="form-control end_date" value="<?php echo $event_res['event_end_date']; ?>" placeholder="Event End Date" name="event_end_date" id="event_end_date">
                  </div>
                </div>
                  <div class="form-group ">
                  <label class="control-label col-lg-2">Event Time*</label>
                  <div class="col-lg-5">
                   <label>Start Time</label>
                    <div class="bfh-timepicker" data-value="" data-placeholder="" data-name="event_start_time" data-mode="12h"></div>
                  </div>


                  <div class="col-lg-5">
                  <label>End Time</label>
                   <div class="bfh-timepicker" data-placeholder="" data-name="event_end_time" data-mode="12h"></div>
                  </div>
                </div>

                <div class="form-group ">
                  <label class="control-label col-lg-2">Event Venue*</label>
                  <div class="col-lg-10">
                    <input type="text" value="<?php echo $event_res['event_vanue']; ?>" class="form-control"  placeholder="Event Venue Name" name="event_vanue" id="event_vanue" >
                  </div>
                </div>

                 <div class="form-group ">
                  <label class="control-label col-lg-2">Event Address 1*</label>
                  <div class="col-lg-10">
                    <textarea class="form-control" placeholder="Event Address" name="event_address_one" id="event_address_one"><?php echo $event_res['event_address_one']; ?></textarea>
                  </div>
                </div>

                <div class="form-group ">
                  <label class="control-label col-lg-2">Event Address 2</label>
                  <div class="col-lg-10">
                    <textarea class="form-control" placeholder="Event Address" name="event_address_two" id="event_address_two"><?php echo $event_res['event_address_two']; ?></textarea>
                  </div>
                </div>

                <div class="form-group ">
                  <label class="control-label col-lg-2">Select Country *</label>
                  <div class="col-lg-10">
                     <select class="form-control" name="event_country" id="event_country" onchange="change_clty(this.value)">
                        <option value="">--Please Select Country --</option>
                        <?php $country_name = $db->db_get_array("SELECT country_id,country_name FROM ".TABLE_COUNTRY." WHERE madmin_id='".$muser_id."' AND country_status = 1" );
                          foreach ($country_name as $country_names) {?>
                           <option <?php if($event_res['event_country'] == $country_names['country_id']) { echo "selected=selected"; } ?> value="<?php echo $country_names['country_id']?>"><?php echo $country_names['country_name']?></option>
                         <?php }?>
                    </select>
                </div>
                </div>

                 <div class="form-group ">
                  <label class="control-label col-lg-2">Select City *</label>
                  <div class="col-lg-10">



                     <?php if(!isset($_GET['edit_id'])) {?>
                       <select class="form-control" name="event_city" id="event_city">
                          <option value="">--Please Select City --</option>
                       </select>
                     <?php } else { ?>
                       <select class="form-control" name="event_city" id="event_city">
                          <option value="">--Please Select Country --</option>
                          <?php $city_name = $db->db_get_array("SELECT city_id,city_name FROM ".TABLE_CITY." WHERE city_country_id = ".$event_res['event_country']." " );
                            foreach ($city_name as $city_names) {?>
                             <option <?php if($event_res['event_city'] == $city_names['city_id']) { echo "selected=selected"; } ?> value="<?php echo $city_names['city_id']?>"><?php echo $city_names['city_name']?></option>
                           <?php }?>
                       </select>
                     <?php } ?>

                  </div>
                </div>

                    <div class="form-group ">
                  <label class="control-label col-lg-2">Nearest Geo Location*</label>
                  <div class="col-lg-10">
                    <input type="text" class="form-control" value="<?php echo $event_res['event_location']?>"  placeholder="Nearest Geo Location" name="event_location" id="event_location">
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-lg-offset-2 col-lg-10">
                    <?php if(!isset($_GET['edit_id'])) { ?>
                    	<input type="submit" class=" btn btn-info col-md-4 col-sm-6 col-xs-12" id="save" name="save" value="Save" >
										<?php } else { ?>
											<input type="submit" class=" btn btn-info col-md-4 col-sm-6 col-xs-12" id="save" name="save" value="Update" >
										<?php } ?>
                  </div>
                </div>





              </form>
            </div>
            <!-- .form -->

          </div>
          <!-- panel-body -->
        </div>
        <!-- panel -->
      </div>
      <!-- col -->

    </div>
    <!-- End row -->

  </div>

  <!-- Page Content Ends -->
  <!-- ================== -->

</section>
<!-- Main Content Ends -->

</body>
</html>
<script type="text/javascript" src="js/wickedpicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css">

        <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>

<link rel="stylesheet" type="text/css" href="css/wickedpicker.min.css">



<script>
   $(function() {
       $('.start_time').datepicker({

         format: "yyyy-mm-dd"

     });
   });
   $(function() {
       $('.end_date').datepicker({

        format: "yyyy-mm-dd" });
   });

    $('.start').wickedpicker();

     $('.end').wickedpicker();

$('.time').bootstrapMaterialDatePicker({ date: false });


</script>
