<?php
	include "../apporioconfig/start_up.php";

	switch($_REQUEST['pages']) {

		case "dashboard" : include "dashboard.php"; break;

		case "account-setting" : include "account_setting.php"; break;

		case "profile" : include "profile.php"; break;

		case "change-password" : include "changepass.php"; break;

		// case "view-user" : include "users.php"; break;

		case "add-city" : include "add-city.php"; break;

		case "view-city" : include "view-city.php"; break;

		case "add-counrty" : include "add-counrty.php"; break;

		case "view-counrty" : include "view-counrty.php"; break;

                 case "add-member" : include "add-member.php"; break;

		case "view-member" : include "view-member.php"; break;

		case "add-membership" : include "add-membership.php"; break;

		case "view-membership" : include "view-membership.php"; break;

		case "view-renewmember" : include "view-renewmember.php"; break;

		case "add-event" : include "add-event.php"; break;

		case "view-event" : include "view-event.php"; break;

		case "view_event_category" : include "view_event_category.php"; break;

		case "add_event_category" : include "add_event_category.php"; break;

		case "add-news" : include "add-news.php"; break;

		case "view-news" : include "view-news.php"; break;

		case "add-gallery" : include "add-gallery.php"; break;

		case "view-gallery" : include "view-gallery.php"; break;
		
		case "add-causes" : include "add-causes.php"; break;

		case "view-causes" : include "view-causes.php"; break;
		
		case "add-account" : include "add-account.php"; break;

		case "view-account" : include "view-account.php"; break;

		case "add-cat-news" : include "add-cat-news.php"; break;

		case "view-cat-news" : include "view-cat-news.php"; break;

		case "add-user" : include "add_user.php"; break;

		case "view-user" : include "view_user.php"; break;

		case "page" : include "page.php"; break;

		case "logout" : include "logout.php"; break;

		case "index" : include "index.php"; break;

		case "view-merchant" : include "view-merchant.php"; break;

		case "add-merchant" : include "add-merchant.php"; break;

		case "membership-registration" : include "membership-registration.php"; break;

		case "membership-registration-details" : include "membership-registration-details.php"; break;



		default : include "404.php";
	}
?>
