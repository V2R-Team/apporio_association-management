<script src="js/jquery.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/modernizr.min.js"></script>
        <script src="js/pace.min.js"></script>
        <script src="js/wow.min.js"></script>
        <script src="js/jquery.scrollTo.min.js"></script>
        <script src="js/jquery.nicescroll.js" type="text/javascript"></script>
        <script src="taxi/chat/moment-2.2.1.js"></script>

        <!-- Counter-up -->
        <script src="js/waypoints.min.js" type="text/javascript"></script>
        <script src="js/jquery.counterup.min.js" type="text/javascript"></script>

        <!-- EASY PIE CHART JS -->
        <script src="taxi/easypie-chart/easypiechart.min.js"></script>
        <script src="taxi/easypie-chart/jquery.easypiechart.min.js"></script>
        <script src="taxi/easypie-chart/example.js"></script>


        <!--C3 Chart-->
        <script src="taxi/c3-chart/d3.v3.min.js"></script>
        <script src="taxi/c3-chart/c3.js"></script>

        <!--Morris Chart-->
        <script src="taxi/morris/morris.min.js"></script>
        <script src="taxi/morris/raphael.min.js"></script>

        <!-- sparkline --> 
        <script src="taxi/sparkline-chart/jquery.sparkline.min.js" type="text/javascript"></script>
        <script src="taxi/sparkline-chart/chart-sparkline.js" type="text/javascript"></script> 

        <!-- sweet alerts -->
        <script src="taxi/sweet-alert/sweet-alert.min.js"></script>
        <script src="taxi/sweet-alert/sweet-alert.init.js"></script>

        <script src="js/jquery.app.js"></script>
        <!-- Chat -->
        <script src="js/jquery.chat.js"></script>
        <!-- Dashboard -->
        <script src="js/jquery.dashboard.js"></script>

        <!-- Todo -->
        <script src="js/jquery.todo.js"></script>


        <script src="taxi/datatables/jquery.dataTables.min.js"></script>
        <script src="taxi/datatables/dataTables.bootstrap.js"></script>


        <script type="text/javascript">
            $(document).ready(function() {
                $('#datatable').dataTable();
            } );
        </script>

        