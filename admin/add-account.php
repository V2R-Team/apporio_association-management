<?php
include('common.php');
include_once '../apporioconfig/start_up.php';

$list = $db->db_get_array("SELECT * FROM ".account_setting." ");
                  
                
             
$table = TABLE_ACCOUNT_SETTING;
   
if($_SERVER['REQUEST_METHOD'] == "POST") { 
   
 if($_FILES['acc_image']['name'] != "") 
 {
  
      $ext = substr($_FILES['acc_image']['name'],-4);
      if($ext == ".jpg" || $ext == ".png" || $ext == ".gif" || $ext == "jpeg") {
        $ext = ($ext == "jpeg") ? ".jpg" : $ext;
        $dir    = "../admin/img";
        if(!is_dir($dir) || !file_exists($dir)) mkdir($dir,0755);
       $id= uniqid();
        $filename = 'img/'.$id.$ext;
        $filepath = "../".$filename;
        if(file_exists($filepath)) unlink($filepath);         
        copy($_FILES['acc_image']['tmp_name'],$filepath);
      }
   $arr = array(
      'acc_site_title'=> $_POST['acc_site_url'],
       'acc_email'    => $_POST['acc_email'],
        'acc_url'=> $_POST['account_url'],
          'acc_phone'    => $_POST['phone'],
           'acc_mobile'=> $_POST['mobile'],
            'acc_img' => $filename,
              'acc_address' => $_POST['address'] ,
	        'acc_country' => $_POST['country'], 
	          'acc_state' => $_POST['state'] ,
	            'acc_city' => $_POST['city'] ,
	              'acc_zip' => $_POST['zipcode'], 
	                'acc_map' => $_POST['map'] ,
  	                  'acc_fb' => $_POST['facebook'], 
		             'acc_tw' => $_POST['twitter'], 
		               'acc_go' => $_POST['google'], 
		                  'acc_linked' => $_POST['linkdein'],  
		                    'acc_youtube' => $_POST['youtube'],  
		                     'acc_pinterest' => $_POST['pineterest'], 
		                       	 'acc_dribble' => $_POST['dribble'], 
				            'acc_rss' => $_POST['rss'] 
              
    );
    
    
           $condition = "acc_id= '1'";
        $db->update($table ,$arr,$condition); 
       //$db->insert(account_setting,$arr,true);   
    $db->redirect("home.php?pages=add-account");
}
else
{

$arr = array(
      'acc_site_title'=> $_POST['acc_site_url'],
       'acc_email'    => $_POST['acc_email'],
        'acc_url'=> $_POST['account_url'],
          'acc_phone'    => $_POST['phone'],
           'acc_mobile'=> $_POST['mobile'],            
              'acc_address' => $_POST['address'] ,
	        'acc_country' => $_POST['country'], 
	          'acc_state' => $_POST['state'] ,
	            'acc_city' => $_POST['city'] ,
	              'acc_zip' => $_POST['zipcode'], 
	                'acc_map' => $_POST['map'] ,
  	                  'acc_fb' => $_POST['facebook'], 
		             'acc_tw' => $_POST['twitter'], 
		               'acc_go' => $_POST['google'], 
		                  'acc_linked' => $_POST['linkdein'],  
		                    'acc_youtube' => $_POST['youtube'],  
		                     'acc_pinterest' => $_POST['pineterest'], 
		                       	 'acc_dribble' => $_POST['dribble'], 
				            'acc_rss' => $_POST['rss'] 
              
    );
        $condition = "acc_id= '1'";
        $db->update($table ,$arr,$condition);   
 $db->redirect("home.php?pages=add-account");
}

}
	      
?>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB21baMnSAvRrcZ3IKymum3_vJFN4hm7xU&v=3.exp&libraries=places"></script>

<script>
function initialize() {

var input = document.getElementById('address');
var options = {
  types: ['(geocode)'],

};

var autocomplete = new google.maps.places.Autocomplete(input,options);
}
google.maps.event.addDomListener(window, 'load', initialize);
</script>




  <!-- Page Content Start -->
  <!-- ================== -->

<div class="wraper container-fluid">
    <div class="page-title">
      <h3 class="title">Add Account</h3>
    </div>
    <div class="row">
      <div class="col-sm-12">
        <div class="panel panel-default">

          <div class="panel-body">
            <div class="form">
              <form class="cmxform form-horizontal tasi-form"  method="post"  enctype="multipart/form-data" action="">

      
                <div class="form-group ">
                  <label class="control-label col-lg-2">Account Site Title*</label>
                  <div class="col-lg-10">
                  <input type="text" class="form-control"  placeholder="Account Site URL" value="<?php echo $list[0]['acc_site_title'];?>"  name="acc_site_url" id="acc_site_url" >
                  </div>
                </div>
                
                <div class="form-group ">
                  <label class="control-label col-lg-2">Account Email*</label>
                  <div class="col-lg-10">
                  <input type="email" class="form-control"  placeholder="Account Email" value="<?php echo $list[0]['acc_email'];?>"  name="acc_email" id="acc_email" >
                  </div>
                </div>
                
                <div class="form-group ">
                  <label class="control-label col-lg-2">Account URL*</label>
                  <div class="col-lg-10">
                  <input type="text" class="form-control"  placeholder="Account URL" value="<?php echo $list[0]['acc_url'];?>"  name="account_url" id="account_url" >
                  </div>
                </div>
                
                <div class="form-group ">
                  <label class="control-label col-lg-2">Phone*</label>
                  <div class="col-lg-10">
                  <input type="text" class="form-control"  placeholder="Enter Phone Number" value="<?php echo $list[0]['acc_phone'];?>"  name="phone" id="phone" >
                  </div>
                </div>
                
                
                <div class="form-group ">
                  <label class="control-label col-lg-2">Mobile*</label>
                  <div class="col-lg-10">
                  <input type="text" class="form-control"  placeholder="Enter Mobile Number" value="<?php echo $list[0]['acc_mobile'];?>"  name="mobile" id="mobile" >
                  </div>
                </div>


                <div class="form-group ">
                  <label for="username" class="control-label col-lg-2">Upload Image*</label>
                  <div class="col-lg-10">
		 <div class="input-group image-preview">
                <input type="text" class="form-control image-preview-filename"  disabled="disabled" >
                <span class="input-group-btn">
                  
                    <button type="button" class="btn btn-default image-preview-clear" style="display:none;">
                        <span class="glyphicon glyphicon-remove"></span> Clear
                    </button>
                  
                    <div class="btn btn-default image-preview-input">
                        <span class="glyphicon glyphicon-folder-open"></span>
                        <span class="image-preview-input-title">Browse</span>
                        <input type="file"  accept="image/png, image/jpeg, image/gif" name="acc_image" id="acc_image"  /> <!-- rename it -->
                    </div>
                </span>
              </div>
             </div>
            </div>
                
                
                <div class="form-group ">
                  <label class="control-label col-lg-2">Address*</label>
                  <div class="col-lg-10">
                    <input type="text" class="form-control" placeholder="Enter Address" value="<?php echo $list[0]['acc_address'];?>"  name="address" id="address" ></textarea>
                  </div>
                </div>


              <div class="form-group ">
                  <label class="control-label col-lg-2">Country*</label>
                  <div class="col-lg-10">
                    <input type="text"  class="form-control" placeholder="Enter Country" value="<?php echo $list[0]['acc_country'];?>"  name="country" id="country" ></textarea>
                  </div>
                </div>
               
            
               
               <div class="form-group ">
                  <label class="control-label col-lg-2">State*</label>
                  <div class="col-lg-10">
                    <input type="text"  class="form-control" placeholder="Enter State" value="<?php echo $list[0]['acc_state'];?>"  name="state" id="state" ></textarea>
                  </div>
                </div>
                
               <div class="form-group ">
                  <label class="control-label col-lg-2">City*</label>
                  <div class="col-lg-10">
                    <input type="text"  class="form-control" placeholder="Enter City" value="<?php echo $list[0]['acc_city'];?>"  name="city" id="city" ></textarea>
                  </div>
                </div>
                  
               <div class="form-group ">
                  <label class="control-label col-lg-2">ZipCode*</label>
                  <div class="col-lg-10">
                    <input type="text"  class="form-control" placeholder="Enter Zipcode" value="<?php echo $list[0]['acc_zip'];?>"  name="zipcode" id="zipcode" ></textarea>
                  </div>
                </div>
                
                <div class="form-group ">
                  <label class="control-label col-lg-2">Map*</label>
                  <div class="col-lg-10">
                    <input type="text"  class="form-control" placeholder="" value="<?php echo $list[0]['acc_map'];?>"  name="map" id="map" ></textarea>
                  </div>
                </div>
                 
                  <div class="form-group ">
                  <label class="control-label col-lg-2">Facebook Account*</label>
                  <div class="col-lg-10">
                    <input type="text"  class="form-control" placeholder="Enter Facebook Account Details" value="<?php echo $list[0]['acc_fb'];?>"  name="facebook" id="facebook" ></textarea>
                  </div>
                </div>   
                
                <div class="form-group ">
                  <label class="control-label col-lg-2">Twitter Account*</label>
                  <div class="col-lg-10">
                    <input type="text"  class="form-control" placeholder="Enter Twitter Account Details" value="<?php echo $list[0]['acc_tw'];?>"  name="twitter" id="twitter" ></textarea>
                  </div>
                </div>
                
                  <div class="form-group ">
                  <label class="control-label col-lg-2">Google Account*</label>
                  <div class="col-lg-10">
                    <input type="text"  class="form-control" placeholder="Enter Google Account Details" value="<?php echo $list[0]['acc_go'];?>"  name="google" id="google" ></textarea>
                  </div>
                </div>
                
                
                <div class="form-group ">
                  <label class="control-label col-lg-2">Linkdein Account*</label>
                  <div class="col-lg-10">
                    <input type="text"  class="form-control" placeholder="Enter linkdein Account Details" value="<?php echo $list[0]['acc_linked'];?>"  name="linkdein" id="linkdein" ></textarea>
                  </div>
                </div>
                
                
                 <div class="form-group ">
                  <label class="control-label col-lg-2">Youtube Account*</label>
                  <div class="col-lg-10">
                    <input type="text"  class="form-control" placeholder="Enter Youtube Account Details" value="<?php echo $list[0]['acc_youtube'];?>"  name="youtube" id="youtube" ></textarea>
                  </div>
                </div>
                
                <div class="form-group ">
                  <label class="control-label col-lg-2">Pineterest Account*</label>
                  <div class="col-lg-10">
                    <input type="text"  class="form-control" placeholder="Enter Pineterest Account Details" value="<?php echo $list[0]['acc_pinterest'];?>"  name="pineterest" id="pineterest" ></textarea>
                  </div>
                </div>
                
                <div class="form-group ">
                  <label class="control-label col-lg-2">Dribble Account*</label>
                  <div class="col-lg-10">
                    <input type="text"  class="form-control" placeholder="Enter Dribble Account Details" value="<?php echo $list[0]['acc_dribble'];?>"  name="dribble" id="dribble" ></textarea>
                  </div>
                </div>
               
                <div class="form-group ">
                  <label class="control-label col-lg-2">RSS Account*</label>
                  <div class="col-lg-10">
                    <input type="text"  class="form-control" placeholder="Enter RSS Account Details"  value="<?php echo $list[0]['acc_rss'];?>"  name="rss" id="rss" ></textarea>
                  </div>
                </div>
                 
                <div class="form-group">
                  <div class="col-lg-offset-2 col-lg-10">
                   
                    	<input type="submit" class=" btn btn-info col-md-4 col-sm-6 col-xs-12" id="save" name="save" value="Save" >
										
                  </div>
                </div>





              </form>
            </div>
            <!-- .form -->

          </div>
          <!-- panel-body -->
        </div>
        <!-- panel -->
      </div>
      <!-- col -->

    </div>
    <!-- End row -->

  </div>

  <!-- Page Content Ends -->
  <!-- ================== -->

</section>
<!-- Main Content Ends -->

</body>
</html>
<script type="text/javascript" src="js/wickedpicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css">

        <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>

<link rel="stylesheet" type="text/css" href="css/wickedpicker.min.css">



<script>
   $(function() {
       $('.start_time').datepicker({

         format: "yyyy-mm-dd"

     });
   });
   $(function() {
       $('.end_date').datepicker({

        format: "yyyy-mm-dd" });
   });

    $('.start').wickedpicker();

     $('.end').wickedpicker();

$('.time').bootstrapMaterialDatePicker({ date: false });


</script>
