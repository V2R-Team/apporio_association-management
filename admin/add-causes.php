<?php
include('common.php');
include_once '../apporioconfig/start_up.php';

if($_SERVER['REQUEST_METHOD'] == "POST") { 

 if($_FILES['cause_image']['name'] != "") {
      $ext = substr($_FILES['cause_image']['name'],-4);
      if($ext == ".jpg" || $ext == ".png" || $ext == ".gif" || $ext == "jpeg") {
        $ext = ($ext == "jpeg") ? ".jpg" : $ext;
        $dir    = "../uploads/cause";
        if(!is_dir($dir) || !file_exists($dir)) mkdir($dir,0755);
       $id= uniqid();
        $filename = 'uploads/cause/'.$id.$ext;
        $filepath = "../".$filename;
        if(file_exists($filepath)) unlink($filepath);         
        copy($_FILES['cause_image']['tmp_name'],$filepath);
      }
      
      $arr = array(
      'causes_category_id'=> $_POST['causes_category_id'],
      'causes_name'    => $_POST['cause_name'],
      'causes_image' => $filename,
      'causes_description' => $_POST['causes_description']  ,
      'cause_name_other'    => $_POST['cause_name_other'],
      'causes_description_other'    => $_POST['causes_description_other'],   
    );
    
       $db->insert(causes,$arr,true);          
    }
    $db->redirect("home.php?pages=add-causes");
  }
?>
  <!-- Page Content Start -->
  <!-- ================== -->

<div class="wraper container-fluid">
    <div class="page-title">
      <h3 class="title">Add Causes</h3>
    </div>
    <div class="row">
      <div class="col-sm-12">
        <div class="panel panel-default">

          <div class="panel-body">
            <div class="form">
              <form class="cmxform form-horizontal tasi-form"  method="post"  enctype="multipart/form-data" action="">

                <div class="form-group ">
                  <label class="control-label col-lg-2">Select Cause Category *</label>
                  <div class="col-lg-10">
                     <select class="form-control" name="causes_category_id" id="causes_category_id" required>
                        <option value="">--Please Select Category --</option>
                        <?php $event_category = $db->db_get_array("SELECT causes_category_id,causes_category_name FROM ".causes_category." WHERE causes_category_status= 1" );
                          foreach ($event_category as $event_categorys) {?>
                           <option  value="<?php echo $event_categorys['causes_category_id']?>"><?php echo $event_categorys['causes_category_name']?></option>
                         <?php }?>
                    </select>
                  </div>
                </div>
                <div class="form-group ">
                  <label class="control-label col-lg-2">Causes Name*</label>
                  <div class="col-lg-10">
                  <input type="text" class="form-control"  placeholder="Causes Name" name="cause_name" id="cause_name" required>
                  </div>
                </div>
                <div class="form-group ">
                  <label class="control-label col-lg-2">Causes Name in french*</label>
                  <div class="col-lg-10">
                  <input type="text" class="form-control"  placeholder="Causes Name (french)" name="cause_name_other" id="cause_name_other" required>
                  </div>
                </div>


                <div class="form-group ">
                  <label for="username" class="control-label col-lg-2">Upload Image*</label>
                  <div class="col-lg-10">
		 <div class="input-group image-preview">
                <input type="text" class="form-control image-preview-filename"  disabled="disabled" required>
                <span class="input-group-btn">
                  
                    <button type="button" class="btn btn-default image-preview-clear" style="display:none;">
                        <span class="glyphicon glyphicon-remove"></span> Clear
                    </button>
                  
                    <div class="btn btn-default image-preview-input">
                        <span class="glyphicon glyphicon-folder-open"></span>
                        <span class="image-preview-input-title">Browse</span>
                        <input type="file"  accept="image/png, image/jpeg, image/gif" name="cause_image" id="cause_image"  required/> <!-- rename it -->
                    </div>
                </span>
              </div>
             </div>
            </div>
                
                
                <div class="form-group ">
                  <label class="control-label col-lg-2">Causes Description *</label>
                  <div class="col-lg-10">
                    <textarea class="form-control" placeholder="Causes Description" name="causes_description" id="causes_description" required></textarea>
                  </div>
                </div>
                
                 <div class="form-group ">
                  <label class="control-label col-lg-2">Causes Description in french *</label>
                  <div class="col-lg-10">
                    <textarea class="form-control" placeholder="Causes Description (french)" name="causes_description_other" id="causes_description_other" required></textarea>
                  </div>
                </div>



               

               

                    
                <div class="form-group">
                  <div class="col-lg-offset-2 col-lg-10">
                    <?php if(!isset($_GET['edit_id'])) { ?>
                    	<input type="submit" class=" btn btn-info col-md-4 col-sm-6 col-xs-12" id="save" name="save" value="Save" >
										<?php } else { ?>
											<input type="submit" class=" btn btn-info col-md-4 col-sm-6 col-xs-12" id="save" name="save" value="Update" >
										<?php } ?>
                  </div>
                </div>





              </form>
            </div>
            <!-- .form -->

          </div>
          <!-- panel-body -->
        </div>
        <!-- panel -->
      </div>
      <!-- col -->

    </div>
    <!-- End row -->

  </div>

  <!-- Page Content Ends -->
  <!-- ================== -->

</section>
<!-- Main Content Ends -->

</body>
</html>
<script type="text/javascript" src="js/wickedpicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css">

        <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>

<link rel="stylesheet" type="text/css" href="css/wickedpicker.min.css">



<script>
   $(function() {
       $('.start_time').datepicker({

         format: "yyyy-mm-dd"

     });
   });
   $(function() {
       $('.end_date').datepicker({

        format: "yyyy-mm-dd" });
   });

    $('.start').wickedpicker();

     $('.end').wickedpicker();

$('.time').bootstrapMaterialDatePicker({ date: false });


</script>
