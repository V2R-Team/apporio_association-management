<?php 
include('common.php'); 
include_once '../apporioconfig/start_up.php';
if ($admininfos['membership_add_status'] != 1) {
  $db->redirect("home.php?pages=dashboard");
}
if(isset($_GET['edit_id'])) {
    $membership_data = $db->db_get_array("SELECT * FROM ".TABLE_MEMBERSHIP." WHERE membership_id='".$_GET['edit_id']."' ");
    foreach($membership_data as $membership_datas);
    // echo "<pre>";
    // print_r($membership_datas);
    // echo "</pre>";

}

if($_SERVER['REQUEST_METHOD'] == "POST") { 
  if(!isset($_GET['edit_id'])) { 
    $arr = array(
      'membership_country_id'         => $_POST['membership_country_id'],
      'membership_city_id'            => $_POST['membership_city_id'],
      'membership_name'               => $_POST['membership_name'],
      'membership_name_other'         => $_POST['membership_name_other'],
      'madmin_id'        => $muser_id,
      'membership_duration'           => $_POST['membership_duration'],
      'membership_price'              => $_POST['membership_price'],
      'membership_terms_conditions'   => $_POST['membership_terms_conditions'],
       'membership_terms_conditions_other'   => $_POST['membership_terms_conditions_other'],
      'membership_notes'              => $_POST['membership_notes'],
     
    );
    $insert_id =  $db->insert(TABLE_MEMBERSHIP,$arr,true);
    
    $db->redirect("home.php?pages=view-membership");

  }else{

    $arr = array(
      'membership_country_id'         => $_POST['membership_country_id'],
      'membership_city_id'            => $_POST['membership_city_id'],
      'membership_name'               => $_POST['membership_name'],
       'membership_name_other'          => $_POST['membership_name_other'],
      'membership_duration'           => $_POST['membership_duration'],
      'membership_price'              => $_POST['membership_price'],
      'membership_terms_conditions'   => $_POST['membership_terms_conditions'],
       'membership_terms_conditions_other'   => $_POST['membership_terms_conditions_other'],
      'membership_notes'              => $_POST['membership_notes'],
     
    );

    $condition = "membership_id='".$_GET['edit_id']."' ";
    $db->update(TABLE_MEMBERSHIP,$arr,$condition,true);

    $db->redirect("home.php?pages=view-membership");
  }
}
?>

<script type="text/javascript">

function change_clty(country_id){
  $.ajax({
   url:"ajax.php?action=change_city&country_id="+country_id,
    success:function(response){
      $("#membership_city_id").html(response);
    }
  });
 }


function validatememebership() {
 if(document.getElementById('membership_country_id').value == "")
  {
  alert("Please Select Country");
  document.getElementById('membership_country_id').focus();
  return false;
  }

  if(document.getElementById('membership_city_id').value == "")
  {
  alert("Please Select City");
  document.getElementById('membership_city_id').focus();
  return false;
  }

  if(document.getElementById('membership_name').value == "")
  {
  alert("Please Enter Membership Name");
  document.getElementById('membership_name').focus();
  return false;
  }


  if(document.getElementById('membership_duration').value == "")
  {
  alert("Please Select Duration");
  document.getElementById('membership_duration').focus();
  return false;
  }

  if(document.getElementById('membership_price').value == "")
  {
  alert("Please Enter Price");
  document.getElementById('membership_price').focus();
  return false;
  }

  if(document.getElementById('membership_terms_conditions').value == "")
  {
  alert("Please Enter Terms & Conditions");
  document.getElementById('membership_terms_conditions').focus();
  return false;
  }

   if(document.getElementById('membership_notes').value == "")
  {
  alert("Please Enter Others Notes");
  document.getElementById('membership_notes').focus();
  return false;
  }
 return true;
}
</script>

  <!-- Page Content Start --> 
  <!-- ================== -->
  
<div class="wraper container-fluid">
    <div class="page-title">
      <h3 class="title">Add Membership</h3>
    </div>
    <div class="row">
      <div class="col-sm-12">
        <div class="panel panel-default">
          
          <div class="panel-body">
            <div class="form" >
              <form class="cmxform form-horizontal tasi-form"  method="post"  onSubmit="return validatememebership()" action="">
 
                <div class="form-group ">
                  <label class="control-label col-lg-2">Select Country *</label>
                  <div class="col-lg-10">
                     <select class="form-control" name="membership_country_id" id="membership_country_id" onchange="change_clty(this.value)" >
                        <option value="">--Please Select Country --</option>
                        <?php $country_name = $db->db_get_array("SELECT country_id,country_name FROM ".TABLE_COUNTRY." WHERE madmin_id='".$muser_id."' AND country_status = 1" ); 
                          foreach ($country_name as $country_names) {?>
                           <option <?php if($membership_datas['membership_country_id'] == $country_names['country_id']) { echo "selected=selected"; } ?> value="<?php echo $country_names['country_id']?>"><?php echo $country_names['country_name']?></option>
                         <?php }?>
                    </select>
              </div>
                </div>   


                  <div class="form-group ">
                  <label class="control-label col-lg-2">Select City*</label>
                  <div class="col-lg-10">
                     <?php if(!isset($_GET['edit_id'])) {?>
                       <select class="form-control" name="membership_city_id" id="membership_city_id" >
                          <option value="">--Please Select City --</option>
                       </select>
                     <?php } else { ?>
                       <select class="form-control" name="membership_city_id" id="membership_city_id"  >
                          <option value="">--Please Select Country --</option>
                          <?php $city_name = $db->db_get_array("SELECT city_id,city_name FROM ".TABLE_CITY." WHERE city_country_id = ".$membership_datas['membership_country_id']." " );
                            foreach ($city_name as $city_names) {?>
                             <option <?php if($membership_datas['membership_city_id'] == $city_names['city_id']) { echo "selected=selected"; } ?> value="<?php echo $city_names['city_id']?>"><?php echo $city_names['city_name']?></option>
                           <?php }?>
                       </select>
                     <?php } ?>
              </div>
                </div>            
         

               <div class="form-group ">
                  <label class="control-label col-lg-2">Membership Name*</label>
                  <div class="col-lg-10">
                    <input type="text" class="form-control" placeholder="Membership Name" name="membership_name" id="membership_name" value="<?php echo $membership_datas['membership_name']?>">
                  </div>
                </div>
                
                <div class="form-group ">
                  <label class="control-label col-lg-2">Membership Name in french*</label>
                  <div class="col-lg-10">
                    <input type="text" class="form-control" placeholder="Membership Name (french)" name="membership_name_other" id="membership_name_other" value="<?php echo $membership_datas['membership_name_other']?>">
                  </div>
                </div>



                 <div class="form-group ">
                  <label class="control-label col-lg-2">Select Duration*</label>
                  <div class="col-lg-10">
                     <select class="form-control" name="membership_duration" id="membership_duration" >
                        <option>--Please Select Duration --</option>
                          <option <?php if($membership_datas['membership_duration'] == "weekly") { echo "selected=selected"; } ?> value="weekly">Weekly</option>
                          <option <?php if($membership_datas['membership_duration'] == "monthly") { echo "selected=selected"; } ?> value="monthly">Monthly</option>
                          <option <?php if($membership_datas['membership_duration'] == "half-yearly") { echo "selected=selected"; } ?> value="half-yearly">Half Yearly</option>
                          <option <?php if($membership_datas['membership_duration'] == "yearly") { echo "selected=selected"; } ?> value="yearly">Yearly</option>
                        </select>
                  </div>
                </div> 
                
                
                
               <div class="form-group ">
                  <label class="control-label col-lg-2">Price*</label>
                  <div class="col-lg-10">
                    <input type="text" class="form-control" placeholder="Enter Price" name="membership_price" id="membership_price" value="<?php echo $membership_datas['membership_price']?>">
                  </div>
                </div>


                <div class="form-group ">
                  <label class="control-label col-lg-2">Terms & Conditions *</label>
                  <div class="col-lg-10">
                    <textarea class="form-control" placeholder="Terms & Conditions " name="membership_terms_conditions" id="membership_terms_conditions"><?php echo $membership_datas['membership_terms_conditions']?></textarea>
                  </div>
                </div>
                
                
                <div class="form-group ">
                  <label class="control-label col-lg-2">Terms & Conditions in french*</label>
                  <div class="col-lg-10">
                    <textarea class="form-control" placeholder="Terms & Conditions (french)" name="membership_terms_conditions_other" id="membership_terms_conditions_other"><?php echo $membership_datas['membership_terms_conditions_other']?></textarea>
                  </div>
                </div>



                <div class="form-group ">
                  <label class="control-label col-lg-2">Others Notes *</label>
                  <div class="col-lg-10">
                    <textarea class="form-control" placeholder="Others Notes" name="membership_notes" id="membership_notes"><?php echo $membership_datas['membership_notes']?></textarea>
                  </div>
                </div>
                
                


                
                
                
                
                <div class="form-group">
                  <div class="col-lg-offset-2 col-lg-10">
                   
                    <input type="submit" class=" btn btn-info col-md-4 col-sm-6 col-xs-12" id="save" name="save" value="Save" >
                  </div>
                </div>
              </form>
            </div>
            <!-- .form --> 
            
          </div>
          <!-- panel-body --> 
        </div>
        <!-- panel --> 
      </div>
      <!-- col --> 
      
    </div>
    <!-- End row --> 
    
  </div>
  
  <!-- Page Content Ends --> 
  <!-- ================== --> 
  
</section>
<!-- Main Content Ends -->

</body>
</html>
