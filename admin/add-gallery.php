<?php 
include('common.php'); 
include_once '../apporioconfig/start_up.php';
if ($admininfos['gallery_add_status'] != 1) {
  $db->redirect("home.php?pages=dashboard");
}

if(isset($_GET['edit_id'])) {
    $gallery_data = $db->db_get_array("SELECT * FROM ".TABLE_GALLERY." WHERE gallery_id='".$_GET['edit_id']."' ");
    foreach($gallery_data as $gallery_datas);
    // echo "<pre>";
    // print_r($gallery_datas);
    // echo "</pre>";

}

if($_SERVER['REQUEST_METHOD'] == "POST") { 
  if(!isset($_GET['edit_id'])) {  
    $arr = array(
      'gallery_image_caption'    => $_POST['gallery_image_caption'],
      'gallery_image_caption_other'    => $_POST['gallery_image_caption_other'],
      'gallery_video'           => $_POST['gallery_video'],
      'gallery_video_caption'    => $_POST['gallery_video_caption'],
      'gallery_video_caption_other'    => $_POST['gallery_video_caption_other'],
      'madmin_id'        => $muser_id,
    );
    $insert_id =  $db->insert(TABLE_GALLERY,$arr,true);
    
    $condition = " `gallery_id` = '".$insert_id."' ";

    if($_FILES['gallery_image']['name'] != "") {
      $ext = substr($_FILES['gallery_image']['name'],-4);
      if($ext == ".jpg" || $ext == ".png" || $ext == ".gif" || $ext == "jpeg") {
        $ext = ($ext == "jpeg") ? ".jpg" : $ext;
        $dir    = "../uploads/gallery_image";
        if(!is_dir($dir) || !file_exists($dir)) mkdir($dir,0755);
        $filename = 'uploads/gallery_image/'.$insert_id.$ext;
        $filepath = "../".$filename;
        if(file_exists($filepath)) unlink($filepath);         
        copy($_FILES['gallery_image']['tmp_name'],$filepath);
      }
      $db->update(TABLE_GALLERY,array('gallery_image' => $filename),$condition);
    }
    $db->redirect("home.php?pages=view-gallery");
  }else{
    $arr = array(
      'gallery_image_caption'    => $_POST['gallery_image_caption'],
       'gallery_image_caption_other'    => $_POST['gallery_image_caption_other'],
      'gallery_video'           => $_POST['gallery_video'],
      'gallery_video_caption'    => $_POST['gallery_video_caption'],
      'gallery_video_caption_other'    => $_POST['gallery_video_caption_other']
    );

    $condition = "gallery_id='".$_GET['edit_id']."' ";

    $db->update(TABLE_GALLERY,$arr,$condition,true);

    if($_FILES['gallery_image']['name'] != "") {
      $ext = substr($_FILES['gallery_image']['name'],-4);
      if($ext == ".jpg" || $ext == ".png" || $ext == ".gif" || $ext == "jpeg") {
        $ext = ($ext == "jpeg") ? ".jpg" : $ext;
        $dir    = "../uploads/gallery_image";
        if(!is_dir($dir) || !file_exists($dir)) mkdir($dir,0755);
        $filename = 'uploads/gallery_image/'.$insert_id.$ext;
        $filepath = "../".$filename;
        if(file_exists($filepath)) unlink($filepath);         
        copy($_FILES['gallery_image']['tmp_name'],$filepath);
      }
      $db->update(TABLE_GALLERY,array('gallery_image' => $filename),$condition);
    }
    $db->redirect("home.php?pages=view-gallery");
  }
}

?>

<script type="text/javascript">

function validategallery() {
 if(document.getElementById('gallery_image').value == "")
  {
  alert("Please Select Image");
  document.getElementById('gallery_image').focus();
  return false;
  }

  if(document.getElementById('gallery_image_caption').value == "")
  {
  alert("Please Enter Image Caption");
  document.getElementById('gallery_image_caption').focus();
  return false;
  }

  if(document.getElementById('gallery_video').value == "")
  {
  alert("Please Enter Video Link");
  document.getElementById('gallery_video').focus();
  return false;
  }


  if(document.getElementById('gallery_video_caption').value == "")
  {
  alert("Please Enter Video Caption");
  document.getElementById('gallery_video_caption').focus();
  return false;
  }
 return true;
}
</script>

  <!-- Page Content Start --> 
  <!-- ================== -->
  
<div class="wraper container-fluid">
    <div class="page-title">
      <h3 class="title">News Management</h3>
    </div>
    <div class="row">
      <div class="col-sm-12">
        <div class="panel panel-default">
          
          <div class="panel-body">
            <div class="form">
              <form class="cmxform form-horizontal tasi-form"  method="post"  enctype="multipart/form-data" onSubmit="return validategallery()" action="">
                 <div class="form-group ">
                  <label for="username" class="control-label col-lg-2">Upload Image*</label>
                  <div class="col-lg-10">

                    <div class="input-group image-preview">
                <input type="text" class="form-control image-preview-filename" value="<?php echo $gallery_datas['gallery_image']?>" disabled="disabled"> <!-- don't give a name === doesn't send on POST/GET -->
                <span class="input-group-btn">
                    <!-- image-preview-clear button -->
                    <button type="button" class="btn btn-default image-preview-clear" style="display:none;">
                        <span class="glyphicon glyphicon-remove"></span> Clear
                    </button>
                    <!-- image-preview-input -->
                    <div class="btn btn-default image-preview-input">
                        <span class="glyphicon glyphicon-folder-open"></span>
                        <span class="image-preview-input-title">Browse</span>
                        <input type="file" value="<?php echo $gallery_datas['gallery_image']?>" accept="image/png, image/jpeg, image/gif" name="gallery_image" id="gallery_image"  /> <!-- rename it -->
                    </div>
                </span>
              </div>
                  </div>
                </div>
                  <div class="form-group ">
                  <label class="control-label col-lg-2">Add Image Caption  *</label>
                  <div class="col-lg-10">
                    <textarea class="form-control" placeholder="Add Image Caption " name="gallery_image_caption" id="gallery_image_caption" ><?php echo $gallery_datas['gallery_image_caption']?></textarea>
                  </div>
                </div>
                
                <div class="form-group ">
                  <label class="control-label col-lg-2">Add Image Caption in french*</label>
                  <div class="col-lg-10">
                    <textarea class="form-control" placeholder="Add Image Caption (french)" name="gallery_image_caption_other" id="gallery_image_caption_other" ><?php echo $gallery_datas['gallery_image_caption_other']?></textarea>
                  </div>
                </div>
                
                <div class="form-group ">
                  <label class="control-label col-lg-2">Paste Video Link*</label>
                  <div class="col-lg-10">
                    <input type="text" class="form-control"  value="<?php echo $gallery_datas['gallery_video']?>" placeholder="Paste Youtube Link" name="gallery_video" id="gallery_video" >
                  </div>
                </div>

                <div class="form-group ">
                  <label class="control-label col-lg-2">Add Video Caption *</label>
                  <div class="col-lg-10">
                    <textarea class="form-control" placeholder="Add Video Caption" name="gallery_video_caption" id="gallery_video_caption" ><?php echo $gallery_datas['gallery_video_caption']?></textarea>
                  </div>
                </div>
                
                <div class="form-group ">
                  <label class="control-label col-lg-2">Add Video Caption in french*</label>
                  <div class="col-lg-10">
                    <textarea class="form-control" placeholder="Add Video Caption (french)" name="gallery_video_caption_other" id="gallery_video_caption_other" ><?php echo $gallery_datas['gallery_video_caption_other']?></textarea>
                  </div>
                </div>
            
                <div class="form-group">
                  <div class="col-lg-offset-2 col-lg-10">
                    <input type="submit" class=" btn btn-info col-md-4 col-sm-6 col-xs-12" id="save" name="save" value="Save" >
                  </div>
                </div>

              </form>
            </div>
            <!-- .form --> 
            
          </div>
          <!-- panel-body --> 
        </div>
        <!-- panel --> 
      </div>
      <!-- col --> 
      
    </div>
    <!-- End row --> 
    
  </div>
  
  <!-- Page Content Ends --> 
  <!-- ================== --> 
  
</section>
<!-- Main Content Ends -->

</body>
</html>
