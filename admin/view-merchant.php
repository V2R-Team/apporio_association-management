<?php
include('common.php');


$table = TABLE_ADMIN;

if(isset($_GET['status']) && isset($_GET['id'])) {
  $arr = array(
    'admin_status' => $_GET['status']
  );
  $condition = " admin_id = '".$_GET['id']."' ";
  $db->update($table,$arr,$condition);
  $db->redirect("home.php?pages=view-merchant");
}
if(isset($_GET['delid'])) {
  $condition = " admin_id = '".$_GET['delid']."'";
  $db->delete($table,$condition);
  $db->redirect("home.php?pages=view-merchant");
}
if(isset($_POST['command']) && $_POST['command'] == "m delete") {
  $ids = implode(",",$_POST['chk']);
  $condition = " admin_id IN ($ids) ";
  $db->delete($table,$condition);
  $db->redirect("home.php?pages=view-merchant");
}

?>
<script>
function checkall(a) {
  if(a.checked) {
    $("input[type='checkbox']").prop("checked",true);
  } else {
    $("input[type='checkbox']").prop("checked",false);
  }
}
function uncheck() {
  var tot_ch = $("input[type='checkbox']").length;
  var chek_ch = $("input[type='checkbox']:checked").length;
  if(tot_ch == chek_ch + 1 && document.getElementById('main_ch').checked == false) {
    $("#main_ch").prop("checked",true);
  } else {
    $("#main_ch").prop("checked",false);
  }
}
function godelete() {
  var tot_chk = $("input[type='checkbox']:checked").length;
  if(tot_chk > 0) {
    if(confirm("Are You Want To Delete Selected Records!")) {
      if(confirm("Are You Sure To Delete Permanently Records!")) {
        //alert('test');
          document.form1.command.value = "m delete";
          document.form1.submit();
      }
    }
  } else {
    alert("Please Select Atlease One Record To Delete!");
  }
}
function single_delete(id) {
  if(confirm("Are You Want To Delete This Records!")) {
    if(confirm("Are You Sure To Delete Permanently Records!")) {
      window.location = "home.php?pages=view-merchant&delid="+id;
    }
  }
}
</script>

<!-- Page Content Start -->
<!-- ================== -->
<form method="post" name="form1">
<div class="wraper container-fluid">
  <div class="page-title">
    <h3 class="title">View Country</h3>admin
      <a href="javascript:godelete()"><button type="button" name="delete" value="delete" class="btn btn-danger glyphicon glyphicon-trash"></button></a>
  </div>

  <div class="row">
    <div class="col-md-12">
      <div class="panel panel-default">

        <div class="panel-body">
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12 mobtbl">

                <input type="hidden" name="command" value="">
                <table id="datatable" class="table table-striped table-bordered table-responsive">
                  <thead>
                    <tr>
                      <th width="10%">
                        <label class="option block mn">
                          <input type="checkbox" id="main_ch" onClick="checkall(this)" >
                          <span class=""></span> Select
                        </label>
                      </th>
                      <th width="5%">S.No</th>
                      <th>Merchant User Name</th>
                      <th>Merchant Password</th>

                      <th width="8%">Status</th>

                    </tr>
                  </thead>
                  <tbody>
                    <?php $country = $db->db_get_array("SELECT * FROM ".TABLE_ADMIN." where admin_role='merchant_user'");
                      $sn = 0;
                      foreach ($country as $countrys) {
                        $sn++;
                    ?>
                      <tr>

                        <td>
                          <label class="option block mn" style="width: 55px;">
                             <input type="checkbox" name="chk[]" value="<?php echo $countrys['admin_id']?>" onClick="uncheck()" >
                             <span class="checkbox mn"></span>
                          </label>
                        </td>

                        <td><?php echo $sn;?></td>


                        <td> <a title="Edit" href="home.php?pages=add-merchant&edit_id=<?php echo $countrys['admin_id']; ?>"><?php echo $countrys['admin_fname']?></a> </td>


                         <td><?php echo $countrys['admin_password']?></td>



                          <?php if ($countrys['admin_status'] != 0) {?>
                            <td class="text-center">
                              <a href="home.php?pages=view-merchant&status=0&id=<?php echo $countrys['admin_id']?>" class="" title="Active">
                              <button type="button" class="btn btn-success br2 btn-xs fs12 activebtn" > Active
                              </button></a>
                            </td>
                          <?php } else { ?>
                            <td class="text-center">
                              <a href="home.php?pages=view-merchant&status=1&id=<?php echo $countrys['admin_id']?>" class="" title="Deactive">
                                <button type="button" class="btn btn-danger  br2 btn-xs fs12 dropdown-toggle" > Deactive
                                </button></a>
                            </td>
                          <?php } ?>
                      </tr>
                    <?php }?>

                   </tbody>
                </table>

            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- End row -->

</div>
</form>


<!-- Page Content Ends -->
<!-- ================== -->

</section>
<!-- Main Content Ends -->

</body></html>
