
		<?php include('common.php');  ?>
	
<div class="wraper container-fluid">
    <div class="page-title">
      <h3 class="title">404 Error</h3>
    </div>
    <div class="row">
      <div class="col-sm-12">
        <div class="panel panel-default">
          
          <div class="panel-body">
             <div class="center-block mt50 mw800" style=" text-align: center;">
              <h1 class="error-title" style="font-size: 220px;line-height: 250px;color:#4a89dc"> 404! </h1>
              <h2 class="error-subtitle">Page Not Found.</h2>
            </div>
            <!-- .form --> 
            
          </div>
          <!-- panel-body --> 
        </div>
        <!-- panel --> 
      </div>
      <!-- col --> 
      
    </div>
    <!-- End row --> 
    
  </div>
  
  <!-- Page Content Ends --> 
  <!-- ================== --> 
  
</section>
<!-- Main Content Ends -->

</body>
</html>


