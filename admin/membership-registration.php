<?php
include('common.php');

$table = TABLE_MEMBERSHIP_REGISTRATION;

if(isset($_GET['status']) && isset($_GET['id'])) {
  $arr = array(
    'membership_reg_status' => $_GET['status']
  );
  $condition = " membership_registration_form_id = '".$_GET['id']."' ";

  $db->update($table,$arr,$condition);

  $db->redirect("home.php?pages=membership-registration");
}
if(isset($_GET['delid'])) {
  $condition = " membership_registration_form_id = '".$_GET['delid']."'";
  $db->delete($table,$condition);
  $db->redirect("home.php?pages=membership-registration");
}
if(isset($_POST['command']) && $_POST['command'] == "m delete") {
  $ids = implode(",",$_POST['chk']);
  $condition = " membership_registration_form_id IN ($ids) ";
  $db->delete($table,$condition);
  $db->redirect("home.php?pages=membership-registration");
}

?>
<script>
function checkall(a) {
  if(a.checked) {
    $("input[type='checkbox']").prop("checked",true);
  } else {
    $("input[type='checkbox']").prop("checked",false);
  }
}
function uncheck() {
  var tot_ch = $("input[type='checkbox']").length;
  var chek_ch = $("input[type='checkbox']:checked").length;
  if(tot_ch == chek_ch + 1 && document.getElementById('main_ch').checked == false) {
    $("#main_ch").prop("checked",true);
  } else {
    $("#main_ch").prop("checked",false);
  }
}
function godelete() {
  var tot_chk = $("input[type='checkbox']:checked").length;
  if(tot_chk > 0) {
    if(confirm("Are You Want To Delete Selected Records!")) {
      if(confirm("Are You Sure To Delete Permanently Records!")) {
        //alert('test');
          document.form1.command.value = "m delete";
          document.form1.submit();
      }
    }
  } else {
    alert("Please Select Atlease One Record To Delete!");
  }
}
function single_delete(id) {
  if(confirm("Are You Want To Delete This Records!")) {
    if(confirm("Are You Sure To Delete Permanently Records!")) {
      window.location = "home.php?pages=membership-registration&delid="+id;
    }
  }
}
</script>


<!-- Page Content Start -->
<!-- ================== -->
<form method="post" name="form1">
<input type="hidden" name="command" value="">
<div class="wraper container-fluid">
  <div class="page-title">
    <h3 class="title">Membership Registration</h3>
<a href="javascript:godelete()"><button type="button" name="delete" value="delete" class="btn btn-danger glyphicon glyphicon-trash"></button></a>
  </div>

  <div class="row">
    <div class="col-md-12">
      <div class="panel panel-default">

        <div class="panel-body">
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12 mobtbl">
              <table id="datatable" class="table table-striped table-bordered table-responsive">
                <thead>
                  <tr>
                    <th width="10%">
                      <label class="option block mn">
                        <input type="checkbox" id="main_ch" onClick="checkall(this)" >
                        <span class=""></span> Select
                      </label>
                    </th>
                    <th width="5%">S.No</th>
                    <th> First Name</th>
                    <th> Email </th>
                    <th> Phone Number</th>
                    <th> Date Of Receipt</th>
                    <th width="8%">View Details</th>
                    <th width="8%">Status</th>
                  </tr>
                </thead>
                <tbody>

                  <?php $adminuser = $db->db_get_array("SELECT * FROM ".TABLE_MEMBERSHIP_REGISTRATION." WHERE 1=1 ");
                      $sn = 0;
                      foreach ($adminuser as $adminusers) {
                        $sn++;
                  ?>
                    <tr>

                        <td>
                          <label class="option block mn" style="width: 55px;">
                             <input type="checkbox" name="chk[]" value="<?php echo $adminusers['membership_registration_form_id']?>" onClick="uncheck()" >
                             <span class="checkbox mn"></span>
                          </label>
                        </td>
                        <td><?php echo $sn;?></td>

                        <td><?php echo $adminusers['first_name']?></td>
                        <td><?php echo $adminusers['email']?></td>
                        <td><?php echo $adminusers['phone']?></td>
                        <td>
                          <?php
                            $adate = explode('-',$adminusers['date_of_receipt']);
                            echo $adate[0];
                          ?>
                        </td>

                        <td class="text-center">
                          <a href="home.php?pages=membership-registration-details&id=<?php echo $adminusers['membership_registration_form_id']?>" class="" title="Deactive">
                          <button type="button" class="btn btn-info  br2 btn-xs fs12 dropdown-toggle" > View Info
                          </button></a>
                        </td>

                        <?php if ($adminusers['membership_reg_status'] != 0) {?>
                          <td class="text-center">
                            <a href="home.php?pages=membership-registration&status=0&id=<?php echo $adminusers['membership_registration_form_id']?>" class="" title="Active">
                            <button type="button" class="btn btn-success br2 btn-xs fs12 activebtn" > Active
                            </button></a>
                          </td>
                        <?php } else { ?>
                          <td class="text-center">
                            <a href="home.php?pages=membership-registration&status=1&id=<?php echo $adminusers['membership_registration_form_id']?>" class="" title="Deactive">
                            <button type="button" class="btn btn-danger  br2 btn-xs fs12 dropdown-toggle" > Deactive
                            </button></a>
                          </td>
                        <?php }?>


                    </tr>

                  <?php }?>






                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- End row -->

</div>
</form>


<!-- Page Content Ends -->
<!-- ================== -->

</section>
<!-- Main Content Ends -->

</body></html>
