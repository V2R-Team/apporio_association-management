<?php
include('common.php');
include_once '../apporioconfig/start_up.php';
if ($admininfos['admin_role'] != 'merchant_user') {
  $db->redirect("home.php?pages=dashboard");
}

$account_setting = $db->db_get_array("SELECT * FROM ".TABLE_ACCOUNT." WHERE acc_id = 1 ");
		foreach($account_setting as $account_settings);
// echo "<pre>";
// print_r($account_settings);
// echo "</pre>";

if($_SERVER['REQUEST_METHOD'] == "POST") {
  
  $arr = array(
    'acc_site_title'        => $_POST['admin_site'],
    'acc_email'             => $_POST['admin_email_id'],
    'acc_phone'             => $_POST['admin_phone'],
    'acc_city'              => $_POST['admin_city'],
    'acc_address'           => $_POST['admin_address'],
    'acc_fb'                => $_POST['facebook_link'],
    'acc_go'                => $_POST['google_link'],
    'acc_tw'                => $_POST['twitter_link'],
    'acc_linked'            => $_POST['linkedin_link'],
    'acc_youtube'           => $_POST['youtube_link'],
    'acc_pinterest'         => $_POST['pinterest_link'],
  );
  $condition = "acc_id = 1 ";
  $db->update(TABLE_ACCOUNT,$arr,$condition,true);
 
  $db->redirect("home.php?pages=account-setting");
}
?>

<script type="text/javascript">

function validatevent() {
    if(document.getElementById('admin_site').value == "")
    {
      alert("Please Enter Site Name");
      document.getElementById('admin_site').focus();
      return false;
    }

    if(document.getElementById('admin_email_id').value == "")
    {
      alert("Please Enter Email Id");
      document.getElementById('admin_email_id').focus();
      return false;
    }
    if(document.getElementById('admin_phone').value == "")
    {
      alert("Please Enter Phone Number");
      document.getElementById('admin_phone').focus();
      return false;
    }
     if(document.getElementById('admin_city').value == "")
    {
      alert("Please Enter City Name");
      document.getElementById('admin_city').focus();
      return false;
    }

    if(document.getElementById('admin_address').value == "")
    {
      alert("Please Enter Address");
      document.getElementById('admin_address').focus();
      return false;
    }
    return true;
  }
</script>


  <!-- Page Content Start -->
  <!-- ================== -->

<div class="wraper container-fluid">
    <div class="page-title">
      <h3 class="title">Account Setting</h3>
    </div>
    <div class="row">
      <div class="col-sm-12">
        <div class="panel panel-default">

          <div class="panel-body">
            <div class="form">
              <form class="cmxform form-horizontal tasi-form"  method="post"  onSubmit="return validatevent()" enctype="multipart/form-data" action="">

                <div class="form-group ">
                  <label class="control-label col-lg-2">Site Name</label>
                  <div class="col-lg-10">
                    <input type="text" class="form-control" value="<?php echo $account_settings['acc_site_title']; ?>" placeholder="Site Name" name="admin_site" id="admin_site">
                  </div>
                </div>

                <div class="form-group ">
                  <label class="control-label col-lg-2">Email Id</label>
                  <div class="col-lg-10">
                    <input type="text" class="form-control" value="<?php echo $account_settings['acc_email']; ?>" placeholder="Email Id" name="admin_email_id" id="admin_email_id">
                  </div>
                </div>

                <div class="form-group ">
                  <label class="control-label col-lg-2">Phone Number</label>
                  <div class="col-lg-10">
                    <input type="text" class="form-control" value="<?php echo $account_settings['acc_phone']; ?>" placeholder="Phone Number" name="admin_phone" id="admin_phone">
                  </div>
                </div>

                <div class="form-group ">
                  <label class="control-label col-lg-2">City</label>
                  <div class="col-lg-10">
                    <input type="text" class="form-control" value="<?php echo $account_settings['acc_city']; ?>" placeholder="Phone Number" name="admin_city" id="admin_city">
                  </div>
                </div>

                <div class="form-group ">
                  <label class="control-label col-lg-2">Address</label>
                  <div class="col-lg-10">
                    <textarea class="form-control" placeholder="Address" name="admin_address" id="admin_address"><?php echo $account_settings['acc_address']; ?></textarea>
                  </div>
                </div>

                <div class="form-group ">
                  <label class="control-label col-lg-2">Facebook Link</label>
                  <div class="col-lg-10">
                    <input type="text" class="form-control" value="<?php echo $account_settings['acc_fb']; ?>" placeholder="Facebook Link<" name="facebook_link" id="facebook_link">
                  </div>
                </div>

                <div class="form-group ">
                  <label class="control-label col-lg-2">Google+ Link</label>
                  <div class="col-lg-10">
                    <input type="text" class="form-control" value="<?php echo $account_settings['acc_go']; ?>" placeholder="Google+ Link<" name="google_link" id="google_link">
                  </div>
                </div>

                <div class="form-group ">
                  <label class="control-label col-lg-2">Twitter Link</label>
                  <div class="col-lg-10">
                    <input type="text" class="form-control" value="<?php echo $account_settings['acc_tw']; ?>" placeholder="Twitter Link" name="twitter_link" id="twitter_link">
                  </div>
                </div>

                <div class="form-group ">
                  <label class="control-label col-lg-2">Linkedin Link</label>
                  <div class="col-lg-10">
                    <input type="text" class="form-control" value="<?php echo $account_settings['acc_linked']; ?>" placeholder="Linkedin Link" name="linkedin_link" id="linkedin_link">
                  </div>
                </div>

                <div class="form-group ">
                  <label class="control-label col-lg-2">YouTube Link</label>
                  <div class="col-lg-10">
                    <input type="text" class="form-control" value="<?php echo $account_settings['acc_youtube']; ?>" placeholder="YouTube Link" name="youtube_link" id="youtube_link">
                  </div>
                </div>

                <div class="form-group ">
                  <label class="control-label col-lg-2">Pinterest Link</label>
                  <div class="col-lg-10">
                    <input type="text" class="form-control" value="<?php echo $account_settings['acc_pinterest']; ?>" placeholder="Pinterest Link" name="pinterest_link" id="pinterest_link">
                  </div>
                </div>



                
                  
                  

                


                

              

               
                <div class="form-group">
                  <div class="col-lg-offset-2 col-lg-10">
                    <?php if(!isset($_GET['edit_id'])) { ?>
                    	<input type="submit" class=" btn btn-info col-md-4 col-sm-6 col-xs-12" id="save" name="save" value="Save" >
										<?php } else { ?>
											<input type="submit" class=" btn btn-info col-md-4 col-sm-6 col-xs-12" id="save" name="save" value="Update" >
										<?php } ?>
                  </div>
                </div>





              </form>
            </div>
            <!-- .form -->

          </div>
          <!-- panel-body -->
        </div>
        <!-- panel -->
      </div>
      <!-- col -->

    </div>
    <!-- End row -->

  </div>

  <!-- Page Content Ends -->
  <!-- ================== -->

</section>
<!-- Main Content Ends -->

</body>
</html>
<script type="text/javascript" src="js/wickedpicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css">

        <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>

<link rel="stylesheet" type="text/css" href="css/wickedpicker.min.css">

