<?php 
include('common.php'); 
include_once '../apporioconfig/start_up.php';

$query="select * from user ORDER BY user_id DESC";
	$result = $db->query($query);
	$list=$result->rows;       
        
    
    if(isset($_GET['status']) && isset($_GET['id'])) 
    {
     $query1="UPDATE user SET status='".$_GET['status']."' WHERE user_id='".$_GET['id']."'";
     $db->query($query1);
     $db->redirect("home.php?pages=view-user");
    }
    
    
     if(isset($_POST['delete'])) 
     {
      $query1="DELETE FROM user WHERE user_id='".$_POST['chk']."'";
      $db->query($query1);
      $db->redirect("home.php?pages=view-user");
     }
	
    
?>

<!-- Page Content Start -->
<!-- ================== -->
<form method="post" name="frm">
<div class="wraper container-fluid">
  <div class="page-title">
    <h3 class="title">Users</h3>
    <button type="submit" name="delete" value="delete" class="btn btn-danger glyphicon glyphicon-trash"></button>
  </div>
  
  <div class="row">
    <div class="col-md-12">
      <div class="panel panel-default">
        
        <div class="panel-body">
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12 mobtbl">
              <table id="datatable" class="table table-striped table-bordered table-responsive">
                <thead>
                  <tr>
                    <th width="6%">Select</th>
                    <th width="5%">S.No</th>
                    <th>User Name</th>
                    <th>Email Id</th>
                    <th>Mobile No.</th>
                    <th>Device</th>
                    <th width="10%">Rating</th>
                    <th width="8%">Status</th>
                    <th width="12%">Full Details</th>
                  </tr>
                </thead>
                <tbody>
                  <?php foreach($list as $user){?>
                  <tr>
                    <td><label class="option block mn" style="width: 55px;">
                       <input type="checkbox" name="chk" value="<?php echo $user['user_id']?>" onClick="uncheck()" >
                       <span class="checkbox mn"></span>
                    </label></td>
                    <td><?php echo $user['user_id'];?></td>
                    <td><?php echo $user['user_name'];?></td>
                    <td><?php echo $user['user_email'];?></td>
                    <td><?php echo $user['user_phone'];?></td>
                    <td><?php
            	      $device=$user['flag'];
            	      if($device==1)
            	      {
            	      echo "Iphone";
            	      }
            	      else if($device==2)
            	      {
            	       echo "Android";
            	      }
            	      else
            	      {
            	       echo "------";
            	      }
            	      ?></td>
                    <td>
                    <?php
   $userrating=$user['rating'];
   if($userrating=="")
   {
     echo "<i class=\"fa fa-star-o\"></i><i class=\"fa fa-star-o\"></i><i class=\"fa fa-star-o\"></i><i class=\"fa fa-star-o\"></i><i class=\"fa fa-star-o\"></i>";
   }
   else if($userrating==1)
   {
     echo "<i class=\"fa fa-star\"></i><i class=\"fa fa-star-o\"></i><i class=\"fa fa-star-o\"></i><i class=\"fa fa-star-o\"></i><i class=\"fa fa-star-o\"></i>";
   }
   else if($userrating==2)
   {
     echo "<i class=\"fa fa-star\"></i><i class=\"fa fa-star\"></i><i class=\"fa fa-star-o\"></i><i class=\"fa fa-star-o\"></i><i class=\"fa fa-star-o\"></i>";
   }
   else if($userrating==3)
   {
     echo "<i class=\"fa fa-star\"></i><i class=\"fa fa-star\"></i><i class=\"fa fa-star\"></i><i class=\"fa fa-star-o\"></i><i class=\"fa fa-star-o\"></i>";
   }
   else if($userrating==4)
   {
     echo "<i class=\"fa fa-star\"></i><i class=\"fa fa-star\"></i><i class=\"fa fa-star\"></i><i class=\"fa fa-star\"></i><i class=\"fa fa-star-o\"></i>";
   }
   else if($userrating==5)
   {
     echo "<i class=\"fa fa-star\"></i><i class=\"fa fa-star\"></i><i class=\"fa fa-star\"></i><i class=\"fa fa-star\"></i><i class=\"fa fa-star\"></i>";
   }
   else if($userrating<1.5 && $userrating>0 && $userrating<1)
   {
     echo "<i class=\"fa fa-star-half-o\"></i><i class=\"fa fa-star-o\"></i><i class=\"fa fa-star-o\"></i><i class=\"fa fa-star-o\"></i><i class=\"fa fa-star-o\"></i>";
   }
   
    else if($userrating>1 && $userrating<2)
   {
     echo "<i class=\"fa fa-star\"></i><i class=\"fa fa-star-half-o\"></i><i class=\"fa fa-star-o\"></i><i class=\"fa fa-star-o\"></i><i class=\"fa fa-star-o\"></i>";
   }
   
    else if($userrating>2 && $userrating<3)
   {
     echo "<i class=\"fa fa-star\"></i><i class=\"fa fa-star\"></i><i class=\"fa fa-star-half-o\"></i><i class=\"fa fa-star-o\"></i><i class=\"fa fa-star-o\"></i>";
   }
   
    else if($userrating>3 && $userrating<4)
   {
     echo "<i class=\"fa fa-star\"></i><i class=\"fa fa-star\"></i><i class=\"fa fa-star\"></i><i class=\"fa fa-star-half-o\"></i><i class=\"fa fa-star-o\"></i></i>";
   }
     else if($userrating>4 && $userrating<5)
   {
     echo "<i class=\"fa fa-star\"></i><i class=\"fa fa-star\"></i><i class=\"fa fa-star\"></i><i class=\"fa fa-star\"></i><i class=\"fa fa-star-half-o\"></i>";
   }
   
?>
                    </td>
                    <?php
                                if($user['status']==1) {
                                ?>
                                <td class="text-center">
                                    <a href="home.php?pages=view-user&status=2&id=<?php echo $user['user_id']?>" class="" title="Active">
                                    <button type="button" class="btn btn-success br2 btn-xs fs12 activebtn" > Active
                                    </button></a>
                                </td>
                                <?php
                                } else {
                                ?>
                                <td class="text-center">
                                <a href="home.php?pages=view-user&status=1&id=<?php echo $user['user_id']?>" class="" title="Deactive">
                                    <button type="button" class="btn btn-danger  br2 btn-xs fs12 dropdown-toggle" > Deactive
                                    </button></a>
                                </td>
                                <?php } ?>
                    <td><button type="button" class="btn btn-success btn-xs" data-toggle="modal" data-target="#<?php echo $user['user_id'];?>"  > Full Details </button></td>
                  </tr>
                  <?php }?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- End row --> 
  
</div>
</form>
<!--User Details Starts-->
<?php
$dummyImg="http://apporio.co.uk/apporiotaxi/uploads/driver/driverprofile.png";
 foreach($list as $user){?>
<div class="modal fade" id="<?php echo $user['user_id'];?>" role="dialog">
  <div class="modal-dialog"> 
    
    <!-- Modal content starts-->
    
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title fdetailsheading">User Full Details</h4>
      </div>
      <div class="modal-body">
        <div class="modal-body">
          <table class="table table-striped table-hover table-bordered" id="sample_editable_1">
            <thead>
              <tr>
                <td class="" colspan="" style="text-align-last:left; width:150px;"><img src="<?php if($user['user_image'] != '' && isset($user['user_image'])){echo '../'.$user['user_image'];}else{ echo $dummyImg; }?>"  width="150px" height="150px"></td>
                <td><table style="margin-left:20px;" aling="center" border="0">
                    <tbody>
                      <tr>
                        <th class=""> Name</th>
                        <td class="">
			  <?php
                             $user_name=$user['user_name'];
                             if($user_name=="")
                             {
                             echo "---------";
                             }
                             else
                             {
                             echo $user_name;
                             }
                           ?>
                         </td>
                      </tr>
                      <tr>
                        <th class="">Email</th>
                        <td class="">
			   <?php
                             $user_email=$user['user_email'];
                             if($user_email=="")
                             {
                             echo "---------";
                             }
                             else
                             {
                             echo $user_email;
                             }
                           ?>
                         </td>
                      </tr>
                      <tr>
                        <th class="">Phone</th>
                        <td class="">
						   <?php
                             $user_phone=$user['user_phone'];
                             if($user_phone=="")
                             {
                             echo "---------";
                             }
                             else
                             {
                             echo $user_phone;
                             }
                           ?></td>
                      </tr>
                      <tr>
                        <th class="">Status</th>
                        <td class="">
			     <?php
            	           	 $login_logout=$user['login_logout'];
            	      		 if($login_logout==1)
            	      		 {
            	      		 echo "<button class=\"btn btn-success btn-xs activebtn\">Login</button>";
							 }
            	      		 else
            	      		 {
            	      		 echo "<button class=\"btn btn-danger btn-xs\">Logout</button>";
            	      		 }
            	              ?>
                         
                        </td>
                      </tr>
                    </tbody>
                  </table></td>
              </tr>
              <tr>
                <th class="">Rating</th>
                <td class="">
		    <?php
   $userrating=$user['rating'];
   if($userrating=="")
   {
     echo "<i class=\"fa fa-star-o\"></i><i class=\"fa fa-star-o\"></i><i class=\"fa fa-star-o\"></i><i class=\"fa fa-star-o\"></i><i class=\"fa fa-star-o\"></i>";
   }
   else if($userrating==1)
   {
     echo "<i class=\"fa fa-star\"></i><i class=\"fa fa-star-o\"></i><i class=\"fa fa-star-o\"></i><i class=\"fa fa-star-o\"></i><i class=\"fa fa-star-o\"></i>";
   }
   else if($userrating==2)
   {
     echo "<i class=\"fa fa-star\"></i><i class=\"fa fa-star\"></i><i class=\"fa fa-star-o\"></i><i class=\"fa fa-star-o\"></i><i class=\"fa fa-star-o\"></i>";
   }
   else if($userrating==3)
   {
     echo "<i class=\"fa fa-star\"></i><i class=\"fa fa-star\"></i><i class=\"fa fa-star\"></i><i class=\"fa fa-star-o\"></i><i class=\"fa fa-star-o\"></i>";
   }
   else if($userrating==4)
   {
     echo "<i class=\"fa fa-star\"></i><i class=\"fa fa-star\"></i><i class=\"fa fa-star\"></i><i class=\"fa fa-star\"></i><i class=\"fa fa-star-o\"></i>";
   }
   else if($userrating==5)
   {
     echo "<i class=\"fa fa-star\"></i><i class=\"fa fa-star\"></i><i class=\"fa fa-star\"></i><i class=\"fa fa-star\"></i><i class=\"fa fa-star\"></i>";
   }
   else if($userrating<1.5 && $userrating>0 && $userrating<1)
   {
     echo "<i class=\"fa fa-star-half-o\"></i><i class=\"fa fa-star-o\"></i><i class=\"fa fa-star-o\"></i><i class=\"fa fa-star-o\"></i><i class=\"fa fa-star-o\"></i>";
   }
   
    else if($userrating>1 && $userrating<2)
   {
     echo "<i class=\"fa fa-star\"></i><i class=\"fa fa-star-half-o\"></i><i class=\"fa fa-star-o\"></i><i class=\"fa fa-star-o\"></i><i class=\"fa fa-star-o\"></i>";
   }
   
    else if($userrating>2 && $userrating<3)
   {
     echo "<i class=\"fa fa-star\"></i><i class=\"fa fa-star\"></i><i class=\"fa fa-star-half-o\"></i><i class=\"fa fa-star-o\"></i><i class=\"fa fa-star-o\"></i>";
   }
   
    else if($userrating>3 && $userrating<4)
   {
     echo "<i class=\"fa fa-star\"></i><i class=\"fa fa-star\"></i><i class=\"fa fa-star\"></i><i class=\"fa fa-star-half-o\"></i><i class=\"fa fa-star-o\"></i></i>";
   }
     else if($userrating>4 && $userrating<5)
   {
     echo "<i class=\"fa fa-star\"></i><i class=\"fa fa-star\"></i><i class=\"fa fa-star\"></i><i class=\"fa fa-star\"></i><i class=\"fa fa-star-half-o\"></i>";
   }
   
?>
                 </td>
              </tr>
              <tr>
                <th class="">Device</th>
                <td class="">
		    <?php
                      $phonedevice=$user['flag'];
                      if($phonedevice==1)
                      {
                      echo "Iphone";
                      }
                      else if($phonedevice==2)
                      {
                      echo "Android";
                      }
                      else
                      {
                      echo "---------";
                      }
                   ?>
                 </td>
              </tr>
              
              <tr>
                        <th class="">Register Date</th>
                        <td class="">
			   <?php
                             $register_date=$user['register_date'];
                             if($register_date=="")
                             {
                             echo "---------";
                             }
                             else
                             {
                             echo $register_date;
                             }
                           ?>
                         </td>
                      </tr>
              
              
              
             
              
              
            
            </thead>
            <tbody>
            </tbody>
          </table>
        </div>
      </div>
    </div>
    
    <!-- Modal content closed--> 
    
  </div>
</div>
<?php }?>
<!--User Details Closed-->  

<!-- Page Content Ends --> 
<!-- ================== -->

</section>
<!-- Main Content Ends -->

</body></html>