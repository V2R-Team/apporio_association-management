<?php 
include('common.php'); 
include_once '../apporioconfig/start_up.php';

$query="select * from car_type";
	$result = $db->query($query);
	$list=$result->rows;       
        
   if(isset($_GET['status']) && isset($_GET['id'])) 
    {
     $query1="UPDATE car_type  SET status='".$_GET['status']."' WHERE car_type_id='".$_GET['id']."'";
     $db->query($query1);
     $db->redirect("home.php?pages=view-car-type");
    }
    
    if(isset($_POST['delete'])) 
     {
      $query1="DELETE FROM car_type WHERE car_type_id='".$_POST['chk']."'";
      $db->query($query1);
      $db->redirect("home.php?pages=view-car-type");
     }
	
    
?>

<!-- Page Content Start -->
<!-- ================== -->
<form method="post" name="frm">
<div class="wraper container-fluid">
  <div class="page-title">
    <h3 class="title">View Car Type</h3>
    <button type="submit" name="delete" value="delete" class="btn btn-danger glyphicon glyphicon-trash"></button>
  </div>
  
  <div class="row">
    <div class="col-md-12">
      <div class="panel panel-default">
        
        <div class="panel-body">
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12 mobtbl">
              <table id="datatable" class="table table-striped table-bordered table-responsive">
                <thead>
                  <tr>
                    <th width="11%">&nbsp; Select</th>
                    <th width="5%">S.No</th>
                    <th width="50%">Car Name</th>
                    <th>Image</th>
                    <th width="12%">Status</th>
                  </tr>
                </thead>
                <tbody>
                  <?php foreach($list as $cartype){?>
                  <tr>
                    <td>
                    <label class="option block mn" style="width: 55px;">
                       <input type="checkbox" name="chk" value="<?php echo $cartype['car_type_id']?>" onClick="uncheck()" >
                       <span class="checkbox mn"></span>
                    </label>
                    </td>
                    <td><?php echo $cartype['car_type_id'];?></td>
                    
                    <td>
                    <?php
            	      $car_type_name=$cartype['car_type_name'];
            	      if($car_type_name=="")
            	      {
            	      echo "---------";
            	      }
            	      else
            	      {
            	       echo $car_type_name;
            	      }
            	      ?>
            	      </td>
            	      
            	      
            	     <td><img src="../<?php echo $cartype['car_type_image'];?>"  width="80px" height="80px"></td>
            	      
            	      
            	    <?php
                                if($cartype['status']==1) {
                                ?>
                                <td class="text-center">
                                    <a href="home.php?pages=view-car-type&status=2&id=<?php echo $cartype['car_type_id']?>" class="" title="Active">
                                    <button type="button" class="btn btn-success br2 btn-xs fs12 activebtn" > Active
                                    </button></a>
                                </td>
                                <?php
                                } else {
                                ?>
                                <td class="text-center">
                                <a href="home.php?pages=view-car-type&status=1&id=<?php echo $cartype['car_type_id']?>" class="" title="Deactive">
                                    <button type="button" class="btn btn-danger  br2 btn-xs fs12 dropdown-toggle" > Deactive
                                    </button></a>
                                </td>
                                <?php } ?>
                   
                  </tr>
                  <?php }?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- End row --> 
  
</div> 
</form>
<!-- Page Content Ends --> 
<!-- ================== -->

</section>
<!-- Main Content Ends -->

</body></html>