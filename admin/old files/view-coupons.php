<?php 
include('common.php'); 
include_once '../apporioconfig/start_up.php';

$query="select * from coupons ORDER BY coupons_id";
	$result = $db->query($query);
	$list=$result->rows;
	



 if(isset($_GET['status']) && isset($_GET['id'])) 
    {
     $query1="UPDATE coupons SET status='".$_GET['status']."' WHERE coupons_id='".$_GET['id']."'";
     $db->query($query1);
     $db->redirect("home.php?pages=view-coupons");
    }
    
    
     if(isset($_POST['delete'])) 
     {
      $query1="DELETE FROM coupons WHERE coupons_id='".$_POST['chk']."'";
      $db->query($query1);
      $db->redirect("home.php?pages=view-coupons");
     }
	
	
?>


  <!-- Page Content Start --> 
  <!-- ================== -->
  <form method="post" name="frm">
  <div class="wraper container-fluid">
    <div class="page-title">
      <h3 class="title">View Coupons</h3>
    <button type="submit" name="delete" value="delete" class="btn btn-danger glyphicon glyphicon-trash"></button>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div class="panel panel-default">
          <div class="panel-body">
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12 mobtbl">
              <table id="datatable" class="table table-striped table-bordered table-responsive">
                    <thead>
                      <tr>
                        <th width="10%">Select</th>
                        <th>S.No</th>
                        <th>Coupon Code</th>
                        <th>Coupon Price</th>
                        <th>Expiry Date</th>
                        <th width="12%">Status</th>
                      </tr>
                    </thead>
                 
                 <tbody>
                 
                 
   
                 
                   <?php foreach($list as $viewcoupon){?>
                    <tr>
                      <td><label class="option block mn" style="width: 55px;">
                       <input type="checkbox" name="chk" value="<?php echo $viewcoupon['coupons_id']?>" onClick="uncheck()" >
                       <span class="checkbox mn"></span>
                    </label></td>
                      <td><?php echo $viewcoupon['coupons_id'];?></td>
            	      <td><?php echo $viewcoupon['coupons_code'];?></td>
            	      <td><?php echo $viewcoupon['coupons_price'];?></td>
            	      <td><?php echo $viewcoupon['expiry_date'];?></td>
                      
                      
                       <?php
                                if($viewcoupon['status']==1) {
                                ?>
                                <td class="text-center">
                                    <a href="home.php?pages=view-coupons&status=2&id=<?php echo $viewcoupon['coupons_id']?>" class="" title="Active">
                                    <button type="button" class="btn btn-success br2 btn-xs fs12 activebtn" > Active
                                    </button></a>
                                </td>
                                <?php
                                } else {
                                ?>
                                <td class="text-center">
                                <a href="home.php?pages=view-coupons&status=1&id=<?php echo $viewcoupon['coupons_id']?>" class="" title="Deactive">
                                    <button type="button" class="btn btn-danger  br2 btn-xs fs12 dropdown-toggle" > Deactive
                                    </button></a>
                                </td>
                                <?php } ?>
                      
                                         
                    </tr>
                    <?php }?>
                   
                 
                 
                 </tbody>
                    
                  </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- End row --> 
    
  </div>
  </form>
  <!-- Page Content Ends --> 
  <!-- ================== --> 
  
</section>
<!-- Main Content Ends -->

</body>
</html>
