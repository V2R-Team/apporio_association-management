<?php 
include('common.php'); 
include_once '../apporioconfig/start_up.php';

$query="select * from car_model";
	$result = $db->query($query);
	$list=$result->rows;    
	
   
   if(isset($_GET['status']) && isset($_GET['id'])) 
    {
     $query1="UPDATE car_model SET status='".$_GET['status']."' WHERE car_model_id='".$_GET['id']."'";
     $db->query($query1);
     $db->redirect("home.php?pages=view-car-model");
    }
    
	
    if(isset($_POST['delete'])) {
	$query1="DELETE FROM car_model WHERE car_model_id='".$_POST['chk']."'";
        $db->query($query1);
	$db->redirect("home.php?pages=view-car-model");
     }   
        
?>
  <!-- Page Content Start --> 
  <!-- ================== -->
  <form method="post" name="frm">
  <div class="wraper container-fluid">
    <div class="page-title">
      <h3 class="title">View Car Model</h3>
<button type="submit" name="delete" value="delete" class="btn btn-danger glyphicon glyphicon-trash"></button>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div class="panel panel-default">
          <div class="panel-body">
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12 mobtbl">
                
              <table id="datatable" class="table table-striped table-bordered table-responsive">
                    <thead>
                      <tr>
                        <th width="10%">Select</th>
                        <th>S.No</th>
                        <th>Car Type</th>
                        <th>Model Name</th>
                        <th>Image</th>
                        <th width="12%">Status</th>
                      </tr>
                    </thead>
                    <tbody>
                    <?php $dummyImg="http://apporio.co.uk/apporiotaxi/uploads/car/cardummy.png";
                      foreach($list as $viewmodel){?>
                    <tr>
                      <td>
                      <label class="option block mn" style="width: 55px;">
                       <input type="checkbox" name="chk" value="<?php echo $viewmodel['car_model_id']?>" onClick="uncheck()" >
                       <span class="checkbox mn"></span>
                    </label>
                      </td>
                      <td><?php echo $viewmodel['car_model_id'];?></td>
                      <td> 
			  <?php 

$car_type_id=$viewmodel['car_type_id'];

$query="select * from car_type where car_type_id=$car_type_id";
	$result = $db->query($query);
	$list=$result->row;    


$car_type_name=$list['car_type_name'];
if($car_type_name=="")
   {
   echo "---------";
   }
    else
    {
    echo $car_type_name;
    }
?>
                      </td>
                      <td> 
			 <?php
                           $carmodel=$viewmodel['car_model_name'];
                           if($carmodel=="")
                           {
                           echo "---------";
                           }
                           else
                           {
                           echo $carmodel;
                           }
                         ?>
                      </td>
                      <td><img src="<?php if($viewmodel['car_model_image'] != '' && isset($viewmodel['car_model_image'])){echo '../'.$viewmodel['car_model_image'];}else{ echo $dummyImg; }?>"  width="70px" height="70px"></td>                      
                      
                      
                      
                      <?php
                                if($viewmodel['status']==1) {
                                ?>
                                <td class="text-center">
                                    <a href="home.php?pages=view-car-model&status=2&id=<?php echo $viewmodel['car_model_id']?>" class="" title="Active">
                                    <button type="button" class="btn btn-success br2 btn-xs fs12 activebtn" > Active
                                    </button></a>
                                </td>
                                <?php
                                } else {
                                ?>
                                <td class="text-center">
                                <a href="home.php?pages=view-car-model&status=1&id=<?php echo $viewmodel['car_model_id']?>" class="" title="Deactive">
                                    <button type="button" class="btn btn-danger  br2 btn-xs fs12 dropdown-toggle" > Deactive
                                    </button></a>
                                </td>
                                <?php } ?>
                                
                      
                      
                      
                    </tr>
                    <?php }?>
                    </tbody>
                    
                  </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- End row --> 
    
  </div>
  </form>
  <!-- Page Content Ends --> 
  <!-- ================== --> 
  
</section>
<!-- Main Content Ends -->

</body>
</html>
