<?php 
include('common.php'); 
include_once '../apporioconfig/start_up.php';

$query="select * from driver ORDER BY driver_id DESC";
	$result = $db->query($query);
	$list=$result->rows;	
		
		
    if(isset($_GET['status']) && isset($_GET['id'])) 
    {
     $query1="UPDATE driver SET status='".$_GET['status']."' WHERE driver_id='".$_GET['id']."'";
     $db->query($query1);
     $db->redirect("home.php?pages=view-driver");
    }
    
    if(isset($_GET['online_offline']) && isset($_GET['id'])) 
    {
     $query1="UPDATE driver SET online_offline='".$_GET['online_offline']."' WHERE driver_id='".$_GET['id']."'";
     $db->query($query1);
     $db->redirect("home.php?pages=view-driver");
    }
	
    if(isset($_POST['delete'])) {
	$query1="DELETE FROM driver WHERE driver_id='".$_POST['chk']."'";
        $db->query($query1);
	$db->redirect("home.php?pages=view-driver");
     }
		
      
        
?>

<!-- Page Content Start -->
<!-- ================== -->
<form method="post" name="frm">


<div class="wraper container-fluid">
  <div class="page-title">
    <h3 class="title">Drivers</h3>
                 
   
<button type="submit" name="delete" value="delete" class="btn btn-danger glyphicon glyphicon-trash"></button>
    
    
     <!-- <button type="submit" class="btn btn-danger glyphicon glyphicon-trash"></button>-->
  </div>
  <div class="row">
   
    <div class="col-md-12">
      <div class="panel panel-default">
        <div class="panel-body">
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12 mobtbl">
              <table id="datatable" class="table table-striped table-bordered table-responsive">
                <thead>
                  <tr>
                    <th width="6%">Select</th>
                    <th width="5%">S.No</th>
                    <th>Driver Name</th>
                    <th>Email Id</th>
                    <th>Mobile No.</th>
                    <th>Rating</th>
                    <th>Status</th>
                    <th width="8%">Status</th>
                    <th width="12%">Full Details</th>
                  </tr>
                </thead>
                <tbody>
                  <?php foreach($list as $driver){?>
                  <tr>
                    <td>
                    <label class="option block mn" style="width: 55px;">
                       <input type="checkbox" name="chk" value="<?php echo $driver['driver_id']?>" onClick="uncheck()" >
                       <span class="checkbox mn"></span>
                    </label>
                    </td>
                    <td><?php echo $driver['driver_id'];?></td>
                    <td><?php echo $driver['driver_name'];?></td>
                    <td><?php echo $driver['driver_email'];?></td>
                    <td><?php echo $driver['driver_phone'];?></td>
                    
                    <td>
                 <?php
   $driverrating=$driver['rating'];
   if($driverrating=="")
   {
     echo "<i class=\"fa fa-star-o\"></i><i class=\"fa fa-star-o\"></i><i class=\"fa fa-star-o\"></i><i class=\"fa fa-star-o\"></i><i class=\"fa fa-star-o\"></i>";
   }
   else if($driverrating==1)
   {
     echo "<i class=\"fa fa-star\"></i><i class=\"fa fa-star-o\"></i><i class=\"fa fa-star-o\"></i><i class=\"fa fa-star-o\"></i><i class=\"fa fa-star-o\"></i>";
   }
   else if($driverrating==2)
   {
     echo "<i class=\"fa fa-star\"></i><i class=\"fa fa-star\"></i><i class=\"fa fa-star-o\"></i><i class=\"fa fa-star-o\"></i><i class=\"fa fa-star-o\"></i>";
   }
   else if($driverrating==3)
   {
     echo "<i class=\"fa fa-star\"></i><i class=\"fa fa-star\"></i><i class=\"fa fa-star\"></i><i class=\"fa fa-star-o\"></i><i class=\"fa fa-star-o\"></i>";
   }
   else if($driverrating==4)
   {
     echo "<i class=\"fa fa-star\"></i><i class=\"fa fa-star\"></i><i class=\"fa fa-star\"></i><i class=\"fa fa-star\"></i><i class=\"fa fa-star-o\"></i>";
   }
   else if($driverrating==5)
   {
     echo "<i class=\"fa fa-star\"></i><i class=\"fa fa-star\"></i><i class=\"fa fa-star\"></i><i class=\"fa fa-star\"></i><i class=\"fa fa-star\"></i>";
   }
   else if($driverrating<1.5 && $driverrating>0 && $driverrating<1)
   {
     echo "<i class=\"fa fa-star-half-o\"></i><i class=\"fa fa-star-o\"></i><i class=\"fa fa-star-o\"></i><i class=\"fa fa-star-o\"></i><i class=\"fa fa-star-o\"></i>";
   }
   
    else if($driverrating>1 && $driverrating<2)
   {
     echo "<i class=\"fa fa-star\"></i><i class=\"fa fa-star-half-o\"></i><i class=\"fa fa-star-o\"></i><i class=\"fa fa-star-o\"></i><i class=\"fa fa-star-o\"></i>";
   }
   
    else if($driverrating>2 && $driverrating<3)
   {
     echo "<i class=\"fa fa-star\"></i><i class=\"fa fa-star\"></i><i class=\"fa fa-star-half-o\"></i><i class=\"fa fa-star-o\"></i><i class=\"fa fa-star-o\"></i>";
   }
   
    else if($driverrating>3 && $driverrating<4)
   {
     echo "<i class=\"fa fa-star\"></i><i class=\"fa fa-star\"></i><i class=\"fa fa-star\"></i><i class=\"fa fa-star-half-o\"></i><i class=\"fa fa-star-o\"></i></i>";
   }
     else if($driverrating>4 && $driverrating<5)
   {
     echo "<i class=\"fa fa-star\"></i><i class=\"fa fa-star\"></i><i class=\"fa fa-star\"></i><i class=\"fa fa-star\"></i><i class=\"fa fa-star-half-o\"></i>";
   }
   
?>
                    </td>
                    
                    
                    <?php
                                if($driver['online_offline']==1) {
                                ?>
                                <td class="text-center">
                                    <a href="home.php?pages=view-driver&online_offline=2&id=<?php echo $driver['driver_id']?>" class="" title="Active">
                                    <button type="button" class="btn btn-success br2 btn-xs fs12" > Online
                                    </button></a>
                                </td>
                                <?php
                                } else {
                                ?>
                                <td class="text-center">
                                <a href="home.php?pages=view-driver&online_offline=1&id=<?php echo $driver['driver_id']?>" class="" title="Deactive">
                                    <button type="button" class="btn btn-danger  br2 btn-xs fs12 dropdown-toggle" > Offline
                                    </button></a>
                                </td>
                                <?php } ?>
            	   		  
                    
                    
                    
					
	<?php
                                if($driver['status']==1) {
                                ?>
                                <td class="text-center">
                                    <a href="home.php?pages=view-driver&status=2&id=<?php echo $driver['driver_id']?>" class="" title="Active">
                                    <button type="button" class="btn btn-success br2 btn-xs fs12 activebtn" > Active
                                    </button></a>
                                </td>
                                <?php
                                } else {
                                ?>
                                <td class="text-center">
                                <a href="home.php?pages=view-driver&status=1&id=<?php echo $driver['driver_id']?>" class="" title="Deactive">
                                    <button type="button" class="btn btn-danger  br2 btn-xs fs12 dropdown-toggle" > Deactive
                                    </button></a>
                                </td>
                                <?php } ?>
                                
                                
                                
                                
                                
                                
                    <td><button type="button" class="btn btn-success btn-xs" data-toggle="modal" data-target="#driverdetails<?php echo $driver['driver_id'];?>"  > Full Details </button></td>
                  </tr>
                  <?php }?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
    
  </div>
  <!-- End row --> 
  
</div>
</form>
<!--Driver Details Starts-->
<?php
$dummyImg="http://apporio.co.uk/apporiotaxi/uploads/driver/driverprofile.png";
 foreach($list as $driver){?>
<div class="modal fade" id="driverdetails<?php echo $driver['driver_id'];?>" role="dialog"> <?php echo $driver['driver_id'];?>
  <div class="modal-dialog"> 
    
    <!-- Modal content starts-->
    
    <div class="modal-content" style="padding:20px !important;">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title fdetailsheading">Driver Full Details</h4>
      </div>
      <div class="modal-body">
        <div class="modal-body">
          <table class="table table-striped table-hover table-bordered" id="sample_editable_1">
            <thead>
              <tr>
                <td class="" colspan="" style="text-align-last:left; width:150px;"><img src="<?php if($driver['driver_image'] != '' && isset($driver['driver_image'])){echo '../'.$driver['driver_image'];}else{ echo $dummyImg; }?>"  width="150px" height="150px"></td>
                <td><table style="margin-left:20px;" aling="center" border="0">
                    <tbody>
                      <tr>
                        <th class=""> Name</th>
                        <td class="">
			  <?php
                             $drivername=$driver['driver_name'];
                             if($drivername=="")
                             {
                             echo "---------";
                             }
                             else
                             {
                             echo $drivername;
                             }
                           ?>
                         </td>
                      </tr>
                      <tr>
                        <th class="">Email</th>
                        <td class="">
			   <?php
                             $driveremail=$driver['driver_email'];
                             if($driveremail=="")
                             {
                             echo "---------";
                             }
                             else
                             {
                             echo $driveremail;
                             }
                           ?>
                         </td>
                      </tr>
                      <tr>
                        <th class="">Phone</th>
                        <td class="">
						   <?php
                             $driverphone=$driver['driver_phone'];
                             if($driverphone=="")
                             {
                             echo "---------";
                             }
                             else
                             {
                             echo $driverphone;
                             }
                           ?></td>
                      </tr>
                      <tr>
                        <th class="">Status</th>
                        <td class="">
						   <?php
            	           	 $loginlogout=$driver['login_logout'];
            	      		 if($loginlogout==1)
            	      		 {
            	      		 echo "<button class=\"btn btn-success btn-xs activebtn\">Login</button>";
							 }
            	      		 else
            	      		 {
            	      		 echo "<button class=\"btn btn-danger btn-xs\">Logout</button>";
            	      		 }
            	   		  ?>
                          <?php
            	      	     $statusbusy=$driver['busy'];
            	      		 if($statusbusy==0)
            	      		 {
            	      		 echo "<button class=\"btn btn-success btn-xs activebtn\">Free</button>";
            	      		 }
            	      		 else if($statusbusy==1)
            	      		 {
            	      		 echo "<button class=\"btn btn-danger btn-xs\" >Busy</button>";
            	      		 }
            	      		  else
            	      		 {
            	      		 echo "----";
            	      		 }
            	   		  ?>
            	   		  
            	   	<?php
            	      		 $onlineoffline=$driver['online_offline'];
            	      		 if($onlineoffline==1)
            	             { 
            	      		 echo "<button class=\"btn btn-success btn-xs activebtn\">Online</button>";
            	      		 }
            	      		 else
            	     		 {
            	      		 echo "<button class=\"btn btn-danger btn-xs\">Offline</button>";
            	      		 }
            	   		 ?>	  
            	   		  
            	   		  
            	   		  
            	   		  
                          
                     
                      </tr>
                    </tbody>
                  </table></td>
              </tr>
              <tr>
                <th class="">Device</th>
                <td class="">
				   <?php
                   	  $phonedevice=$driver['flag'];
                      if($phonedevice==1)
                      {
                      echo "Iphone";
                      }
                      else if($phonedevice==2)
                      {
                      echo "Android";
                      }
                      else
                      {
                      echo "---------";
                      }
                   ?>
                 </td>
              </tr>
              <tr>
                <th class="">Rating</th>
                <td class="">
				<?php
   $driverrating=$driver['rating'];
   if($driverrating=="")
   {
     echo "<i class=\"fa fa-star-o\"></i><i class=\"fa fa-star-o\"></i><i class=\"fa fa-star-o\"></i><i class=\"fa fa-star-o\"></i><i class=\"fa fa-star-o\"></i>";
   }
   else if($driverrating==1)
   {
     echo "<i class=\"fa fa-star\"></i><i class=\"fa fa-star-o\"></i><i class=\"fa fa-star-o\"></i><i class=\"fa fa-star-o\"></i><i class=\"fa fa-star-o\"></i>";
   }
   else if($driverrating==2)
   {
     echo "<i class=\"fa fa-star\"></i><i class=\"fa fa-star\"></i><i class=\"fa fa-star-o\"></i><i class=\"fa fa-star-o\"></i><i class=\"fa fa-star-o\"></i>";
   }
   else if($driverrating==3)
   {
     echo "<i class=\"fa fa-star\"></i><i class=\"fa fa-star\"></i><i class=\"fa fa-star\"></i><i class=\"fa fa-star-o\"></i><i class=\"fa fa-star-o\"></i>";
   }
   else if($driverrating==4)
   {
     echo "<i class=\"fa fa-star\"></i><i class=\"fa fa-star\"></i><i class=\"fa fa-star\"></i><i class=\"fa fa-star\"></i><i class=\"fa fa-star-o\"></i>";
   }
   else if($driverrating==5)
   {
     echo "<i class=\"fa fa-star\"></i><i class=\"fa fa-star\"></i><i class=\"fa fa-star\"></i><i class=\"fa fa-star\"></i><i class=\"fa fa-star\"></i>";
   }
   else if($driverrating<1.5 && $driverrating>0 && $driverrating<1)
   {
     echo "<i class=\"fa fa-star-half-o\"></i><i class=\"fa fa-star-o\"></i><i class=\"fa fa-star-o\"></i><i class=\"fa fa-star-o\"></i><i class=\"fa fa-star-o\"></i>";
   }
   
    else if($driverrating>1 && $driverrating<2)
   {
     echo "<i class=\"fa fa-star\"></i><i class=\"fa fa-star-half-o\"></i><i class=\"fa fa-star-o\"></i><i class=\"fa fa-star-o\"></i><i class=\"fa fa-star-o\"></i>";
   }
   
    else if($driverrating>2 && $driverrating<3)
   {
     echo "<i class=\"fa fa-star\"></i><i class=\"fa fa-star\"></i><i class=\"fa fa-star-half-o\"></i><i class=\"fa fa-star-o\"></i><i class=\"fa fa-star-o\"></i>";
   }
   
    else if($driverrating>3 && $driverrating<4)
   {
     echo "<i class=\"fa fa-star\"></i><i class=\"fa fa-star\"></i><i class=\"fa fa-star\"></i><i class=\"fa fa-star-half-o\"></i><i class=\"fa fa-star-o\"></i></i>";
   }
     else if($driverrating>4 && $driverrating<5)
   {
     echo "<i class=\"fa fa-star\"></i><i class=\"fa fa-star\"></i><i class=\"fa fa-star\"></i><i class=\"fa fa-star\"></i><i class=\"fa fa-star-half-o\"></i>";
   }
   
?>
                </td>
              </tr>
              <tr>
                <th class="">Car Type</th>
                <td class="">
                
                <?php 

$car_type_id=$driver['car_type_id'];

$query="select * from car_type where car_type_id=$car_type_id";
	$result = $db->query($query);
	$list=$result->row;    


$car_type_name=$list['car_type_name'];
if($car_type_name=="")
   {
   echo "---------";
   }
    else
    {
    echo $car_type_name;
    }
?>
                
                </td>
              </tr>
              <tr>
                <th class="">Car Model</th>
                <td class=""><?php 

$car_model_id=$driver['car_model_id'];

$query="select * from car_model where car_model_id=$car_model_id";
	$result = $db->query($query);
	$list=$result->row;    


$car_model_name=$list['car_model_name'];
if($car_model_name=="")
   {
   echo "---------";
   }
    else
    {
    echo $car_model_name;
    }
?>
                </td>
              </tr>
              <tr>
                <th class="">Car Number</th>
                <td class="">
				   <?php
                      $carnumber=$driver['car_number'];
                      if($carnumber=="")
                      {
                      echo "---------";
                      }
                      else
                      {
                      echo $carnumber;
                      }
                   ?>
                </td>
              </tr>
              <tr>
                <th class="">City Name</th>
                <td class=""><?php 

$city_id=$driver['city_id'];

$query="select * from city where city_id=$city_id";
	$result = $db->query($query);
	$list=$result->row;    


echo $list['city_name'];
?>
                </td>
              </tr>
              <tr>
                <th class="">Register Date</th>
                <td class="">
				   <?php
                      $registerdate=$driver['register_date'];
                      if($registerdate=="")
                      {
                      echo "---------";
                      }
                      else
                      {
                      echo $registerdate;
                      }
                   ?>
                </td>
              </tr>
              <tr>
                <th class="">Current Location</th>
                <td class="">
				   <?php
                      $currentlocation=$driver['current_location'];
                                                        if($currentlocation=="")
                                                           {
                                                           echo "---------";
                                                           }
                                                           else
                                                           {
                                                           echo $currentlocation;
                                                           }
                                                       ?></td>
              </tr>
              <tr>
                <th class="">Completed Rides</th>
                <td class=""><?php
                                                        $completedrides=$driver['completed_rides'];
                                                        if($completedrides=="")
                                                           {
                                                           echo "---------";
                                                           }
                                                           else
                                                           {
                                                           echo $completedrides;
                                                           }
                                                       ?></td>
              </tr>
              <tr>
                <th class="">Rejected Rides</th>
                <td class=""><?php
                                                        $rejectrides=$driver['reject_rides'];
                                                        if($rejectrides=="")
                                                           {
                                                           echo "---------";
                                                           }
                                                           else
                                                           {
                                                           echo $rejectrides;
                                                           }
                                                       ?></td>
              </tr>
              <tr>
                <th class="">Cancelled Rides</th>
                <td class=""><?php
                                                        $cancelledrides=$driver['cancelled_rides'];
                                                        if($cancelledrides=="")
                                                           {
                                                           echo "---------";
                                                           }
                                                           else
                                                           {
                                                           echo $cancelledrides;
                                                           }
                                                       ?></td>
              </tr>
            </thead>
            <tbody>
            </tbody>
          </table>
        </div>
      </div>
    </div>
    
    <!-- Modal content closed--> 
    
  </div>
</div>
<?php }?>
<!--Driver Details Closed--> 

<!-- Page Content Ends --> 
<!-- ================== -->

</section>
<!-- Main Content Ends -->

</body></html>