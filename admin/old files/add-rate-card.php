<?php 
include('common.php'); 
include_once '../apporioconfig/start_up.php';

$query="select * from city";
	$result = $db->query($query);
	$list=$result->rows; 
	
$query="select * from car_type";
	$result = $db->query($query);
	$list1=$result->rows;       

   if(isset($_POST['save'])) {
     $query1="INSERT INTO rate_card (city_id,car_type_id,first_2km,after_2km,time_charges,waiting_charges,extra_charges) VALUES('".$_POST['city_id']."','".$_POST['car_type_id']."','".$_POST['first_2km']."','".$_POST['after_2km']."','".$_POST['time_charges']."','".$_POST['waiting_charges']."','".$_POST['extra_charges']."')";
     		$db->query($query1);
			
			
	}

?>

  <!-- Page Content Start --> 
  <!-- ================== -->
  
<div class="wraper container-fluid">
    <div class="page-title">
      <h3 class="title">Add Rate Card</h3>
    </div>
    <div class="row">
      <div class="col-sm-12">
        <div class="panel panel-default">
          
          <div class="panel-body">
            <div class="form" >
              <form class="cmxform form-horizontal tasi-form"  method="post">

                <div class="form-group ">
                  <label class="control-label col-lg-2">Choose City*</label>
                  <div class="col-lg-10">
                    <select class="form-control" name="city_id" id="" required>
                        <option>--Please Select City Name--</option>
                          <?php foreach($list as $cityname){ ?>
                           <option id="<?php echo $cityname['city_id'];?>"  value="<?php echo $cityname['city_id'];?>"><?php echo $cityname['city_name']; ?></option>


                <?php } ?>
                    </select>
                  </div>
                </div> 
                
                <div class="form-group ">
                  <label class="control-label col-lg-2">Choose Car Type Name*</label>
                  <div class="col-lg-10">
                    <select class="form-control" name="car_type_id" id="" required>
                        <option>--Please Select Car Type--</option>
                          <?php foreach($list1 as $cartype){ ?>
                            
                           <option id="<?php echo $cartype['car_type_id'];?>"  value="<?php echo $cartype['car_type_id'];?>"><?php echo $cartype['car_type_name']; ?></option>


                <?php } ?>
                    </select>
                  </div>
                </div>            

                <div class="form-group ">
                  <label class="control-label col-lg-2">First 2KM*</label>
                  <div class="col-lg-10">
                    <input type="text" class="form-control" placeholder="First 2KM" name="first_2km" id="" required>
                  </div>
                </div>
                
                <div class="form-group ">
                  <label class="control-label col-lg-2">After 2KM*</label>
                  <div class="col-lg-10">
                    <input type="text" class="form-control" placeholder="After 2KM" name="after_2km" id="" required>
                  </div>
                </div>
                
                <div class="form-group ">
                  <label class="control-label col-lg-2">Charges/minute*</label>
                  <div class="col-lg-10">
                    <input type="text" class="form-control" placeholder="Time Charges/minute" name="time_charges" id="" required>
                  </div>
                </div>
                
                <div class="form-group ">
                  <label class="control-label col-lg-2">Wait Time charge*</label>
                  <div class="col-lg-10">
                    <input type="text" class="form-control" placeholder="Wait Time charge" name="waiting_charges" id="" required>
                  </div>
                </div>
                
                <div class="form-group ">
                  <label class="control-label col-lg-2">Extra Charge*</label>
                  <div class="col-lg-10">
                    <input type="text" class="form-control" placeholder="Extra Charge" name="extra_charges" id="">
                  </div>
                </div>
                
                
                
                
                
                
                
                <div class="form-group">
                  <div class="col-lg-offset-2 col-lg-10">
                    <input type="submit" class=" btn btn-info col-md-4 col-sm-6 col-xs-12" id="save" name="save" value="Add Rate Card" >
                  </div>
                </div>
              </form>
            </div>
            <!-- .form --> 
            
          </div>
          <!-- panel-body --> 
        </div>
        <!-- panel --> 
      </div>
      <!-- col --> 
      
    </div>
    <!-- End row --> 
    
  </div>
  
  <!-- Page Content Ends --> 
  <!-- ================== --> 
  
</section>
<!-- Main Content Ends -->

</body>
</html>
