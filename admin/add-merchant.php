<?php

include('common.php');
include_once '../apporioconfig/start_up.php';
// echo "<pre>";
// print_r($_SESSION);
// echo "</pre>";
// if ($admininfos['country_add_status'] != 1) {
//   $db->redirect("home.php?pages=dashboard");
// }

if(isset($_GET['edit_id'])) {
		$country_data = $db->db_get_array("SELECT * FROM ".TABLE_ADMIN." WHERE admin_id='".$_GET['edit_id']."' ");
		foreach($country_data as $country_res);
}
if($_SERVER['REQUEST_METHOD'] == "POST") {
	if(!isset($_GET['edit_id'])) {
			$country_data = $db->db_get_array("SELECT * FROM ".TABLE_ADMIN." WHERE admin_fname='".$_POST['admin_fname']."' ");
 			$tot = count($country_data);
			if ($tot != 1) {
			$arr = array(
        'admin_fname'                         => $_POST['admin_fname'],
        'admin_password'                      => $_POST['admin_password'],
		    'admin_role'                          => 'merchant_user',
        'country_add_status'                  => 1,
        'country_view_status'                 => 1,
        'city_add_status'                     => 1,
        'city_view_status'                    => 1,
        'eventcat_add_status'                 => 1,
        'eventcat_view_status'                => 1,
        'eventman_add_status'                 => 1,
        'eventman_view_status'                => 1,
        'newscat_add_status'                  => 1,
        'newscat_view_status'                 => 1,
        'news_add_status'                     => 1,
        'news_view_status'                    => 1,
        'gallery_add_status'                  => 1,
        'gallery_view_status'                 => 1,
        'membership_add_status'               => 1,
        'membership_view_status'              => 1,
        'members_add_status'                  => 1,
        'members_view_status'                 => 1,
		  );
		  $db->insert(TABLE_ADMIN,$arr,true);
		  $db->redirect("home.php?pages=view-merchant");
		}else{
			echo "<script>alert('This User Is Already Exist')</script>";
			$db->redirect("home.php?pages=add-merchant");
		}
	}else {
			$arr = array(
        'admin_fname'                         => $_POST['admin_fname'],
        'admin_password'                      => $_POST['admin_password'],
		  );
			$condition = "admin_id='".$_GET['edit_id']."' ";
		  $db->update(TABLE_ADMIN,$arr,$condition,true);
		  $db->redirect("home.php?pages=view-merchant");
		}
}
?>


<script type="text/javascript">
  function validatecountry() {
    if(document.getElementById('admin_fname').value == "")
    {
    alert("Please Enter Merchant Name");
    document.getElementById('admin_fname').focus();
    return false;
    }
    if(document.getElementById('admin_password').value == "")
    {
    alert("Please Enter Merchant Password");
    document.getElementById('admin_password').focus();
    return false;
    }
    return true;
  }
</script>


  <!-- Page Content Start -->
  <!-- ================== -->

<div class="wraper container-fluid">
    <div class="page-title">
      <h3 class="title">Add Merchant</h3>
    </div>
    <div class="row">
      <div class="col-sm-12">
        <div class="panel panel-default">

          <div class="panel-body">
            <div class="form">
              <form class="cmxform form-horizontal tasi-form"  method="post"  onSubmit="return validatecountry()" action="">
                <div class="form-group ">
                  <label class="control-label col-lg-2">Merchant User Name*</label>
                  <div class="col-lg-10">
                    <?php 
		                if ($_GET['edit_id']!="") {
		                  $disabl = "disabled";
		                }else
		                {
		                   $disabl = "";
		                }
	                ?>
                    <input type="text" class="form-control" value='<?php  echo $country_res['admin_fname'];?>' placeholder="Merchant Name" name="admin_fname" id="admin_fname" autocomplete="off" <?php echo $disabl?>>
                  </div>
                </div>
                <div class="form-group ">
                  <label class="control-label col-lg-2">Merchant Password*</label>
                  <div class="col-lg-10">
                    <input type="text" class="form-control" value='<?php echo $country_res['admin_password'];?>' placeholder="Merchant Password" name="admin_password" id="admin_password">
                  </div>
                </div>

                <div class="form-group">
                  <div class="col-lg-offset-2 col-lg-10">
										<?php if(!isset($_GET['edit_id'])) { ?>
                    	<input type="submit" class=" btn btn-info col-md-4 col-sm-6 col-xs-12" id="save" name="save" value="Save" >
										<?php } else { ?>
											<input type="submit" class=" btn btn-info col-md-4 col-sm-6 col-xs-12" id="save" name="save" value="Update" >
										<?php } ?>
                  </div>
                </div>
              </form>
            </div>
            <!-- .form -->

          </div>
          <!-- panel-body -->
        </div>
        <!-- panel -->
      </div>
      <!-- col -->

    </div>
    <!-- End row -->

  </div>

  <!-- Page Content Ends -->
  <!-- ================== -->

</section>
<!-- Main Content Ends -->

</body>
</html>
