<?php 
if ($_SESSION['ADMIN']['ID']=="") {
    $db->redirect("index.php");
}
include_once '../apporioconfig/start_up.php';

$admininfo = $db->db_get_array("SELECT * FROM ".TABLE_ADMIN." WHERE admin_id='".$_SESSION['ADMIN']['ID']."'");
  foreach($admininfo as $admininfos);

if ($admininfos['admin_role']=='normal_user') {
   $muser_id = $admininfos['admin_user_id'];
}else
{
    $muser_id = $admininfos['admin_id'];
}
?>
<header class="top-head container-fluid">
                <button type="button" class="navbar-toggle pull-left">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <!-- Left navbar -->
                <!-- Right navbar -->
                <ul class="list-inline navbar-right top-menu top-right-menu">  
                   
                    <!-- user login dropdown start-->
                    <li class="dropdown text-center">
                        <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                    <img alt="" class="img-circle" src="images/user.png" />
                                    <span class="username username-hide-on-mobile">  Mr. <?php echo $admininfos['admin_fname'];?></span>
                                    <i class="fa fa-angle-down"></i>
                                </a>
                        <ul class="dropdown-menu extended pro-menu fadeInUp animated" tabindex="5003" style="overflow: hidden; outline: none;">
                                    <?php if($admininfos['admin_role']=='merchant_user') {?>
                                    <li>
                                        <a href="">
                                         <i class="fa fa-user" aria-hidden="true"></i>My Profile </a>
                                    </li>
                                   
								     <li>
                                        <a href="home.php?pages=account-setting"><i class="fa fa-cogs" aria-hidden="true"></i>Account Setting </a>
                                    </li>
                                     
									 <li>
                                        <a href="">
                                            <i class="fa fa-key" aria-hidden="true"></i>Change Password </a>
                                    </li>
                                    <?php }?>
                                     
									 <li>
                                        <a href="home.php?pages=logout">
                                           <i class="fa fa-sign-out"></i>Log Out </a>
                                    </li>
                        </ul>
                    </li>
                    <!-- user login dropdown end -->       
                </ul>
                <!-- End right navbar -->

            </header>