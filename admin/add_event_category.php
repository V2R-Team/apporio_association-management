<?php
include('common.php');
include_once '../apporioconfig/start_up.php';
if ($admininfos['eventcat_add_status'] != 1) {
  $db->redirect("home.php?pages=dashboard");
}
if(isset($_GET['edit_id'])) {
		$event_cat_data = $db->db_get_array("SELECT * FROM ".TABLE_EVENT_CATEGORY." WHERE event_category_id='".$_GET['edit_id']."' ");
		foreach($event_cat_data as $event_cat_res);
}

if($_SERVER['REQUEST_METHOD'] == "POST") {
  if(!isset($_GET['edit_id'])) {
    $slug = $db->slugify($_POST['event_category_name'], TABLE_EVENT_CATEGORY, 'event_cat_slug');
    $arr = array(
      'event_category_name'     => $_POST['event_category_name'],
       'event_category_name_other'     => $_POST['event_category_name_other'],
      'madmin_id'        => $muser_id,
      'event_cat_slug'=>$slug,
    );
    $db->insert(TABLE_EVENT_CATEGORY,$arr,true);
    $db->redirect("home.php?pages=view_event_category");
  } else {
    $arr = array(
      'event_category_name'     => $_POST['event_category_name'],
       'event_category_name_other'     => $_POST['event_category_name_other']
    );
    $condition = "event_category_id='".$_GET['edit_id']."' ";
    $db->update(TABLE_EVENT_CATEGORY,$arr,$condition,true);
    $db->redirect("home.php?pages=view_event_category");
  }
}
?>

<script type="text/javascript">
  function validatevent() {
    if(document.getElementById('event_category_name').value == "")
    {
      alert("Please Enter Category");
      document.getElementById('event_category_name').focus();
      return false;
    }
    return true;
  }
</script>


  <!-- Page Content Start -->
  <!-- ================== -->

<div class="wraper container-fluid">
    <div class="page-title">
      <h3 class="title">Add Category</h3>
    </div>
    <div class="row">
      <div class="col-sm-12">
        <div class="panel panel-default">

          <div class="panel-body">
            <div class="form">
              <form class="cmxform form-horizontal tasi-form"  method="post"  onSubmit="return validatevent()" action="">
                <div class="form-group ">
                  <label class="control-label col-lg-2">Add Category*</label>
                  <div class="col-lg-10">
                    <input type="text" class="form-control" value="<?php echo $event_cat_res['event_category_name']?>" placeholder="Add Category Name" name="event_category_name" id="event_category_name">
                  </div>
                </div>

                <div class="form-group ">
                  <label class="control-label col-lg-2">Add Category in french*</label>
                  <div class="col-lg-10">
                    <input type="text" class="form-control" value="<?php echo $event_cat_res['event_category_name_other']?>" placeholder="Add Category in french" name="event_category_name_other" id="event_category_name_other">
                  </div>
                </div>

                <div class="form-group">
                  <div class="col-lg-offset-2 col-lg-10">
                    <?php if(!isset($_GET['edit_id'])) { ?>
                    	<input type="submit" class=" btn btn-info col-md-4 col-sm-6 col-xs-12" id="save" name="save" value="Save" >
										<?php } else { ?>
											<input type="submit" class=" btn btn-info col-md-4 col-sm-6 col-xs-12" id="save" name="save" value="Update" >
										<?php } ?>
                  </div>
                </div>
              </form>
            </div>
            <!-- .form -->

          </div>
          <!-- panel-body -->
        </div>
        <!-- panel -->
      </div>
      <!-- col -->

    </div>
    <!-- End row -->

  </div>

  <!-- Page Content Ends -->
  <!-- ================== -->

</section>
<!-- Main Content Ends -->

</body>
</html>
