<?php

include('common.php');
include_once '../apporioconfig/start_up.php';

if ($admininfos['country_add_status'] != 1) {
  $db->redirect("home.php?pages=dashboard");
}

if(isset($_GET['edit_id'])) {
		$country_data = $db->db_get_array("SELECT * FROM ".TABLE_COUNTRY." WHERE country_id='".$_GET['edit_id']."' ");
		foreach($country_data as $country_res);
}

if($_SERVER['REQUEST_METHOD'] == "POST") {
	if(!isset($_GET['edit_id'])) {
			$arr = array(
		    'country_name'     => $_POST['country_name'],
		    'country_name_other'     => $_POST['country_name_other'],
        'madmin_id'        => $muser_id,
		  );
		  $db->insert(TABLE_COUNTRY,$arr,true);
		  $db->redirect("home.php?pages=view-counrty");
		} else {
			$arr = array(
		    'country_name'     => $_POST['country_name'],
		     'country_name_other'     => $_POST['country_name_other']
		  );
			$condition = "country_id='".$_GET['edit_id']."' ";
		  $db->update(TABLE_COUNTRY,$arr,$condition,true);
		  $db->redirect("home.php?pages=view-counrty");
		}
}
?>


<script type="text/javascript">
  function validatecountry() {
    if(document.getElementById('country_name').value == "")
    {
    alert("Please Enter Country Name");
    document.getElementById('country_name').focus();
    return false;
    }
    return true;
  }
</script>


  <!-- Page Content Start -->
  <!-- ================== -->

<div class="wraper container-fluid">
    <div class="page-title">
      <h3 class="title">Add Country</h3>
    </div>
    <div class="row">
      <div class="col-sm-12">
        <div class="panel panel-default">

          <div class="panel-body">
            <div class="form">
              <form class="cmxform form-horizontal tasi-form"  method="post"  onSubmit="return validatecountry()" action="">
                <div class="form-group ">
                  <label class="control-label col-lg-2">Add Country*</label>
                  <div class="col-lg-10">
                    <input type="text" class="form-control" value='<?php echo $country_res['country_name'];?>' placeholder="Add Country Name" name="country_name" id="country_name">
                  </div>
                </div>
                <div class="form-group ">
                  <label class="control-label col-lg-2">Add Country in french*</label>
                  <div class="col-lg-10">
                    <input type="text" class="form-control" value='<?php echo $country_res['country_name_other'];?>' placeholder="Add Country Name Other" name="country_name_other" id="country_name_other">
                  </div>
                </div>

                <div class="form-group">
                  <div class="col-lg-offset-2 col-lg-10">
										<?php if(!isset($_GET['edit_id'])) { ?>
                    	<input type="submit" class=" btn btn-info col-md-4 col-sm-6 col-xs-12" id="save" name="save" value="Save" >
										<?php } else { ?>
											<input type="submit" class=" btn btn-info col-md-4 col-sm-6 col-xs-12" id="save" name="save" value="Update" >
										<?php } ?>
                  </div>
                </div>
              </form>
            </div>
            <!-- .form -->

          </div>
          <!-- panel-body -->
        </div>
        <!-- panel -->
      </div>
      <!-- col -->

    </div>
    <!-- End row -->

  </div>

  <!-- Page Content Ends -->
  <!-- ================== -->

</section>
<!-- Main Content Ends -->

</body>
</html>
