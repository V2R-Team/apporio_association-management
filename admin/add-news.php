<?php 
include('common.php'); 
include_once '../apporioconfig/start_up.php';
if ($admininfos['news_add_status'] != 1) {
  $db->redirect("home.php?pages=dashboard");
}
if(isset($_GET['edit_id'])) {
    $news_data = $db->db_get_array("SELECT * FROM ".TABLE_NEWS." WHERE news_id='".$_GET['edit_id']."' ");
    foreach($news_data as $news_datas);

}

if($_SERVER['REQUEST_METHOD'] == "POST") { 
  if(!isset($_GET['edit_id'])) { 
    $slug = $db->slugify($_POST['event_category_name'], TABLE_EVENT_CATEGORY, 'event_cat_slug');
    $arr = array(
      'news_category_id'    => $_POST['news_category_id'],
      'news_name'           => $_POST['news_name'],
      'news_name_other'           => $_POST['news_name_other'],
      'news_description'    => $_POST['news_description'],
       'news_description_other'    => $_POST['news_description_other'],
      'madmin_id'            => $muser_id,
      'news_video'          => $_POST['news_video'],
      'news_country'        => $_POST['news_country'],
      'news_city'           => $_POST['news_city'],
      'news_slug'            => $slug,
    );
    $insert_id =  $db->insert(TABLE_NEWS,$arr,true);
    
    $condition = " `news_id` = '".$insert_id."' ";
    if($_FILES['news_image']['name'] != "") {
      $ext = substr($_FILES['news_image']['name'],-4);
      if($ext == ".jpg" || $ext == ".png" || $ext == ".gif" || $ext == "jpeg") {
        $ext = ($ext == "jpeg") ? ".jpg" : $ext;
        $dir    = "../uploads/news_image";
        if(!is_dir($dir) || !file_exists($dir)) mkdir($dir,0755);
        $filename = 'uploads/news_image/'.$insert_id.$ext;
        $filepath = "../".$filename;
        if(file_exists($filepath)) unlink($filepath);         
        copy($_FILES['news_image']['tmp_name'],$filepath);
      }
      $db->update(TABLE_NEWS,array('news_image' => $filename),$condition);
    }
    $db->redirect("home.php?pages=view-news");
  }else{

    $arr = array(
      'news_category_id'    => $_POST['news_category_id'],
      'news_name'           => $_POST['news_name'],
      'news_name_other'           => $_POST['news_name_other'],
      'news_description'    => $_POST['news_description'],
       'news_description_other'    => $_POST['news_description_other'],
      'news_video'          => $_POST['news_video'],
      'news_country'        => $_POST['news_country'],
      'news_city'           => $_POST['news_city'],
    );

    $condition = "news_id='".$_GET['edit_id']."' ";
    $db->update(TABLE_NEWS,$arr,$condition,true);

    $condition = " `news_id` = '".$insert_id."' ";
    if($_FILES['news_image']['name'] != "") {
      $ext = substr($_FILES['news_image']['name'],-4);
      if($ext == ".jpg" || $ext == ".png" || $ext == ".gif" || $ext == "jpeg") {
        $ext = ($ext == "jpeg") ? ".jpg" : $ext;
        $dir    = "../uploads/news_image";
        if(!is_dir($dir) || !file_exists($dir)) mkdir($dir,0755);
        $filename = 'uploads/news_image/'.$insert_id.$ext;
        $filepath = "../".$filename;
        if(file_exists($filepath)) unlink($filepath);         
        copy($_FILES['news_image']['tmp_name'],$filepath);
      }
      $db->update(TABLE_NEWS,array('news_image' => $filename),$condition);
    }
    $db->redirect("home.php?pages=view-news");
  }
}

?>

<script type="text/javascript">


function change_clty(country_id){
  $.ajax({
   url:"ajax.php?action=change_city&country_id="+country_id,
    success:function(response){
      $("#news_city").html(response);
    }
  });
 }

function validatenews() {

  if(document.getElementById('news_category_id').value == "")
  {
  alert("Please Select Category");
  document.getElementById('news_category_id').focus();
  return false;
  }

  if(document.getElementById('news_name').value == "")
  {
  alert("Please Enter News Title");
  document.getElementById('news_name').focus();
  return false;
  }

  if(document.getElementById('news_description').value == "")
  {
  alert("Please Enter News Description");
  document.getElementById('news_description').focus();
  return false;
  }
  
  
  if(document.getElementById('news_name_other').value == "")
  {
  alert("Please Enter News Title");
  document.getElementById('news_name_other').focus();
  return false;
  }

  if(document.getElementById('news_description_other').value == "")
  {
  alert("Please Enter News Description Other");
  document.getElementById('news_description_other').focus();
  return false;
  }

  if(document.getElementById('news_image').value == "")
  {
  alert("Please Enter Image");
  document.getElementById('news_image').focus();
  return false;
  }

  if(document.getElementById('news_video').value == "")
  {
  alert("Please Enter Video Link");
  document.getElementById('news_video').focus();
  return false;
  }

  if(document.getElementById('news_country').value == "")
  {
  alert("Please Select Country");
  document.getElementById('news_country').focus();
  return false;
  }

  if(document.getElementById('news_city').value == "")
  {
  alert("Please Select City");
  document.getElementById('news_city').focus();
  return false;
  }

  return true;

}
</script>

  <!-- Page Content Start --> 
  <!-- ================== -->
  
<div class="wraper container-fluid">
    <div class="page-title">
      <h3 class="title">News Management</h3>
    </div>
    <div class="row">
      <div class="col-sm-12">
        <div class="panel panel-default">
          
          <div class="panel-body">
            <div class="form">
              <form class="cmxform form-horizontal tasi-form"  method="post"  enctype="multipart/form-data" onSubmit="return validatenews()" action="">
                <div class="form-group ">
                  <label class="control-label col-lg-2">Select Category *</label>
                  <div class="col-lg-10">
                     <select class="form-control" name="news_category_id" id="news_category_id">
                        <option value="">--Please Select Category --</option>
                        <?php $news_category = $db->db_get_array("SELECT news_category_id,news_category_name FROM ".TABLE_NEWS_CATEGORY." WHERE madmin_id='".$muser_id."' AND news_category_status = 1" ); 
                          foreach ($news_category as $news_categorys) {?>
                           <option <?php if($news_datas['news_category_id'] == $news_categorys['news_category_id']) echo "selected=selected";?> value="<?php echo $news_categorys['news_category_id']?>"><?php echo $news_categorys['news_category_name']?></option>
                         <?php }?>
                    </select>
                </div>
                </div>  
                <div class="form-group ">
                  <label class="control-label col-lg-2">News  Title*</label>
                  <div class="col-lg-10">
                    <input type="text" class="form-control" value="<?php echo $news_datas['news_name']?>" placeholder="News Title" name="news_name" id="news_name">
                  </div>
                </div>
                
                       
                <div class="form-group ">
                  <label class="control-label col-lg-2">News  Title in french*</label>
                  <div class="col-lg-10">
                    <input type="text" class="form-control" value="<?php echo $news_datas['news_name_other']?>" placeholder="News Title in french" name="news_name_other" id="news_name_other">
                  </div>
                </div>
                
                <div class="form-group ">
                  <label class="control-label col-lg-2">News Description *</label>
                  <div class="col-lg-10">
                    <textarea class="form-control" placeholder="Event Description" name="news_description" id="news_description"><?php echo $news_datas['news_description']?></textarea>
                  </div>
                </div>
         

                
                
                <div class="form-group ">
                  <label class="control-label col-lg-2">News Description in french*</label>
                  <div class="col-lg-10">
                    <textarea class="form-control" placeholder="Event Description in french" name="news_description_other" id="news_description_other"><?php echo $news_datas['news_description_other']?></textarea>
                  </div>
                </div>



                   <div class="form-group ">
                  <label for="username" class="control-label col-lg-2">Upload Image*</label>
                  <div class="col-lg-10">

                    <div class="input-group image-preview">
                <input type="text" class="form-control image-preview-filename" value="<?php echo $news_datas['news_image']?>" disabled="disabled"> <!-- don't give a name === doesn't send on POST/GET -->
                <span class="input-group-btn">
                    <!-- image-preview-clear button -->
                    <button type="button" class="btn btn-default image-preview-clear" style="display:none;">
                        <span class="glyphicon glyphicon-remove"></span> Clear
                    </button>
                    <!-- image-preview-input -->
                    <div class="btn btn-default image-preview-input">
                        <span class="glyphicon glyphicon-folder-open"></span>
                        <span class="image-preview-input-title">Browse</span>
                        <input type="file" value="<?php echo $news_datas['news_image']?>" accept="image/png, image/jpeg, image/gif" name="news_image" id="news_image"> <!-- rename it -->
                    </div>
                </span>
              </div>
                  </div>
                </div>
                 
                <div class="form-group ">
                  <label class="control-label col-lg-2">News Video*</label>
                  <div class="col-lg-10">
                    <input type="text" class="form-control" value="<?php echo $news_datas['news_video']?>"  placeholder="Paste Video Link" name="news_video" id="news_video">
                  </div>
                </div>

                

                <div class="form-group ">
                  <label class="control-label col-lg-2">Select Country *</label>
                  <div class="col-lg-10">
                     <select class="form-control" name="news_country" id="news_country" onchange="change_clty(this.value)">
                        <option value="">--Please Select Country --</option>
                        <?php $country_name = $db->db_get_array("SELECT country_id,country_name FROM ".TABLE_COUNTRY." WHERE madmin_id='".$muser_id."' AND country_status = 1" ); 
                          foreach ($country_name as $country_names) {?>
                           <option <?php if($news_datas['news_country'] == $country_names['country_id']) { echo "selected=selected"; } ?> value="<?php echo $country_names['country_id']?>"><?php echo $country_names['country_name']?></option>
                         <?php }?>
                    </select>
                </div>
                </div> 

                 <div class="form-group ">
                  <label class="control-label col-lg-2">Select City *</label>
                  <div class="col-lg-10">
                     

                     <?php if(!isset($_GET['edit_id'])) {?>
                       <select class="form-control" name="news_city" id="news_city">
                          <option value="">--Please Select City --</option>
                       </select>
                     <?php } else { ?>
                       <select class="form-control" name="news_city" id="news_city" >
                          <option value="">--Please Select Country --</option>
                          <?php $city_name = $db->db_get_array("SELECT city_id,city_name FROM ".TABLE_CITY." WHERE city_country_id = ".$news_datas['news_country']." " );
                            foreach ($city_name as $city_names) {?>
                             <option <?php if($news_datas['news_city'] == $city_names['city_id']) { echo "selected=selected"; } ?> value="<?php echo $city_names['city_id']?>"><?php echo $city_names['city_name']?></option>
                           <?php }?>
                       </select>
                     <?php } ?>

                  </div>
                </div> 

                  
                <div class="form-group">
                  <div class="col-lg-offset-2 col-lg-10">
                    <input type="submit" class=" btn btn-info col-md-4 col-sm-6 col-xs-12" id="save" name="save" value="Save" >
                  </div>
                </div>


                


              </form>
            </div>
            <!-- .form --> 
            
          </div>
          <!-- panel-body --> 
        </div>
        <!-- panel --> 
      </div>
      <!-- col --> 
      
    </div>
    <!-- End row --> 
    
  </div>
  
  <!-- Page Content Ends --> 
  <!-- ================== --> 
  
</section>
<!-- Main Content Ends -->

</body>
</html>
<script type="text/javascript" src="js/wickedpicker.min.js"></script> 
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css">

        <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>

<link rel="stylesheet" type="text/css" href="css/wickedpicker.min.css">



<script>
   $(function() {
       $('.start_time').datepicker({

         format: "yyyy-mm-dd" 

     });
   });
   $(function() {
       $('.end_date').datepicker({  

        format: "yyyy-mm-dd" });
   });

    $('.start').wickedpicker();

     $('.end').wickedpicker();
  
$('.time').bootstrapMaterialDatePicker({ date: false });


</script>