<?php 
include('common.php');     
if ($admininfos['membership_view_status'] != 1) {
  $db->redirect("home.php?pages=dashboard");
}
$table = TABLE_MEMBERSHIP;

if(isset($_GET['status']) && isset($_GET['id'])) {
  $arr = array(
    'membership_status' => $_GET['status']
  );
  $condition = " membership_id = '".$_GET['id']."' ";
  
  $db->update($table,$arr,$condition);

  $db->redirect("home.php?pages=view-membership");
}
if(isset($_GET['delid'])) {
  $condition = " membership_id = '".$_GET['delid']."'";
  $db->delete($table,$condition);
  $db->redirect("home.php?pages=view-membership");
}
if(isset($_POST['command']) && $_POST['command'] == "m delete") {
  $ids = implode(",",$_POST['chk']);
  $condition = " membership_id IN ($ids) ";
  $db->delete($table,$condition);
  $db->redirect("home.php?pages=view-membership");
}
    
?>
<script>
function checkall(a) {
  if(a.checked) {
    $("input[type='checkbox']").prop("checked",true);
  } else {
    $("input[type='checkbox']").prop("checked",false);
  }
}
function uncheck() {
  var tot_ch = $("input[type='checkbox']").length;
  var chek_ch = $("input[type='checkbox']:checked").length;
  if(tot_ch == chek_ch + 1 && document.getElementById('main_ch').checked == false) {
    $("#main_ch").prop("checked",true);
  } else {
    $("#main_ch").prop("checked",false);
  }
}
function godelete() {
  var tot_chk = $("input[type='checkbox']:checked").length;
  if(tot_chk > 0) {
    if(confirm("Are You Want To Delete Selected Records!")) {
      if(confirm("Are You Sure To Delete Permanently Records!")) {
        //alert('test');
          document.form1.command.value = "m delete";
          document.form1.submit();
      }
    }
  } else {
    alert("Please Select Atlease One Record To Delete!");
  }
}
function single_delete(id) {
  if(confirm("Are You Want To Delete This Records!")) {
    if(confirm("Are You Sure To Delete Permanently Records!")) {
      window.location = "home.php?pages=view-membership&delid="+id;
    }
  }
}
</script>


<!-- Page Content Start -->
<!-- ================== -->
<form method="post" name="form1">
<input type="hidden" name="command" value="">
<div class="wraper container-fluid">
  <div class="page-title">
    <h3 class="title">View Membership</h3>
    <?php if ($admininfos['membership_add_status'] == 1) {?>
<a href="javascript:godelete()"><button type="button" name="delete" value="delete" class="btn btn-danger glyphicon glyphicon-trash"></button></a>
<?php }?>
  </div>
  
  <div class="row">
    <div class="col-md-12">
      <div class="panel panel-default">
        
        <div class="panel-body">
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12 mobtbl">
              <table id="datatable" class="table table-striped table-bordered table-responsive">
                <thead>
                  <tr>
                     <?php if ($admininfos['membership_add_status'] == 1) {?>
                    <th width="10%"> 
                      <label class="option block mn">
                        <input type="checkbox" id="main_ch" onClick="checkall(this)" >
                        <span class=""></span> Select
                      </label> 
                    </th>
                    <?php }?>
                    <th width="5%">S.No</th>
                    <th>Membership Name</th>
                     <th>Membership Name in french</th>
                    <th>Country</th>
                    <th>City</th>
                    <th>Duration</th>
                    <th>Price</th>
                    <?php if ($admininfos['membership_add_status'] == 1) {?>
                    <th width="8%">Status</th>
                    <?php }?>
                  </tr>
                </thead>
                <tbody>
                  
                  <?php $membership = $db->db_get_array("SELECT * FROM ".TABLE_MEMBERSHIP." WHERE madmin_id='".$muser_id."'");
                      $sn = 0;
                      foreach ($membership as $memberships) {
                        $sn++;
                  ?>
                    <tr>
                       <?php if ($admininfos['membership_add_status'] == 1) {?>
                        <td> 
                          <label class="option block mn" style="width: 55px;">
                             <input type="checkbox" name="chk[]" value="<?php echo $memberships['membership_id']?>" onClick="uncheck()" >
                             <span class="checkbox mn"></span>
                          </label>
                        </td>
                        <?php }?>
                        <td><?php echo $sn;?></td>
                         <?php if ($admininfos['membership_add_status'] == 1) {?>
                        <td><a title="Edit" href="home.php?pages=add-membership&edit_id=<?php echo $memberships['membership_id']; ?>"><?php echo $memberships['membership_name']?></a></td>
                        <?php }else{?>
                        <td><?php echo $memberships['membership_name']?></td>
                        <?php }?>
                        
                         <?php if ($admininfos['membership_add_status'] == 1) {?>
                        <td><a title="Edit" href="home.php?pages=add-membership&edit_id=<?php echo $memberships['membership_id']; ?>"><?php echo $memberships['membership_name_other']?></a></td>
                        <?php }else{?>
                        <td><?php echo $memberships['membership_name_other']?></td>
                        <?php }?>
                        
                        
                        <td>
                          <?php $country_name = $db->db_get_array("SELECT country_name FROM ".TABLE_COUNTRY." WHERE country_id = ".$memberships['membership_country_id']." " ); 
                            echo $country_name[0]['country_name'];
                          ?>
                        </td>
                        <td>
                          <?php $city_name = $db->db_get_array("SELECT city_name FROM ".TABLE_CITY." WHERE city_id = ".$memberships['membership_city_id']." " ); 
                            echo $city_name[0]['city_name'];
                          ?>
                        </td>
                        <td style="text-transform: capitalize;"><?php echo $memberships['membership_duration']?></td>
                        <td><?php echo $memberships['membership_price']?></td>
                       
                         <?php if ($admininfos['membership_add_status'] == 1) {?>
                        <?php if ($memberships['membership_status'] != 0) {?>
                          <td class="text-center">
                            <a href="home.php?pages=view-membership&status=0&id=<?php echo $memberships['membership_id']?>" class="" title="Active">
                            <button type="button" class="btn btn-success br2 btn-xs fs12 activebtn" > Active
                            </button></a>
                          </td>
                        <?php } else { ?>
                          <td class="text-center">
                            <a href="home.php?pages=view-membership&status=1&id=<?php echo $memberships['membership_id']?>" class="" title="Deactive">
                                <button type="button" class="btn btn-danger  br2 btn-xs fs12 dropdown-toggle" > Deactive
                                </button></a>
                          </td>
                        <?php }?>
                        <?php }?>
                    </tr>
                  
                  <?php }?>


                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- End row --> 
  
</div>
</form>


<!-- Page Content Ends --> 
<!-- ================== -->

</section>
<!-- Main Content Ends -->

</body></html>