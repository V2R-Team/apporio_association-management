<?php
include('common.php');
include_once '../apporioconfig/start_up.php';
if ($admininfos['city_add_status'] != 1) {
  $db->redirect("home.php?pages=dashboard");
}
if(isset($_GET['edit_id'])) {
		$city_data = $db->db_get_array("SELECT * FROM ".TABLE_CITY." WHERE city_id='".$_GET['edit_id']."' ");
		foreach($city_data as $city_res);    
}

if($_SERVER['REQUEST_METHOD'] == "POST") {
  if(!isset($_GET['edit_id'])) {
    $arr = array(
      'city_country_id'     => $_POST['country_id'],
      'city_name'           => $_POST['city_name'],
       'city_name_other'           => $_POST['city_name_other'],
      'madmin_id'        => $muser_id,
    );
    $db->insert(TABLE_CITY,$arr,true);
    $db->redirect("home.php?pages=view-city");
  } else {
    $arr = array(
      'city_country_id'     => $_POST['country_id'],
      'city_name'           => $_POST['city_name'],
      'city_name_other'           => $_POST['city_name_other'],
    );
    $condition = "city_id='".$_GET['edit_id']."' ";
    $db->update(TABLE_CITY,$arr,$condition,true);
    $db->redirect("home.php?pages=view-city");
  }
}
?>

<script type="text/javascript">
  function validatecity() {
    if(document.getElementById('country_id').value == "")
    {
    alert("Please Select Country");
    document.getElementById('country_id').focus();
    return false;
    }

    if(document.getElementById('city_name').value == "")
    {
    alert("Please Enter City Name");
    document.getElementById('city_name').focus();
    return false;
    }
    return true;
  }
</script>
  <!-- Page Content Start -->
  <!-- ================== -->

<div class="wraper container-fluid">
    <div class="page-title">
      <h3 class="title">Add City Name</h3>
    </div>
    <div class="row">
      <div class="col-sm-12">
        <div class="panel panel-default">

          <div class="panel-body">
            <div class="form">
              <form class="cmxform form-horizontal tasi-form"  method="post"  onSubmit="return validatecity()" action="">

                <div class="form-group ">
                  <label class="control-label col-lg-2">Select Country *</label>
                  <div class="col-lg-10">
                     <select class="form-control" name="country_id" id="country_id">
                        <option value="">--Please Select Country --</option>
                        <?php $country_name = $db->db_get_array("SELECT country_id,country_name FROM ".TABLE_COUNTRY." WHERE country_status = 1 AND madmin_id='".$muser_id."'" );
                          foreach ($country_name as $country_names) { ?>
                           <option <?php if($city_res['city_country_id'] == $country_names['country_id']) { echo "selected=selected"; }?> value="<?php echo $country_names['country_id']?>"><?php echo $country_names['country_name']?></option>
                         <?php }?>
                    </select>
                  </div>
                </div>
                <div class="form-group ">
                  <label class="control-label col-lg-2">Add City*</label>
                  <div class="col-lg-10">
                    <input type="text" class="form-control" value='<?php echo $city_res['city_name'] ;?>' placeholder="Add City Name" name="city_name" id="city_name">
                  </div>
                </div>
                
                <div class="form-group ">
                  <label class="control-label col-lg-2">Add City in french*</label>
                  <div class="col-lg-10">
                    <input type="text" class="form-control" value='<?php echo $city_res['city_name'] ;?>' placeholder="Add City Name Other" name="city_name_other" id="city_name_other">
                  </div>
                </div>

                <div class="form-group">
                  <div class="col-lg-offset-2 col-lg-10">
                    <?php if(!isset($_GET['edit_id'])) { ?>
                    	<input type="submit" class=" btn btn-info col-md-4 col-sm-6 col-xs-12" id="save" name="save" value="Save" >
										<?php } else { ?>
											<input type="submit" class=" btn btn-info col-md-4 col-sm-6 col-xs-12" id="save" name="save" value="Update" >
										<?php } ?>
                  </div>
                </div>
              </form>
            </div>
            <!-- .form -->

          </div>
          <!-- panel-body -->
        </div>
        <!-- panel -->
      </div>
      <!-- col -->

    </div>
    <!-- End row -->

  </div>

  <!-- Page Content Ends -->
  <!-- ================== -->

</section>
<!-- Main Content Ends -->

</body>
</html>
