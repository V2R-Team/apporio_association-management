<aside class="left-panel">

<!-- brand -->
<div class="logo"> <a href="home.php?pages=dashboard" class="logo-expanded"> <!-- <img src="img/taxilogo.png" alt="logo"> --> <span class="nav-label"> Admin Panel</span> </a> </div>
<!-- / brand -->
<?php
$admininfos = $db->db_get_array("SELECT * FROM ".TABLE_ADMIN." WHERE admin_id='".$_SESSION['ADMIN']['ID']."'");
  foreach($admininfos as $admininfos);

?>
<!-- Navbar Start -->
<nav class="navigation">
  <ul class="list-unstyled">
    <!-- Dashboard Start -->
    <?php
      $li_open = $arr_open = $ul_open = "";
      if(empty($_REQUEST['pages'])) {
        $li_open    = "active open";
        $arr_open   = "open";
        $ul_open    = "style='display: block'";
      }
    ?>
    <li class="has-submenu <?php echo $li_open ?>"><a href="home.php?pages=dashboard"><i class="glyphicon glyphicon-home" aria-hidden="true"></i> <span class="nav-label">Dashboard</span><span class="selected"></span></a></li>


    <?php
    if ($admininfos['country_add_status'] == 1 || $admininfos['country_view_status']==1) {

      $li_open = $arr_open = $ul_open = "";
      if(@$_REQUEST['pages'] == "add-counrty" || @$_REQUEST['pages'] == "view-counrty") {
        $li_open    = "active open";
        $arr_open   = "open";
        $ul_open    = "style='display: block'";
      }
    ?>
    <li class="has-submenu <?php echo $li_open ?>"><a href="javascript:;"><i class="fa fa-globe" aria-hidden="true"></i>
      <span class="nav-label">Country</span><span class="selected"></span> <span class="arrow <?php echo $arr_open ?> "></span></a>
      <ul class="list-unstyled">
        <?php  if ($admininfos['country_add_status'] == 1){?>
          <li><a href="home.php?pages=add-counrty">Add Country</a></li>
        <?php }?>
        <?php  if ($admininfos['country_view_status'] == 1){?>
        <li><a href="home.php?pages=view-counrty">View Country</a></li>
        <?php }?>

      </ul>
    </li>
    <?php }?>

    <?php
    if ($admininfos['city_add_status'] == 1 || $admininfos['city_view_status']==1) {
      $li_open = $arr_open = $ul_open = "";
      if(@$_REQUEST['pages'] == "add-city" || @$_REQUEST['pages'] == "view-city") {
        $li_open    = "active open";
        $arr_open   = "open";
        $ul_open    = "style='display: block'";
      }
    ?>
    <li class="has-submenu <?php echo $li_open ?>"><a href="javascript:;"><i class="fa fa-location-arrow" aria-hidden="true"></i>
      <span class="nav-label">City</span><span class="selected"></span> <span class="arrow <?php echo $arr_open ?> "></span></a>
      <ul class="list-unstyled">
        <?php  if ($admininfos['city_add_status'] == 1){?>
        <li><a href="home.php?pages=add-city">Add City</a></li>
        <?php }?>
        <?php  if ($admininfos['city_view_status'] == 1){?>
          <li><a href="home.php?pages=view-city">View City</a></li>
        <?php }?>
      </ul>
    </li>
    <?php }?>


    <?php
    if ($admininfos['eventcat_add_status'] == 1 || $admininfos['eventcat_view_status']==1) {
      $li_open = $arr_open = $ul_open = "";
      if(@$_REQUEST['pages'] == "add_event_category" || @$_REQUEST['pages'] == "view_event_category") {
        $li_open    = "active open";
        $arr_open   = "open";
        $ul_open    = "style='display: block'";
      }
    ?>
    <li class="has-submenu <?php echo $li_open ?>"><a href="javascript:;"><i class="fa fa-calendar" aria-hidden="true"></i> <span class="nav-label" >Event Category</span><span class="selected"></span> <span class="arrow <?php echo $arr_open ?>"></span></a>
      <ul class="list-unstyled">
        <?php  if ($admininfos['eventcat_add_status'] == 1){?>
          <li><a href="home.php?pages=add_event_category">Add Event Category</a></li>
        <?php }?>
        <?php  if ($admininfos['eventcat_view_status'] == 1){?>
          <li><a href="home.php?pages=view_event_category">View Event Category</a></li>
        <?php }?>
      </ul>
    </li>
    <?php }?>

    <!-- Add car Start -->
    <?php
      if ($admininfos['eventman_add_status'] == 1 || $admininfos['eventman_view_status']==1) {
      $li_open = $arr_open = $ul_open = "";
      if(@$_REQUEST['pages'] == "add-event" || @$_REQUEST['pages'] == "view-event") {
        $li_open    = "active open";
        $arr_open   = "open";
        $ul_open    = "style='display: block'";
      }
    ?>
    <li class="has-submenu <?php echo $li_open ?>"><a href="javascript:;"><i class="fa fa-calendar" aria-hidden="true"></i> <span class="nav-label" >Event Management</span><span class="selected"></span> <span class="arrow <?php echo $arr_open ?>"></span></a>
      <ul class="list-unstyled">
        <?php  if ($admininfos['eventman_add_status'] == 1){?>
          <li><a href="home.php?pages=add-event">Add Event</a></li>
        <?php }?>
        <?php  if ($admininfos['eventman_view_status'] == 1){?>
          <li><a href="home.php?pages=view-event">View Event</a></li>
        <?php }?>
      </ul>
    </li>
    <?php }?>


    <?php
     if ($admininfos['newscat_add_status'] == 1 || $admininfos['newscat_view_status']==1) {
      $li_open = $arr_open = $ul_open = "";
      if(@$_REQUEST['pages'] == "add-cat-news" || @$_REQUEST['pages'] == "view-cat-news") {
        $li_open    = "active open";
        $arr_open   = "open";
        $ul_open    = "style='display: block'";
      }
    ?>
    <li class="has-submenu <?php echo $li_open ?>"><a href="javascript:;"><i class="fa fa-newspaper-o" aria-hidden="true"></i>
      </i> <span class="nav-label" >News Category</span><span class="selected"></span> <span class="arrow <?php echo $arr_open ?>"></span></a>
      <ul class="list-unstyled">
          <?php  if ($admininfos['newscat_add_status'] == 1){?>
            <li><a href="home.php?pages=add-cat-news">Add Category</a></li>
          <?php }?>
          <?php  if ($admininfos['newscat_view_status'] == 1){?>
            <li><a href="home.php?pages=view-cat-news">View Category</a></li>
          <?php }?>
      </ul>
    </li>
    <?php }?>


    <?php
     if ($admininfos['news_add_status'] == 1 || $admininfos['news_view_status']==1) {
      $li_open = $arr_open = $ul_open = "";
      if(@$_REQUEST['pages'] == "add-news" || @$_REQUEST['pages'] == "view-news") {
        $li_open    = "active open";
        $arr_open   = "open";
        $ul_open    = "style='display: block'";
      }
    ?>
    <li class="has-submenu <?php echo $li_open ?>"><a href="javascript:;"><i class="fa fa-newspaper-o" aria-hidden="true"></i>
      </i> <span class="nav-label" >News Management</span><span class="selected"></span> <span class="arrow <?php echo $arr_open ?>"></span></a>
      <ul class="list-unstyled">
        <?php  if ($admininfos['news_add_status'] == 1){?>
          <li><a href="home.php?pages=add-news">Add News</a></li>
        <?php }?>
        <?php  if ($admininfos['news_view_status'] == 1){?>
          <li><a href="home.php?pages=view-news">View News</a></li>
        <?php }?>
      </ul>
    </li>
    <?php }?>


    <?php
    if ($admininfos['gallery_add_status'] == 1 || $admininfos['gallery_view_status']==1) {
      $li_open = $arr_open = $ul_open = "";
      if(@$_REQUEST['pages'] == "add-gallery" || @$_REQUEST['pages'] == "view-gallery") {
        $li_open    = "active open";
        $arr_open   = "open";
        $ul_open    = "style='display: block'";
      }
    ?>
    <li class="has-submenu <?php echo $li_open ?>"><a href="javascript:;"><i class="fa fa-picture-o" aria-hidden="true"></i>
      </i> <span class="nav-label" >Gallery</span><span class="selected"></span> <span class="arrow <?php echo $arr_open ?>"></span></a>
      <ul class="list-unstyled">
        <?php  if ($admininfos['gallery_add_status'] == 1){?>
          <li><a href="home.php?pages=add-gallery">Add Gallery</a></li>
        <?php }?>
        <?php  if ($admininfos['gallery_view_status'] == 1){?>
          <li><a href="home.php?pages=view-gallery">View Gallery</a></li>
        <?php }?>
      </ul>
    </li>
    <?php }?>

<!-- Add Causes model Start -->
   <?php
    if ($admininfos['causes_add_status'] == "" || $admininfos['causes_view_status']=="") {
      $li_open = $arr_open = $ul_open = "";
      if(@$_REQUEST['pages'] == "add-causes" || @$_REQUEST['pages'] == "view-causes") {
        $li_open    = "active open";
        $arr_open   = "open";
        $ul_open    = "style='display: block'";
      }
    ?>
    <li class="has-submenu <?php echo $li_open ?>"><a href="javascript:;"><i class="fa fa-picture-o" aria-hidden="true"></i>
      </i> <span class="nav-label" >Causes</span><span class="selected"></span> <span class="arrow <?php echo $arr_open ?>"></span></a>
      <ul class="list-unstyled">
        <?php  if ($admininfos['causes_add_status'] == ""){?>
          <li><a href="home.php?pages=add-causes">Add Causes</a></li>
        <?php }?>
        <?php  if ($admininfos['causes_view_status'] == ""){?>
          <li><a href="home.php?pages=view-causes">View Causes</a></li>
        <?php }?>
      </ul>
    </li>
    <?php }?>
    
    
    <!-- Add Account model Start -->
   <?php
    if ($admininfos['account_add_status'] == "" || $admininfos['account_view_status']=="") {
      $li_open = $arr_open = $ul_open = "";
      if(@$_REQUEST['pages'] == "add-account" || @$_REQUEST['pages'] == "view-account") {
        $li_open    = "active open";
        $arr_open   = "open";
        $ul_open    = "style='display: block'";
      }
    ?>
    <li class="has-submenu <?php echo $li_open ?>"><a href="javascript:;"><i class="fa fa-picture-o" aria-hidden="true"></i>
      </i> <span class="nav-label" >Accounts</span><span class="selected"></span> <span class="arrow <?php echo $arr_open ?>"></span></a>
      <ul class="list-unstyled">
        <?php  if ($admininfos['account_add_status'] == ""){?>
          <li><a href="home.php?pages=add-account">Add Account</a></li>
        <?php }?>
      
      </ul>
    </li>
    <?php }?>
    
    
    

    <!-- Add car model Start -->
    <?php
    if ($admininfos['membership_add_status'] == 1 || $admininfos['membership_view_status']==1) {
      $li_open = $arr_open = $ul_open = "";
      if(@$_REQUEST['pages'] == "add-membership" || @$_REQUEST['pages'] == "view-membership") {
        $li_open    = "active open";
        $arr_open   = "open";
        $ul_open    = "style='display: block'";
      }
    ?>
    <li class="has-submenu <?php echo $li_open ?>"><a href="javascript:;"><i class="fa fa-users" aria-hidden="true"></i> <span class="nav-label">Membership Type</span><span class="selected"></span><span class="arrow <?php echo $arr_open ?>"></span></a>
      <ul class="list-unstyled">
        <?php  if ($admininfos['membership_add_status'] == 1){?>
          <li><a href="home.php?pages=add-membership">Add Membership </a></li>
        <?php }?>
        <?php  if ($admininfos['membership_view_status'] == 1){?>
          <li><a href="home.php?pages=view-membership">View Membership </a></li>
        <?php }?>
      </ul>
    </li>
    <?php }?>


    <!-- <?php
    if ($admininfos['members_add_status'] == 1 || $admininfos['members_view_status']==1) {
      $li_open = $arr_open = $ul_open = "";
      if(@$_REQUEST['pages'] == "add-member" || @$_REQUEST['pages'] == "view-member") {
        $li_open    = "active open";
        $arr_open   = "open";
        $ul_open    = "style='display: block'";
      }
    ?>
    <li class="has-submenu <?php echo $li_open ?>"><a href="javascript:;"><i class="fa fa-user" aria-hidden="true"></i>
      <span class="nav-label"> Members</span><span class="selected"></span><span class="arrow <?php echo $arr_open ?>"></span></a>
      <ul class="list-unstyled">
        <?php  if ($admininfos['members_add_status'] == 1){?>
          <li><a href="home.php?pages=add-member">Add Member </a></li>
        <?php }?>
        <?php  if ($admininfos['members_view_status'] == 1){?>
          <li><a href="home.php?pages=view-member">View Member </a></li>
        <?php }?>
      </ul>
    </li>
    <?php }?> -->

    <?php
    if ($admininfos['members_add_status'] == 1 || $admininfos['members_view_status']==1) {
      $li_open = $arr_open = $ul_open = "";
      if(@$_REQUEST['pages'] == "add-member" || @$_REQUEST['pages'] == "view-member") {
        $li_open    = "active open";
        $arr_open   = "open";
        $ul_open    = "style='display: block'";
      }
    ?>
    <li class="has-submenu <?php echo $li_open ?>"><a href="home.php?pages=membership-registration"><i class="glyphicon glyphicon-user" aria-hidden="true"></i> <span class="nav-label">Member</span><span class="selected"></span></a></li>

    <?php }?>





    <?php
    if ($_SESSION['ADMIN']['UN'] == 'merchant_user'){
      $li_open = $arr_open = $ul_open = "";
      if(@$_REQUEST['pages'] == "add-user" || @$_REQUEST['pages'] == "view-user") {
        $li_open    = "active open";
        $arr_open   = "open";
        $ul_open    = "style='display: block'";
      }
    ?>
    <li class="has-submenu <?php echo $li_open ?>"><a href="javascript:;"><i class="fa fa-users" aria-hidden="true"></i>
      <span class="nav-label"> Staff Member</span><span class="selected"></span><span class="arrow <?php echo $arr_open ?>"></span></a>
      <ul class="list-unstyled">
        <li><a href="home.php?pages=add-user">Add User </a></li>
        <li><a href="home.php?pages=view-user">View User </a></li>
      </ul>
    </li>

    <?php
      if ($admininfos['eventman_add_status'] == 1 || $admininfos['eventman_view_status']==1) {
      $li_open = $arr_open = $ul_open = "";
      if(@$_REQUEST['pages'] == "add-event" || @$_REQUEST['pages'] == "view-event") {
        $li_open    = "active open";
        $arr_open   = "open";
        $ul_open    = "style='display: block'";
      }
    ?>
    <li class="has-submenu <?php echo $li_open ?>"><a href="javascript:;"><i class="fa fa-newspaper-o" aria-hidden="true"></i> <span class="nav-label" >Pages</span><span class="selected"></span> <span class="arrow <?php echo $arr_open ?>"></span></a>
      <ul class="list-unstyled">

        <?php
          $pages_data = $db->db_get_array("SELECT * FROM ".TABLE_PAGES);
          foreach ($pages_data as $page_name) { ?>
          <li><a href="home.php?pages=page&page_id=<?php echo $page_name['page_id'];?>"><?php echo $page_name['title'];?></a></li>
        <?php  }?>

      </ul>
    </li>
    <?php }?>


    <?php
      $li_open = $arr_open = $ul_open = "";
      if(@$_REQUEST['pages'] == "view-renewmember") {
        $li_open    = "active open";
        $arr_open   = "open";
        $ul_open    = "style='display: block'";
      }
    ?>
    <li class="has-submenu <?php echo $li_open ?>"><a href="home.php?pages=view-renewmember"><i class="fa fa-repeat" aria-hidden="true"></i>
    <span class="nav-label" >Renew Membership </span><span class="selected"></span></a></li>
  <?php }?>

  <?php
  if ($_SESSION['ADMIN']['UN'] == 'admin_user'){

      $li_open = $arr_open = $ul_open = "";
      if(@$_REQUEST['pages'] == "add-merchant" || @$_REQUEST['pages'] == "view-merchant") {
        $li_open    = "active open";
        $arr_open   = "open";
        $ul_open    = "style='display: block'";
      }
    ?>
    <li class="has-submenu <?php echo $li_open ?>"><a href="javascript:;"><i class="fa fa-user" aria-hidden="true"></i>
      <span class="nav-label"> Merchant </span><span class="selected"></span><span class="arrow <?php echo $arr_open ?>"></span></a>
      <ul class="list-unstyled">
          <li><a href="home.php?pages=add-merchant">Add Merchant </a></li>
          <li><a href="home.php?pages=view-merchant">View Merchant </a></li>
      </ul>
    </li>

  <?php }?>
  </ul>
</nav>
</aside>
