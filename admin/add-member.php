<?php 
include('common.php'); 
include_once '../apporioconfig/start_up.php';
if ($admininfos['members_add_status'] != 1) {
  $db->redirect("home.php?pages=dashboard");
}
if(isset($_GET['edit_id'])) {
    $members_data = $db->db_get_array("SELECT * FROM ".TABLE_MEMBERS." WHERE members_id='".$_GET['edit_id']."' ");
    foreach($members_data as $members_datas);
    // echo "<pre>";
    // print_r($members_datas);
    // echo "</pre>";

}

if($_SERVER['REQUEST_METHOD'] == "POST") { 
  if(!isset($_GET['edit_id'])) { 
    $arr = array(
      'members_country_id'            => $_POST['members_country_id'],
      'members_city_id'               => $_POST['members_city_id'],
      'members_duration'              => $_POST['members_duration'],
      'members_type_id'               => $_POST['members_type_id'],
      'members_fname'                 => $_POST['members_fname'],
      'members_lname'                 => $_POST['members_lname'],
      'members_dob'                   => $_POST['members_dob'],
      'members_gender'                => $_POST['members_gender'],
      'members_phone'                 => $_POST['members_phone'],
      'members_address'               => $_POST['members_address'],
      'madmin_id'                     => $muser_id,
      'members_pin'                   => $_POST['members_pin'],
      'members_rnumber'               => $_POST['members_rnumber'],
      'members_entry_date'            => $_POST['members_entry_date'],
    );
    $insert_id =  $db->insert(TABLE_MEMBERS,$arr,true);

    $condition = " `members_id` = '".$insert_id."' ";

    if($_FILES['members_payment_receipt']['name'] != "") {
      $ext = substr($_FILES['members_payment_receipt']['name'],-4);
      if($ext == ".jpg" || $ext == ".png" || $ext == ".gif" || $ext == "jpeg") {
        $ext = ($ext == "jpeg") ? ".jpg" : $ext;
        $dir    = "../uploads/members_payment_receipt";
        if(!is_dir($dir) || !file_exists($dir)) mkdir($dir,0755);
        $filename = 'uploads/members_payment_receipt/'.$insert_id.$ext;
        $filepath = "../".$filename;
        if(file_exists($filepath)) unlink($filepath);         
        copy($_FILES['members_payment_receipt']['tmp_name'],$filepath);
      }
      $db->update(TABLE_MEMBERS,array('members_payment_receipt' => $filename),$condition);
    }
    $db->redirect("home.php?pages=view-member");

  }else{

    $arr = array(
      'members_country_id'            => $_POST['members_country_id'],
      'members_city_id'               => $_POST['members_city_id'],
      'members_duration'              => $_POST['members_duration'],
      'members_type_id'               => $_POST['members_type_id'],
      'members_fname'                 => $_POST['members_fname'],
      'members_lname'                 => $_POST['members_lname'],
      'members_dob'                   => $_POST['members_dob'],
      'members_gender'                => $_POST['members_gender'],
      'members_phone'                 => $_POST['members_phone'],
      'members_address'               => $_POST['members_address'],
      'members_pin'                   => $_POST['members_pin'],
      'members_rnumber'               => $_POST['members_rnumber'],
      'members_entry_date'            => $_POST['members_entry_date'],
    );
    $condition = "members_id='".$_GET['edit_id']."' ";
    $db->update(TABLE_MEMBERS,$arr,$condition,true);

    $condition = " `members_id` = '".$insert_id."' ";
    if($_FILES['members_payment_receipt']['name'] != "") {
      $ext = substr($_FILES['members_payment_receipt']['name'],-4);
      if($ext == ".jpg" || $ext == ".png" || $ext == ".gif" || $ext == "jpeg") {
        $ext = ($ext == "jpeg") ? ".jpg" : $ext;
        $dir    = "../uploads/members_payment_receipt";
        if(!is_dir($dir) || !file_exists($dir)) mkdir($dir,0755);
        $filename = 'uploads/members_payment_receipt/'.$insert_id.$ext;
        $filepath = "../".$filename;
        if(file_exists($filepath)) unlink($filepath);         
        copy($_FILES['members_payment_receipt']['tmp_name'],$filepath);
      }
      $db->update(TABLE_MEMBERS,array('members_payment_receipt' => $filename),$condition);
    }
    $db->redirect("home.php?pages=view-member");
  }
}
?>
<script>
function change_clty(country_id){
  $.ajax({
   url:"ajax.php?action=change_city&country_id="+country_id,
    success:function(response){
      $("#members_city_id").html(response);
    }
  });
 }

function change_mtype(m_dr){

  var ch_id = $("#members_country_id").val();
  var city_id = $("#members_city_id").val();
  //alert(ch_id);
  $.ajax({
   url:"ajax.php?action=change_mtype&m_dr="+m_dr+"&ch_id="+ch_id+"&city_id="+city_id,
    success:function(response){
      $("#members_type_id").html(response);
    }
  });
 }

function validatmemeber() {
  if(document.getElementById('members_country_id').value == "")
    {
    alert("Please Select Country");
    document.getElementById('members_country_id').focus();
    return false;
    }

    if(document.getElementById('members_city_id').value == "")
    {
    alert("Please Select City");
    document.getElementById('members_city_id').focus();
    return false;
    }

    if(document.getElementById('members_duration').value == "")
    {
    alert("Please Select Duration");
    document.getElementById('members_duration').focus();
    return false;
    }
    if(document.getElementById('members_type_id').value == "")
    {
    alert("Please Select Membership Type");
    document.getElementById('members_type_id').focus();
    return false;
    }

    if(document.getElementById('members_fname').value == "")
    {
    alert("Please Enter First Name");
    document.getElementById('members_fname').focus();
    return false;
    }

    if(document.getElementById('members_lname').value == "")
    {
    alert("Please Enter Last Name");
    document.getElementById('members_lname').focus();
    return false;
    }

    if(document.getElementById('members_dob').value == "")
    {
    alert("Please Enter Date Of Birth");
    document.getElementById('members_dob').focus();
    return false;
    }

    if(document.getElementById('members_gender').value == "")
    {
    alert("Please Select Gender");
    document.getElementById('members_gender').focus();
    return false;
    }

    if(document.getElementById('members_phone').value == "")
    {
    alert("Please Enter Phone No");
    document.getElementById('members_phone').focus();
    return false;
    }

    if(document.getElementById('members_address').value == "")
    {
    alert("Please Enter Address");
    document.getElementById('members_address').focus();
    return false;
    }
    if(document.getElementById('members_pin').value == "")
    {
    alert("Please Enter Post Code");
    document.getElementById('members_pin').focus();
    return false;
    }
    if(document.getElementById('members_payment_receipt').value == "")
    {
    alert("Please Upload Payment Receipt");
    document.getElementById('members_payment_receipt').focus();
    return false;
    }

    if(document.getElementById('members_pin').value == "")
    {
    alert("Please Enter Payment Reference No");
    document.getElementById('members_pin').focus();
    return false;
    }
    if(document.getElementById('members_entry_date').value == "")
    {
    alert("Please Enter Entry Date");
    document.getElementById('members_entry_date').focus();
    return false;
    }
  return true;
}
</script>


  <!-- Page Content Start --> 
  <!-- ================== -->
  
<div class="wraper container-fluid">
    <div class="page-title">
      <h3 class="title">Add Members</h3>
    </div>
    <div class="row">
      <div class="col-sm-12">
        <div class="panel panel-default">
          
          <div class="panel-body">
            <div class="form" >
              <form class="cmxform form-horizontal tasi-form"  method="post"  enctype="multipart/form-data" onSubmit="return validatmemeber()" action="">
 
                <div class="form-group ">
                  <label class="control-label col-lg-2">Select Country *</label>
                  <div class="col-lg-10">
                     <select class="form-control" name="members_country_id" id="members_country_id" onchange="change_clty(this.value)" >
                        <option value="">--Please Select Country --</option>
                        <?php $country_name = $db->db_get_array("SELECT country_id,country_name FROM ".TABLE_COUNTRY." WHERE madmin_id='".$muser_id."' AND country_status = 1" ); 
                          foreach ($country_name as $country_names) {?>
                           <option <?php if($members_datas['members_country_id'] == $country_names['country_id']) { echo "selected=selected"; } ?> value="<?php echo $country_names['country_id']?>"><?php echo $country_names['country_name']?></option>
                         <?php }?>
                    </select>
              </div>
                </div>   
                  <div class="form-group ">
                  <label class="control-label col-lg-2">Select City*</label>
                  <div class="col-lg-10">
                    <?php if(!isset($_GET['edit_id'])) {?>
                       <select class="form-control" name="members_city_id" id="members_city_id" >
                          <option value="">--Please Select City --</option>
                       </select>
                     <?php } else { ?>
                       <select class="form-control" name="members_city_id" id="members_city_id"  >
                          <option value="">--Please Select Country --</option>
                          <?php $city_name = $db->db_get_array("SELECT city_id,city_name FROM ".TABLE_CITY." WHERE city_country_id = ".$members_datas['members_country_id']." " );
                            foreach ($city_name as $city_names) {?>
                             <option <?php if($members_datas['members_city_id'] == $city_names['city_id']) { echo "selected=selected"; } ?> value="<?php echo $city_names['city_id']?>"><?php echo $city_names['city_name']?></option>
                           <?php }?>
                       </select>
                     <?php } ?>
              </div>
                </div>            
             <div class="form-group ">
                  <label class="control-label col-lg-2">Membership Duration*</label>
                  <div class="col-lg-10">
                     <select class="form-control" name="members_duration" id="members_duration" onchange="change_mtype(this.value)">
                        <option>--Please Select Duration --</option>
                          <option <?php if($members_datas['members_duration'] == "weekly") { echo "selected=selected"; } ?> value="weekly">Weekly</option>
                          <option <?php if($members_datas['members_duration'] == "monthly") { echo "selected=selected"; } ?> value="monthly">Monthly</option>
                          <option <?php if($members_datas['members_duration'] == "half-yearly") { echo "selected=selected"; } ?> value="half-yearly">Half Yearly</option>
                          <option <?php if($members_datas['members_duration'] == "yearly") { echo "selected=selected"; } ?> value="yearly">Yearly</option>
                        </select>
              </div>
                </div>  
                 <div class="form-group ">
                  <label class="control-label col-lg-2">Membership Type*</label>
                  <div class="col-lg-10">
                     

                     <?php if(!isset($_GET['edit_id'])) {?>
                         <select class="form-control" name="members_type_id" id="members_type_id" >
                            <option>--Please Select Type --</option>
                              
                         </select>
                     <?php } else { ?>
                       <select class="form-control" name="members_type_id" id="members_type_id" >
                          <option value="">--Please Select Type --</option>
                          <?php $mtype_name = $db->db_get_array("SELECT membership_id,membership_name FROM ".TABLE_MEMBERSHIP." WHERE membership_country_id = ".$members_datas['members_country_id']." AND membership_city_id = ".$members_datas['members_city_id']." AND membership_duration = '".$members_datas['members_duration']."'" );
                            foreach ($mtype_name as $mtype_names) {?>
                             <option <?php if($members_datas['members_type_id'] == $mtype_names['membership_id']) { echo "selected=selected"; } ?> value="<?php echo $mtype_names['membership_id']?>"><?php echo $mtype_names['membership_name']?></option>
                           <?php }?>
                       </select>
                     <?php } ?>


              </div>
                </div> 
               <div class="form-group ">
                  <label class="control-label col-lg-2">First Name*</label>
                  <div class="col-lg-10">
                    <input type="text" class="form-control" placeholder="First Name" name="members_fname" id="members_fname" value="<?php echo $members_datas['members_fname']?>">
                  </div>
                </div>


                 <div class="form-group ">
                  <label class="control-label col-lg-2">Last Name*</label>
                  <div class="col-lg-10">
                    <input type="text" class="form-control" placeholder="Last Name" name="members_lname" id="members_lname" value="<?php echo $members_datas['members_lname']?>">
                  </div>
                </div>




                   <div class="form-group ">
                  <label class="control-label col-lg-2">Date Of Birth*</label>
                  <div class="col-lg-10">
                    <input type="text" class="form-control my_datepicker" placeholder="Date Of Birth" name="members_dob" id="members_dob" value="<?php echo $members_datas['members_dob']?>">
                  </div>
                </div>



                 <div class="form-group ">
                  <label class="control-label col-lg-2">Gender*</label>
                  <div class="col-lg-10">
                     <select class="form-control" name="members_gender" id="members_gender">
                            <option>--Please Select Gender --</option>
                              <option <?php if($members_datas['members_gender'] == "male") { echo "selected=selected"; } ?> value="male">Male</option>
                             <option <?php if($members_datas['members_gender'] == "female") { echo "selected=selected"; } ?> value="female">Female</option>
                              <option <?php if($members_datas['members_gender'] == "other") { echo "selected=selected"; } ?> value="other">Other</option>
                            
                    </select>
                  </div>
                </div> 
                
                 <div class="form-group ">
                  <label class="control-label col-lg-2">Phone No.*</label>
                  <div class="col-lg-10">
                    <input type="text" class="form-control" placeholder="Phone No." name="members_phone" id="members_phone" value="<?php echo $members_datas['members_phone']?>">
                  </div>
                </div>


                
                <div class="form-group ">
                  <label class="control-label col-lg-2">Address *</label>
                  <div class="col-lg-10">
                    <textarea class="form-control" placeholder="Address " name="members_address" id="members_address"><?php echo $members_datas['members_address']?></textarea>
                  </div>
                </div>

                   <div class="form-group ">
                  <label class="control-label col-lg-2">Post Code*</label>
                  <div class="col-lg-10">
                    <input type="text" class="form-control" placeholder="Post Code." name="members_pin" id="members_pin" value="<?php echo $members_datas['members_pin']?>">
                  </div>
                </div>


                 <div class="form-group ">
                  <label for="username" class="control-label col-lg-2">Upload Payment Receipt*</label>
                  <div class="col-lg-10">

                    <div class="input-group image-preview">
                <input type="text" class="form-control image-preview-filename" disabled="disabled" value="<?php echo $members_datas['members_payment_receipt']?>"> <!-- don't give a name === doesn't send on POST/GET -->
                <span class="input-group-btn">
                    <!-- image-preview-clear button -->
                    <button type="button" class="btn btn-default image-preview-clear" style="display:none;">
                        <span class="glyphicon glyphicon-remove"></span> Clear
                    </button>
                    <!-- image-preview-input -->
                    <div class="btn btn-default image-preview-input">
                        <span class="glyphicon glyphicon-folder-open"></span>
                        <span class="image-preview-input-title">Browse</span>
                        <input type="file" accept="image/png, image/jpeg, image/gif" name="members_payment_receipt" value="<?php echo $members_datas['members_payment_receipt']?>" id="members_payment_receipt"  /> <!-- rename it -->
                    </div>
                </span>
              </div>

                  </div>
                </div>

                 <div class="form-group ">
                  <label class="control-label col-lg-2">Payment Reference No.*</label>
                  <div class="col-lg-10">
                    <input type="text" class="form-control" placeholder="Payment Reference No." value="<?php echo $members_datas['members_rnumber']?>" name="members_rnumber" id="members_rnumber">
                  </div>
                </div>


                 <div class="form-group ">
                  <label class="control-label col-lg-2">Membership Entry Date*</label>
                  <div class="col-lg-10">
                    <input type="text" class="form-control my_datepicker" value="<?php echo $members_datas['members_entry_date']?>" placeholder="Payment Reference No." name="members_entry_date" id="members_entry_date">
                  </div>
                </div>
                
                
                <div class="form-group">
                  <div class="col-lg-offset-2 col-lg-10">
                   
                    <input type="submit" class=" btn btn-info col-md-4 col-sm-6 col-xs-12" id="save" name="save" value="Save" >
                  </div>
                </div>
              </form>
            </div>
            <!-- .form --> 
            
          </div>
          <!-- panel-body --> 
        </div>
        <!-- panel --> 
      </div>
      <!-- col --> 
      
    </div>
    <!-- End row --> 
    
  </div>
  
  <!-- Page Content Ends --> 
  <!-- ================== --> 
  
</section>
<!-- Main Content Ends -->

</body>
</html>
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css">
        <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
<script>
   $(function() {
       $('.my_datepicker').datepicker({
         format: "yyyy-mm-dd" 

     });
   });
   $(function() {
       $('.end_date').datepicker({  format: "yyyy-mm-dd" });
   });
  
</script>