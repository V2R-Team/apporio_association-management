<?php
include('common.php');

$table = TABLE_MEMBERSHIP_REGISTRATION;

if(isset($_GET['status']) && isset($_GET['id'])) {
  $arr = array(
    'membership_reg_status' => $_GET['status']
  );
  $condition = " membership_registration_form_id = '".$_GET['id']."' ";

  $db->update($table,$arr,$condition);

  $db->redirect("home.php?pages=membership-registration");
}
if(isset($_GET['delid'])) {
  $condition = " membership_registration_form_id = '".$_GET['delid']."'";
  $db->delete($table,$condition);
  $db->redirect("home.php?pages=membership-registration");
}
if(isset($_POST['command']) && $_POST['command'] == "m delete") {
  $ids = implode(",",$_POST['chk']);
  $condition = " membership_registration_form_id IN ($ids) ";
  $db->delete($table,$condition);
  $db->redirect("home.php?pages=membership-registration");
}

?>
<script>
function checkall(a) {
  if(a.checked) {
    $("input[type='checkbox']").prop("checked",true);
  } else {
    $("input[type='checkbox']").prop("checked",false);
  }
}
function uncheck() {
  var tot_ch = $("input[type='checkbox']").length;
  var chek_ch = $("input[type='checkbox']:checked").length;
  if(tot_ch == chek_ch + 1 && document.getElementById('main_ch').checked == false) {
    $("#main_ch").prop("checked",true);
  } else {
    $("#main_ch").prop("checked",false);
  }
}
function godelete() {
  var tot_chk = $("input[type='checkbox']:checked").length;
  if(tot_chk > 0) {
    if(confirm("Are You Want To Delete Selected Records!")) {
      if(confirm("Are You Sure To Delete Permanently Records!")) {
        //alert('test');
          document.form1.command.value = "m delete";
          document.form1.submit();
      }
    }
  } else {
    alert("Please Select Atlease One Record To Delete!");
  }
}
function single_delete(id) {
  if(confirm("Are You Want To Delete This Records!")) {
    if(confirm("Are You Sure To Delete Permanently Records!")) {
      window.location = "home.php?pages=membership-registration&delid="+id;
    }
  }
}
</script>


<!-- Page Content Start -->
<!-- ================== -->
<form method="post" name="form1">
<input type="hidden" name="command" value="">
<div class="wraper container-fluid">
  <div class="page-title">
    <h3 class="title">Membership Registration Details</h3>
<!-- <a href="javascript:godelete()"><button type="button" name="delete" value="delete" class="btn btn-danger glyphicon glyphicon-trash"></button></a> -->
  </div>

  <div class="row">
    <div class="col-md-12">
      <div class="panel panel-default">

        <div class="panel-body">
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12 mobtbl">

            <?php $adminuser = $db->db_get_array("SELECT * FROM ".TABLE_MEMBERSHIP_REGISTRATION." WHERE membership_registration_form_id = '".$_GET['id']."' ");
              $sn = 0;
              foreach ($adminuser as $adminusers);
            ?>
              <table width='100%'>
                <tr style="border-bottom:1px solid #ccc;">
                  <th width="200" style=" padding:5px 0px;">First Name : &nbsp;</th>  <td> <?php echo $adminusers['first_name']?> </td>
                </tr>

                <tr style="border-bottom:1px solid #ccc;">
                  <th width="200" style=" padding:5px 0px;">Last Name : &nbsp;</th>  <td> <?php echo $adminusers['last_name']?> </td>
                </tr>


                <tr style="border-bottom:1px solid #ccc;">
                  <th width="200" style=" padding:5px 0px;">Address : &nbsp;</th>  <td> <?php echo $adminusers['address']?> </td>
                </tr>

                <tr style="border-bottom:1px solid #ccc;">
                  <th width="200" style=" padding:5px 0px;">Place of Birth : &nbsp;</th>  <td> <?php echo $adminusers['place_of_birth']?> </td>
                </tr>


                <tr style="border-bottom:1px solid #ccc;">
                  <th width="200" style=" padding:5px 0px;">Nationality : &nbsp;</th>  <td> <?php echo $adminusers['nationality']?> </td>
                </tr>

                <tr style="border-bottom:1px solid #ccc;">
                  <th width="200" style=" padding:5px 0px;">Phone : &nbsp;</th>  <td> <?php echo $adminusers['phone']?> </td>
                </tr>

                <tr style="border-bottom:1px solid #ccc;">
                  <th width="200" style=" padding:5px 0px;"> Pocket Portable : &nbsp;</th>  <td> <?php echo $adminusers['pocket_portable']?> </td>
                </tr>

                <tr style="border-bottom:1px solid #ccc;">
                  <th width="200" style=" padding:5px 0px;"> Email: &nbsp;</th>  <td> <?php echo $adminusers['email']?> </td>
                </tr>



                <tr style="border-bottom:1px solid #ccc;">
                  <th width="200" style=" padding:5px 0px;"> Fax : &nbsp;</th>  <td> <?php echo $adminusers['fax']?> </td>
                </tr>

                <tr style="border-bottom:1px solid #ccc;">
                  <th width="200" style=" padding:5px 0px;"> Family Sitution : &nbsp;</th>
                  <td>
                      <?php
                        if($adminusers['family_sitution'] == 1) {
                          echo 'Single';
                        } else {
                          echo 'Married';
                        }
                      ?>
                  </td>
                </tr>

                <tr style="border-bottom:1px solid #ccc;">
                  <th width="200" style=" padding:5px 0px;"> Education : &nbsp;</th>
                  <td>
                      <?php
                        if($adminusers['education'] == 1) {
                          echo 'First Primary';
                        } else if($adminusers['education'] == 2) {
                          echo 'Middle School';
                        } else if($adminusers['education'] == 3) {
                          echo 'High School';
                        } else {
                          echo 'University';
                        }
                      ?>
                  </td>
                </tr>


                <tr style="border-bottom:1px solid #ccc;">
                  <th width="200" style=" padding:5px 0px;"> Job : &nbsp;</th>  <td> <?php echo $adminusers['job']?> </td>
                </tr>

                <tr style="border-bottom:1px solid #ccc;">
                  <th width="200" style=" padding:5px 0px;"> Associate Mandate : &nbsp;</th>  <td> <?php echo $adminusers['first_activity']?> </td>
                </tr>

                <tr style="border-bottom:1px solid #ccc;">
                  <th width="200" style=" padding:5px 0px;"> Electoral Mandate  : &nbsp;</th>  <td> <?php echo $adminusers['second_activity']?> </td>
                </tr>

                <tr style="border-bottom:1px solid #ccc;">
                  <th width="200" style=" padding:5px 0px;"> Quantity  : &nbsp;</th>  <td> <?php echo $adminusers['quantity']?> </td>
                </tr>

                <tr style="border-bottom:1px solid #ccc;">
                  <th width="200" style=" padding:5px 0px;"> Amount : &nbsp;</th>
                  <td>
                      <?php
                        if($adminusers['amount'] == 1) {
                          echo 'STUDENT 10 €';
                        } else if($adminusers['amount'] == 2) {
                          echo 'Normal 20 €';
                        } else if($adminusers['amount'] == 3) {
                          echo 'COMPANY / ASSOCIATION 50 €';
                        }
                      ?>
                  </td>
                </tr>

                <tr style="border-bottom:1px solid #ccc;">
                  <th width="200" style=" padding:5px 0px;"> Payment Type : &nbsp;</th>
                  <td>
                      <?php
                        if($adminusers['payment_type'] == 1) {
                          echo 'Species';
                        } else if($adminusers['payment_type'] == 2) {
                          echo 'Check';
                        } else if($adminusers['payment_type'] == 3) {
                          echo 'Transfer';
                        }
                      ?>
                  </td>
                </tr>

                <tr style="border-bottom:1px solid #ccc;">
                  <th width="200" style=" padding:5px 0px;"> Donation  : &nbsp;</th>  <td> <?php echo $adminusers['donation']?> </td>
                </tr>

                <tr style="border-bottom:1px solid #ccc;">
                  <th width="200" style=" padding:5px 0px;"> Donation  : &nbsp;</th>  <td> <?php $adate = explode('-',$adminusers['date_of_receipt']);
                  echo $adate[0]; ?> </td>
                </tr>

                <tr style="border-bottom:1px solid #ccc;">
                  <th width="200" style=" padding:5px 0px;"> Adherent  : &nbsp;</th>  <td> <?php echo $adminusers['adherent']?> </td>
                </tr>



                <tr style="border-bottom:1px solid #ccc;">
                  <th width="200" style=" padding:5px 0px;"> Received  : &nbsp;</th>  <td> <?php echo $adminusers['received']?> </td>
                </tr>

                <tr style="border-bottom:1px solid #ccc;">
                  <th width="200" style=" padding:5px 0px;"> Received By  : &nbsp;</th>
                  <td>
                      <?php
                        if($_SERVER['REQUEST_METHOD'] == "POST") {
                          $arr = array(
                            'received_by'     => $_POST['received_by'],
                          );
                          $condition = "membership_registration_form_id='".$_GET['id']."' ";
                          $db->update(TABLE_MEMBERSHIP_REGISTRATION,$arr,$condition,true);
                          $db->redirect("home.php?pages=membership-registration-details&id=".$_GET['id']." ");
                        }
                      ?>
                      <form class="" action="" method="post">
                        <input style="width:300px; height:35px; margin:10px;" type="text" name="received_by" value="<?php echo $adminusers['received_by']?> " placeholder="Received By">
                        <br>
                        <input type="submit" style="margin:10px;" class="btn btn-info" name="submit" value="Update">
                      </form>

                  </td>
                </tr>

              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- End row -->

</div>
</form>


<!-- Page Content Ends -->
<!-- ================== -->

</section>
<!-- Main Content Ends -->

</body></html>
