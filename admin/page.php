<?php
include('common.php');
include_once '../apporioconfig/start_up.php';
// if ($admininfos['membership_add_status'] != 1) {
//   $db->redirect("home.php?pages=dashboard");
// }
if(isset($_GET['page_id'])) {
    $page_data= $db->db_get_array("SELECT * FROM ".TABLE_PAGES." WHERE page_id='".$_GET['page_id']."' ");
    foreach($page_data as $page_details);
    // echo "<pre>";
    // print_r($membership_datas);
    // echo "</pre>";

}

if($_SERVER['REQUEST_METHOD'] == "POST") {
  if(!isset($_GET['page_id'])) {
    $arr = array(
      'title'          => $_POST['title'],
      'description'   => addslashes($_POST['description']),
      'title_other'          => $_POST['title_other'],
      'description_other'   => addslashes($_POST['description_other']),

    );
    $insert_id =  $db->insert(TABLE_PAGES,$arr,true);

    $db->redirect("home.php?pages=page&page_id=".$_GET['page_id']);

  }else{

    $arr = array(
      'title'          => $_POST['title'],
      'description'   => addslashes($_POST['description']),
      'title_other'          => $_POST['title_other'],
      'description_other'   => addslashes($_POST['description_other']),

    );

    $condition = "page_id='".$_GET['page_id']."' ";
    $db->update(TABLE_PAGES,$arr,$condition,true);

    $db->redirect("home.php?pages=page&page_id=".$_GET['page_id']);
  }
}
?>

<script type="text/javascript">

function change_clty(country_id){
  $.ajax({
   url:"ajax.php?action=change_city&country_id="+country_id,
    success:function(response){
      $("#membership_city_id").html(response);
    }
  });
 }


function validatememebership() {



  if(document.getElementById('title').value == "")
  {
  alert("Please Enter Page Title");
  document.getElementById('title').focus();
  return false;
  }
  if(document.getElementById('description').value == "")
  {
  alert("Please Enter Description");
  document.getElementById('description').focus();
  return false;
  }

 return true;
}
</script>

  <!-- Page Content Start -->
  <!-- ================== -->

<div class="wraper container-fluid">
    <div class="page-title">
      <h3 class="title"><?php echo $page_details['title'];?></h3>
    </div>
    <div class="row">
      <div class="col-sm-12">
        <div class="panel panel-default">

          <div class="panel-body">
            <div class="form" >
              <form class="cmxform form-horizontal tasi-form"  method="post"  onSubmit="return validatememebership()" action="">
                <div class="form-group ">
                  <label class="control-label col-lg-2"> Title*</label>
                  <div class="col-lg-10">
                    <input type="text" class="form-control" placeholder="" name="title" id="title" value="<?php echo $page_details['title'];?>">
                  </div>
                </div>
                <div class="form-group ">
                  <label class="control-label col-lg-2"> Description*</label>
                  <div class="col-lg-10">
                    <textarea class="form-control" placeholder=" " name="description" id="description"><?php echo $page_details['description'];?></textarea>
                  </div>
                </div>
                
                <div class="form-group ">
                  <label class="control-label col-lg-2"> Title in french*</label>
                  <div class="col-lg-10">
                    <input type="text" class="form-control" placeholder="title(french)" name="title_other" id="title_other" value="<?php echo $page_details['title_other'];?>">
                  </div>
                </div>
                <div class="form-group ">
                  <label class="control-label col-lg-2"> Description in french*</label>
                  <div class="col-lg-10">
                    <textarea class="form-control" placeholder=" description(french)" name="description_other" id="description_other"><?php echo $page_details['description_other'];?></textarea>
                  </div>
                </div>
                
                
                <div class="form-group">
                  <div class="col-lg-offset-2 col-lg-10">

                    <input type="submit" class=" btn btn-info col-md-4 col-sm-6 col-xs-12" id="save" name="save" value="Save" >
                  </div>
                </div>
              </form>
            </div>
            <!-- .form -->

          </div>
          <!-- panel-body -->
        </div>
        <!-- panel -->
      </div>
      <!-- col -->

    </div>
    <!-- End row -->

  </div>

  <!-- Page Content Ends -->
  <!-- ================== -->

</section>
<!-- Main Content Ends -->

</body>
</html>
