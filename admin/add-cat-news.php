<?php 
include('common.php'); 
include_once '../apporioconfig/start_up.php';
if ($admininfos['newscat_add_status'] != 1) {
  $db->redirect("home.php?pages=dashboard");
}
if(isset($_GET['edit_id'])) {
    $news_cat_data = $db->db_get_array("SELECT * FROM ".TABLE_NEWS_CATEGORY." WHERE news_category_id='".$_GET['edit_id']."' ");
    foreach($news_cat_data as $news_cat_datas);
}

if($_SERVER['REQUEST_METHOD'] == "POST") {
  if(!isset($_GET['edit_id'])) { 
    $slug = $db->slugify($_POST['event_category_name'], TABLE_EVENT_CATEGORY, 'event_cat_slug'); 
    $arr = array(
      'news_category_name'     => $_POST['news_category_name'],
       'news_category_name_other'     => $_POST['news_category_name_other'],
      'madmin_id'        => $muser_id,
      'news_cat_slug'=>$slug,
    );
    $db->insert(TABLE_NEWS_CATEGORY,$arr,true);
    $db->redirect("home.php?pages=view-cat-news");
  } else {
    $arr = array(
      'news_category_name'     => $_POST['news_category_name'],
      'news_category_name_other'     => $_POST['news_category_name_other'],
    );
    $condition = "news_category_id='".$_GET['edit_id']."' ";
    $db->update(TABLE_NEWS_CATEGORY,$arr,$condition,true);
    $db->redirect("home.php?pages=view-cat-news");
  }
}

?>

 <script type="text/javascript">
  function validatenewscat() {
    if(document.getElementById('news_category_name').value == "")
    {
    alert("Please Enter Category");
    document.getElementById('news_category_name').focus();
    return false;
    }
    return true;
  }
</script>


  <!-- Page Content Start --> 
  <!-- ================== -->
  
<div class="wraper container-fluid">
    <div class="page-title">
      <h3 class="title">Add News Category</h3>
    </div>
    <div class="row">
      <div class="col-sm-12">
        <div class="panel panel-default">
          <div class="panel-body">
            <div class="form">
              <form class="cmxform form-horizontal tasi-form"  method="post"  onSubmit="return validatenewscat()" action="">
                <div class="form-group ">
                  <label class="control-label col-lg-2">Add Category*</label>
                  <div class="col-lg-10">
                    <input type="text" class="form-control" value="<?php echo $news_cat_datas['news_category_name']?>" placeholder="Add Category Name" name="news_category_name" id="news_category_name">
                  </div>
                </div>
                
                <div class="form-group ">
                  <label class="control-label col-lg-2">Add Category in french*</label>
                  <div class="col-lg-10">
                    <input type="text" class="form-control" value="<?php echo $news_cat_datas['news_category_name_other']?>" placeholder="" name="news_category_name_other" id="news_category_name_other">
                  </div>
                </div>
                
                <div class="form-group">
                  <div class="col-lg-offset-2 col-lg-10">
                    <input type="submit" class=" btn btn-info col-md-4 col-sm-6 col-xs-12" id="save" name="save" value="Save" >
                  </div>
                </div>
              </form>
            </div>
            <!-- .form --> 
            
          </div>
          <!-- panel-body --> 
        </div>
        <!-- panel --> 
      </div>
      <!-- col --> 
      
    </div>
    <!-- End row --> 
    
  </div>
  
  <!-- Page Content Ends --> 
  <!-- ================== --> 
  
</section>
<!-- Main Content Ends -->

</body>
</html>
