<?php
include('common.php');
if ($admininfos['eventcat_view_status'] != 1) {
  $db->redirect("home.php?pages=dashboard");
}
$table = TABLE_EVENT_CATEGORY;

if(isset($_GET['status']) && isset($_GET['id'])) {
  $arr = array(
    'event_category_status' => $_GET['status']
  );
  $condition = " event_category_id = '".$_GET['id']."' ";

  $db->update($table,$arr,$condition);

  $db->redirect("home.php?pages=view_event_category");
}
if(isset($_GET['delid'])) {
  $condition = " event_category_id = '".$_GET['delid']."'";
  $db->delete($table,$condition);
  $db->redirect("home.php?pages=view_event_category");
}
if(isset($_POST['command']) && $_POST['command'] == "m delete") {
  $ids = implode(",",$_POST['chk']);
  $condition = " event_category_id IN ($ids) ";
  $db->delete($table,$condition);
  $db->redirect("home.php?pages=view_event_category");
}

?>
<script>
function checkall(a) {
  if(a.checked) {
    $("input[type='checkbox']").prop("checked",true);
  } else {
    $("input[type='checkbox']").prop("checked",false);
  }
}
function uncheck() {
  var tot_ch = $("input[type='checkbox']").length;
  var chek_ch = $("input[type='checkbox']:checked").length;
  if(tot_ch == chek_ch + 1 && document.getElementById('main_ch').checked == false) {
    $("#main_ch").prop("checked",true);
  } else {
    $("#main_ch").prop("checked",false);
  }
}
function godelete() {
  var tot_chk = $("input[type='checkbox']:checked").length;
  if(tot_chk > 0) {
    if(confirm("Are You Want To Delete Selected Records!")) {
      if(confirm("Are You Sure To Delete Permanently Records!")) {
        //alert('test');
          document.form1.command.value = "m delete";
          document.form1.submit();
      }
    }
  } else {
    alert("Please Select Atlease One Record To Delete!");
  }
}
function single_delete(id) {
  if(confirm("Are You Want To Delete This Records!")) {
    if(confirm("Are You Sure To Delete Permanently Records!")) {
      window.location = "home.php?pages=view_event_category&delid="+id;
    }
  }
}
</script>


<!-- Page Content Start -->
<!-- ================== -->
<form method="post" name="form1">
<input type="hidden" name="command" value="">

<div class="wraper container-fluid">
  <div class="page-title">
    <h3 class="title">View Category</h3>
    <?php if ($admininfos['eventcat_add_status'] == 1) {?>
     <a href="javascript:godelete()"><button type="button" name="delete" value="delete" class="btn btn-danger glyphicon glyphicon-trash"></button></a>
     <?php }?>
  </div>

  <div class="row">
    <div class="col-md-12">
      <div class="panel panel-default">

        <div class="panel-body">
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12 mobtbl">
              <table id="datatable" class="table table-striped table-bordered table-responsive">
                <thead>
                  <tr>
                  <?php if ($admininfos['eventcat_add_status'] == 1) {?>
                    <th width="10%">
                      <label class="option block mn">
                        <input type="checkbox" id="main_ch" onClick="checkall(this)" >
                        <span class=""></span> Select
                      </label>
                    </th>
                    <?php }?>
                    <th width="5%">S.No</th>
                    <th>Category Name</th>
                    <th>Category Name in french</th>
                    <?php if ($admininfos['eventcat_add_status'] == 1) {?>
                    <th width="8%">Status</th>
                    <?php }?>
                  </tr>
                </thead>
                <tbody>
                  <?php $event_category = $db->db_get_array("SELECT * FROM ".TABLE_EVENT_CATEGORY." WHERE madmin_id='".$muser_id."'");
                      $sn = 0;
                      foreach ($event_category as $event_categorys) {
                        $sn++;
                  ?>
                    <tr>
                      <?php if ($admininfos['eventcat_add_status'] == 1) {?>
                        <td>
                          <label class="option block mn" style="width: 55px;">
                             <input type="checkbox" name="chk[]" value="<?php echo $event_categorys['event_category_id']?>" onClick="uncheck()" >
                             <span class="checkbox mn"></span>
                          </label>
                        </td>
                        <?php }?>
                        <td><?php echo $sn;?></td>
                        <?php if ($admininfos['eventcat_add_status'] == 1) {?>
                        <td><a title="Edit" href="home.php?pages=add_event_category&edit_id=<?php echo $event_categorys['event_category_id']; ?>"> <?php echo $event_categorys['event_category_name']?> </a> </td>
                        <?php }else{?>
                        <td><?php echo $event_categorys['event_category_name']?></td>
                        <?php }?>
                        
                        <?php if ($admininfos['eventcat_add_status'] == 1) {?>
                        <td><a title="Edit" href="home.php?pages=add_event_category&edit_id=<?php echo $event_categorys['event_category_id']; ?>"> <?php echo $event_categorys['event_category_name_other']?> </a> </td>
                        <?php }else{?>
                        <td><?php echo $event_categorys['event_category_name_other']?></td>
                        <?php }?>

                         <?php if ($admininfos['eventcat_add_status'] == 1) {?>
                        <?php if ($event_categorys['event_category_status'] != 0) {?>
                          <td class="text-center">
                            <a href="home.php?pages=view_event_category&status=0&id=<?php echo $event_categorys['event_category_id']?>" class="" title="Active">
                            <button type="button" class="btn btn-success br2 btn-xs fs12 activebtn" > Active
                            </button></a>
                          </td>
                        <?php } else { ?>
                          <td class="text-center">
                            <a href="home.php?pages=view_event_category&status=1&id=<?php echo $event_categorys['event_category_id']?>" class="" title="Deactive">
                                <button type="button" class="btn btn-danger  br2 btn-xs fs12 dropdown-toggle" > Deactive
                                </button></a>
                          </td>
                        <?php }}?>
                    </tr>

                  <?php }?>

                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- End row -->

</div>
</form>


<!-- Page Content Ends -->
<!-- ================== -->

</section>
<!-- Main Content Ends -->

</body></html>
