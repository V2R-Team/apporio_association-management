<?php
include('common.php');
include_once '../apporioconfig/start_up.php';

$table = "membership_registration_form";


if($_SERVER['REQUEST_METHOD'] == "POST") {
    date_default_timezone_set('Asia/Kolkata');
    $timestamp = time();
    $create_date = date("d F Y-g:i A", $timestamp);
    $arr = array(
      'first_name'           => $_POST['first_name'],
      'last_name'            => $_POST['last_name'],
      'address'              => $_POST['address'],
      'place_of_birth'       => $_POST['place_of_birth'],
      'date_of_birth'        => $_POST['date_of_birth'],
      'nationality'          => $_POST['nationality'],
      'phone'                => $_POST['phone'],
      'pocket_portable'      => $_POST['pocket_portable'],
      'email'                => $_POST['email'],
      'fax'               	 => $_POST['fax'],
      'family_sitution'      => $_POST['family_sitution'],
      'education'            => $_POST['education'],
      'job'            		   => $_POST['job'],
      'first_activity'       => $_POST['first_activity'],
      'second_activity'      => $_POST['second_activity'],
      'quantity'             => $_POST['quantity'],
      'amount'               => $_POST['amount'],
      'payment_type'         => $_POST['payment_type'],
      'donation'             => $_POST['donation'],
      'date_of_receipt'      => $create_date,
      'adherent'             => $_POST['adherent'],
      'received'             => $_POST['received'],
    );
    $insert_id =  $db->insert($table,$arr,true);

    $db->redirect("thank-you");

  }

?>


<article id="" class="">
	<div class="entry-content">
		<div class="top_background" style="border-style: none;">
			<div class="container">
				<div class=""  style="font-family:'Montserrat';font-weight:700;">
					<p class="sd-text-bg-white">Renew Membership</p>
				</div>
			</div>
		</div>
	</div>
</article>
<div class="container">
	<div style="margin-top: 50px;"></div>
    <div class="vc_col-md-3"></div>
	<div class="wpb_column vc_column_container vc_col-sm-6" style="box-shadow: 0px 0px 5px #999; margin:0px 0px 50px 0px;">
		<div class="vc_column-inner ">
			<div class="wpb_wrapper">

			<script type="text/javascript">
function formvalidation() {
  if( document.getElementById('first_name').value == "" ) {
   alert('Please Enter First name');
   document.getElementById('first_name').focus();
   return false;
  }
  if( document.getElementById('last_name').value == "" ) {
   alert('Please Enter Last Name');
   document.getElementById('last_name').focus();
   return false;
  }
  if( document.getElementById('address').value == "" ) {
   alert('Please Enter Address');
   document.getElementById('address').focus();
   return false;
  }
 if( document.getElementById('place_of_birth').value == "" ) {
   alert('Please Select Place of Birth');
   document.getElementById('place_of_birth').focus();
   return false;
  }
 if( document.getElementById('date_of_birth').value == "" ) {
   alert('Please Select Date of Birth');
   document.getElementById('date_of_birth').focus();
   return false;
  }
 if( document.getElementById('nationality').value == "" ) {
   alert('Please Enter Nationality');
   document.getElementById('nationality').focus();
   return false;
  }
 if( document.getElementById('phone').value == "" ) {
   alert('Please Enter Phone Number');
   document.getElementById('phone').focus();
   return false;
  }
 if( document.getElementById('pocket_portable').value == "" ) {
   alert('Please Enter Pocket Portable');
   document.getElementById('pocket_portable').focus();
   return false;
  }
 if( document.getElementById('email').value == "" ) {
   alert('Please Enter Email');
   document.getElementById('email').focus();
   return false;
  }
 if( document.getElementById('fax').value == "" ) {
   alert('Please Enter Fax Number');
   document.getElementById('fax').focus();
   return false;
  }
 if( document.getElementById('family_sitution1').checked == false &&  document.getElementById('family_sitution2').checked == false) {
   alert('Please Select Family Situation');
   document.getElementById('family_sitution1').focus();
   return false;
  }
if( document.getElementById('education1').checked == false && document.getElementById('education2').checked == false && document.getElementById('education3').checked == false && document.getElementById('education4').checked == false) {
  alert('Please Select Education');
  document.getElementById('education1').focus();
  return false;
 }

 if( document.getElementById('job').value == "" ) {
   alert('Please Enter Job');
   document.getElementById('job').focus();
   return false;
  }
 if( document.getElementById('first_activity').value == "" ) {
   alert('Please Enter Associate Mandate');
   document.getElementById('first_activity').focus();
   return false;
  }
if( document.getElementById('second_activity').value == "" ) {
   alert('Please Enter Electoral Mandate');
   document.getElementById('second_activity').focus();
   return false;
  }

 if( document.getElementById('quantity').value == "" ) {
   alert('Please Enter Quantity');
   document.getElementById('quantity').focus();
   return false;
  }


  if( document.getElementById('amount1').checked == false &&  document.getElementById('amount2').checked == false &&  document.getElementById('amount3').checked == false) {
    alert('Please Select Amount');
    document.getElementById('amount1').focus();
    return false;
   }



  if( document.getElementById('payment_type1').checked == false &&  document.getElementById('payment_type2').checked == false &&  document.getElementById('payment_type3').checked == false) {
    alert('Please Select Payment Type');
    document.getElementById('payment_type1').focus();
    return false;
   }

 if( document.getElementById('adherent').value == "" ) {
   alert('Please Enter Adherent');
   document.getElementById('adherent').focus();
   return false;
  }
 if( document.getElementById('received').value == "" ) {
   alert('Please Enter Received');
   document.getElementById('received').focus();
   return false;
  }

  return true;
}
</script>


			<form action="" method="post" class="wpcf7-form" novalidate="novalidate" onSubmit="return formvalidation()">
				<div id="ultimate-heading-90658946288c8bad" class="uvc-heading ult-adjust-bottom-margin ultimate-heading-90658946288c8bad uvc-1195 " data-hspacer="no_spacer"  data-halign="left" style="text-align:left">
					<div class="uvc-heading-spacer no_spacer" style="top"></div>
					<div class="uvc-main-heading ult-responsive"  data-ultimate-target='.uvc-heading.ultimate-heading-90658946288c8bad h3'  data-responsive-json-new='{"font-size":"desktop:24px;","line-height":"desktop:30px;"}' >
						<h3 style="font-family:'Montserrat';padding:20px 0px; text-align:center;">Renew Membership</h3>
					</div>
				</div>

				<div role="form" class="wpcf7" id="wpcf7-f357-p355-o1" lang="en-US" dir="ltr">
					<div class="screen-reader-response"></div>

				       <div class="col-md-12 mb10">
						<label>choose plan </label>
						<select class="wpcf7-form-control new_text wpcf7-text wpcf7-validates-as-required">
						   <option>---Please select your Plan---</option>
						   
						   	<option>Plan 1</option>
						   	
						</select>
					</div>
				    
				       <div class="col-md-12 mb10">
						<label>E-Mail</label>
						<input type="text" name="email" id="email" value="" size="40" class="wpcf7-form-control new_text wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" placeholder="E-Mail" />
					</div>
					
					
					<div class="col-md-12 mb10">
						<label>Password</label>
						<input type="password" name="password" id="password" value="" size="40" class="wpcf7-form-control new_text wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" placeholder="Password" />
					</div>

					<div class="col-md-6 mb10" style="margin:20px 0px;">
						<p style="padding:22px 0px"><a href="#" style="text-decoration:underline">Forgot Password?</a></p>
					</div>


					<div class="col-md-6 mb10"  style="margin:20px 0px;">
						<p style="padding:15px 0px;">
						<input type="submit" value="Submit" class="wpcf7-form-control wpcf7-submit" style="width:100%;" />
						</p>
					</div>

				</div>
			</form>
			</div>
            
            <div class="vc_col-md-3"></div>
		</div>
	</div>
</div>



<script type="text/javascript" src="admin/js/wickedpicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="admin/css/wickedpicker.min.css">

<script>
   $(function() {
       $('.start_time').datepicker({

         format: "yyyy-mm-dd"

     });
   });
   $(function() {
       $('.end_date').datepicker({

        format: "yyyy-mm-dd" });
   });

    $('.start').wickedpicker();

     $('.end').wickedpicker();

$('.time').bootstrapMaterialDatePicker({ date: false });

</script>
