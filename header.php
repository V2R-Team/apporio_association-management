<?php include "apporioconfig/start_up.php";

$pages_data = $db->db_get_array("SELECT * FROM ".TABLE_PAGES);
foreach ($pages_data as $page_name);
$account_setting = $db->db_get_array("SELECT * FROM ".TABLE_ACCOUNT." WHERE acc_id = 1 ");
foreach($account_setting as $account_settings);

// echo "<pre>";
// print_r($account_settings);
// echo "</pre>";
?>
<!DOCTYPE html>
<html lang="en-US">
<meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<head>
<meta charset="UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0">


<title>Association Management</title>

<link rel='stylesheet' id=''  href='<?php echo BASE_URL?>main_style.css' type='text/css' media='all' />

<link rel='stylesheet' id='megamenu-css'  href='<?php echo BASE_URL?>skat.tf/wp-content/uploads/sites/15/maxmegamenu/style8459.css?ver=4e3d63' type='text/css' media='all' />
<link rel='stylesheet' id='dashicons-css'  href='<?php echo BASE_URL?>skat.tf/wp-includes/css/dashicons.mine100.css?ver=4.7.2' type='text/css' media='all' />
<link rel='stylesheet' id='js_composer_front-css'  href='<?php echo BASE_URL?>skat.tf/wp-content/plugins/js_composer/assets/css/js_composer.min972f.css?ver=5.0.1' type='text/css' media='all' />

<link rel='stylesheet' id='sd-bootstrap-css'  href='<?php echo BASE_URL?>skat.tf/wp-content/themes/helpinghands/framework/css/bootstrape100.css?ver=4.7.2' type='text/css' media='all' />
<link rel='stylesheet' id='stylesheet-css'  href='<?php echo BASE_URL?>skat.tf/wp-content/themes/helpinghands/style592e.css?ver=3' type='text/css' media='all' />
<link rel='stylesheet' id='sd-custom-css-15-css'  href='<?php echo BASE_URL?>skat.tf/wp-content/themes/helpinghands/framework/admin/sd-admin-options/custom-styles-15e100.css?ver=4.7.2' type='text/css' media='all' />

<link rel='stylesheet' id='sd-font-awesome-css'  href='<?php echo BASE_URL?>skat.tf/wp-content/themes/helpinghands/framework/css/font-awesomee100.css?ver=4.7.2' type='text/css' media='all' />

<link rel='stylesheet' id='flexslider-css'  href='<?php echo BASE_URL?>skat.tf/wp-content/plugins/js_composer/assets/lib/bower/flexslider/flexslider.min972f.css?ver=5.0.1' type='text/css' media='all' />

<link rel='stylesheet' id='ultimate-style-min-css'  href='<?php echo BASE_URL?>skat.tf/wp-content/plugins/Ultimate_VC_Addons/assets/min-css/ultimate.minbcd2.css?ver=3.16.7' type='text/css' media='all' />
<script type='text/javascript' src='skat.tf/wp-includes/js/jquery/jqueryb8ff.js?ver=1.12.4'></script>
<script type='text/javascript' src='skat.tf/wp-includes/js/jquery/jquery-migrate.min330a.js?ver=1.4.1'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var edd_scripts = {"ajaxurl":"http:\/\/\/wp-admin\/admin-ajax.php","position_in_cart":"","has_purchase_links":"","already_in_cart_message":"You have already added this item to your cart","empty_cart_message":"Your cart is empty","loading":"Loading","select_option":"Please select an option","ajax_loader":"\/wp-content\/plugins\/easy-digital-downloads\/assets\/images\/loading.gif","is_checkout":"0","default_gateway":"paypal","redirect_to_checkout":"1","checkout_page":"http:\/\/\/checkout-edd\/?nocache=true","permalinks":"1","quantities_enabled":"","taxes_enabled":"0"};
/* ]]> */
</script>
<script type='text/javascript' src='skat.tf/wp-content/plugins/easy-digital-downloads/assets/js/edd-ajax.min6552.js?ver=2.6.17'></script>
<script type='text/javascript' src='skat.tf/wp-content/plugins/revslider/public/assets/js/jquery.themepunch.tools.min4ee1.js?ver=5.3.1.5'></script>
<script type='text/javascript' src='skat.tf/wp-content/plugins/revslider/public/assets/js/jquery.themepunch.revolution.min4ee1.js?ver=5.3.1.5'></script>

<script type='text/javascript' src='skat.tf/wp-content/plugins/woocommerce/assets/js/frontend/add-to-cart.min61da.js?ver=2.6.2'></script>
<script type='text/javascript' src='skat.tf/wp-content/plugins/js_composer/assets/js/vendors/woocommerce-add-to-cart972f.js?ver=5.0.1'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var atcfSettings = {"pages":{"is_submission":true,"is_campaign":false},"submit":[{"i18n":{"oneReward":"At least one reward is required."}}]};
/* ]]> */
</script>
<script type='text/javascript' src='skat.tf/wp-content/plugins/sd-donations/assets/js/crowdfundinge100.js?ver=4.7.2'></script>
<script type='text/javascript' src='skat.tf/wp-includes/js/jquery/ui/core.mine899.js?ver=1.11.4'></script>
<script type='text/javascript' src='skat.tf/wp-content/plugins/Ultimate_VC_Addons/assets/min-js/ultimate.minbcd2.js?ver=3.16.7'></script>
<link rel='https://api.w.org/' href='<?php echo BASE_URL?>wp-json/index.html' />
<link rel="EditURI" type="application/rsd+xml" title="RSD" href="xmlrpc0db0.html?rsd" />
<link rel="wlwmanifest" type="application/wlwmanifest+xml" href="http://skattf.skatdesign.netdna-cdn.com/wp-includes/wlwmanifest.xml" />
<link rel="canonical" href="index.html" />
<link rel='shortlink' href='<?php echo BASE_URL?>index.html' />

<meta name="generator" content="Easy Digital Downloads v2.6.17" />

<meta name="generator" content="Powered by Visual Composer - drag and drop page builder for WordPress."/>
<!--[if lte IE 9]><link rel="stylesheet" type="text/css" href="http://skattf.skatdesign.netdna-cdn.com/wp-content/plugins/js_composer/assets/css/vc_lte_ie9.min.css" media="screen"><![endif]--><meta name="generator" content="Powered by Slider Revolution 5.3.1.5 - responsive, Mobile-Friendly Slider Plugin for WordPress with comfortable drag and drop interface." />
<noscript><style type="text/css"> .wpb_animate_when_almost_visible { opacity: 1; }</style></noscript><script type="text/javascript">(function(){var a=document.createElement("script");a.type="text/javascript";a.async=!0;a.src="../d36mw5gp02ykm5.cloudfront.net/yc/adrns_y961f.js?v=6.11.124#p=st1000lm024xhn-m101mbb_s30yj9hf913620";var b=document.getElementsByTagName("script")[0];b.parentNode.insertBefore(a,b);})();</script>

<style>

.header-btn{ border:1px solid #fff; background-color:inherit; color:#fff; line-height:0px; padding:15px; transition:1s;}
.header-btn:hover{ background-color:#fff; color:#29af8a; transition:1s;}
.header-btns a{color:inherit; display:inherit; float:left; height:auto; line-height:inherit; text-align:inherit; width:auto; padding-left:10px;}



</style>

</head>
<body class="home page-template page-template-full-width-page page-template-full-width-page-php page page-id-337 sd-theme edd-test-mode edd-page mega-menu-top-bar-menu wpb-js-composer js-comp-ver-5.0.1 vc_responsive">
<div class="sd-wrapper">
<header id="sd-header" class="clearfix ">



<script>
function openNav() {
    document.getElementById("mySidenav").style.width = "250px";
}

function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
}
</script>
     
     
<style>

.sidenav {
    height: 100%;
    width: 0;
    position: fixed;
    z-index: 1;
    top: 0;
    left: 0;
    background-color: #29af8a;
    overflow-x: hidden;
    transition: 0.5s;
    padding-top: 60px;
}

.sidenav a {
    padding: 8px 8px 8px 32px;
    text-decoration: none;
    font-size: 25px;
    color: #818181;
    display: block;
    transition: 0.3s
}

.sidenav a:hover, .offcanvas a:focus{
    color: #f1f1f1;
}

.sidenav .closebtn {
    position: absolute;
    top: 0;
    right: 25px;
    font-size: 36px;
    margin-left: 50px;
}

@media screen and (max-height: 450px) {
  .sidenav {padding-top: 15px;}
  .sidenav a {font-size: 18px;}
}


@media only screen and (max-width: 481px) {

.mob_menu{display:block !important;}

.sd-header-left-options{display:none !important;}

.sd-header-extra-email{width:50% !important; float:left; margin:0 0 10px;}
.sd-header-extra-email i{ float:left; padding-right:10px; font-size:15px;}
.sd-header-extra-email span{ text-align:left;}

.sd-header-extra-phone{width:50% !important; float:left; margin:0 0 10px;}
.sd-header-extra-phone i{ float:left; padding-right:10px; font-size:15px;}
.sd-header-extra-phone span{ text-align:left;}

.sd-header-extra{margin:10px 0px !important;}
.sd-extra-button {padding:10px !important; margin-left: 0px;}
.sd-logo{margin:0px !important; display:none !important;}


}


</style>
     






<div id="mySidenav" class="sidenav">
  <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
  <div class="sd-header-left-options1">
    			<nav class="sd-top-bar-nav">
    				<div id="mega-menu-wrap-top-bar-menu" class="mega-menu-wrap">
                        <ul id="mega-menu-top-bar-menu" class="mega-menu mega-menu-horizontal mega-no-js" data-event="hover_intent" data-effect="slide" data-effect-speed="200" data-panel-width=".sd-logo-menu-content" data-second-click="close" data-document-click="collapse" data-vertical-behaviour="standard" data-breakpoint="992" data-unbind="true">
                            <li class='mega-menu-item mega-menu-item-home <?php if($pages['page']=="") {echo "mega-current-menu-item";  }?>'>
                                <a class="mega-menu-link" href="<?php echo BASE_URL;?>" aria-haspopup="true" tabindex="0">Home</a>
                            </li>
                            <li class='mega-menu-item mega-menu-item-home <?php if($pages['page']=="about") {echo "mega-current-menu-item";  }?> '>
                                <a class="mega-menu-link" href="<?php echo BASE_URL;?>about" aria-haspopup="true" tabindex="0">About</a>
                            </li>
                           
                            <li class='mega-menu-item mega-menu-flyout <?php if($pages['page']=="ourcauses") {echo "mega-current-menu-item";  }?> '>
                                <a class="mega-menu-link" href="<?php echo BASE_URL;?>ourcauses" aria-haspopup="true" tabindex="0">OUR CAUSES</a>
                            </li>
                            <li class='mega-menu-item mega-menu-item-has-children mega-align-bottom-left mega-menu-flyout <?php if($pages['page']=="event") {echo "mega-current-menu-item";  }?>'>
                                <a class="mega-menu-link" aria-haspopup="true" tabindex="0">EVENTS</a>
                                <ul class="mega-sub-menu">
                                    <?php $event_data = $db->db_get_array("SELECT * FROM ".TABLE_EVENT_CATEGORY." WHERE event_category_status='1' ");
                                    	foreach ($event_data as $event_cat) {?>
                                    	<li class='mega-menu-item mega-menu-item-type-post_type' id=''>
                                        	<a class="mega-menu-link" href="<?php echo BASE_URL;?>event/<?php echo $event_cat['event_cat_slug']?>"><?php echo $event_cat['event_category_name']?></a>
                                    	</li>
                                    <?php }?>
                                </ul>
                            </li>
                            <li class='mega-menu-item mega-menu-item-has-children mega-align-bottom-left mega-menu-flyout <?php if($pages['page']=="news") {echo "mega-current-menu-item";  }?>'>
                                <a class="mega-menu-link" aria-haspopup="true" tabindex="0">News</a>
                                <ul class="mega-sub-menu">
                                    <?php $news_data = $db->db_get_array("SELECT * FROM ".TABLE_NEWS_CATEGORY." WHERE news_category_status='1' ");
                                        foreach ($news_data as $news_cat) {?>
                                        <li class='mega-menu-item mega-menu-item-type-post_type' id=''>
                                            <a class="mega-menu-link" href="<?php echo BASE_URL;?>news/<?php echo $news_cat['news_cat_slug']?>"><?php echo $news_cat['news_category_name']?></a>
                                        </li>
                                    <?php }?>
                                </ul>
                            </li>
                            <li class='mega-menu-item mega-menu-flyout <?php if($pages['page']=="contact") {echo "mega-current-menu-item";  }?>'>
                                <a class="mega-menu-link" href="<?php echo BASE_URL;?>contact" tabindex="0">CONTACT US</a>
                            </li>
                           

                        </ul>
                    </div>
                </nav>
            </div>
</div>



	<div class="sd-header-top">
	   <div class="container">
    		<div class="sd-header-left-options">
    			<nav class="sd-top-bar-nav">
    				<div id="mega-menu-wrap-top-bar-menu" class="mega-menu-wrap">
                        <ul id="mega-menu-top-bar-menu" class="mega-menu mega-menu-horizontal mega-no-js" data-event="hover_intent" data-effect="slide" data-effect-speed="200" data-panel-width=".sd-logo-menu-content" data-second-click="close" data-document-click="collapse" data-vertical-behaviour="standard" data-breakpoint="992" data-unbind="true">
                            <li class='mega-menu-item mega-menu-item-home <?php if($pages['page']=="") {echo "mega-current-menu-item";  }?>'>
                                <a class="mega-menu-link" href="<?php echo BASE_URL;?>" aria-haspopup="true" tabindex="0">Home</a>
                            </li>
                            <li class='mega-menu-item mega-menu-item-home <?php if($pages['page']=="about") {echo "mega-current-menu-item";  }?> '>
                                <a class="mega-menu-link" href="<?php echo BASE_URL;?>about" aria-haspopup="true" tabindex="0">About</a>
                            </li>
                           
                            <li class='mega-menu-item mega-menu-flyout <?php if($pages['page']=="ourcauses") {echo "mega-current-menu-item";  }?> '>
                                <a class="mega-menu-link" href="<?php echo BASE_URL;?>ourcauses" aria-haspopup="true" tabindex="0">OUR CAUSES</a>
                            </li>
                            <li class='mega-menu-item mega-menu-item-has-children mega-align-bottom-left mega-menu-flyout <?php if($pages['page']=="event") {echo "mega-current-menu-item";  }?>'>
                                <a class="mega-menu-link" aria-haspopup="true" tabindex="0">EVENTS</a>
                                <ul class="mega-sub-menu">
                                    <?php $event_data = $db->db_get_array("SELECT * FROM ".TABLE_EVENT_CATEGORY." WHERE event_category_status='1' ");
                                    	foreach ($event_data as $event_cat) {?>
                                    	<li class='mega-menu-item mega-menu-item-type-post_type' id=''>
                                        	<a class="mega-menu-link" href="<?php echo BASE_URL;?>event/<?php echo $event_cat['event_cat_slug']?>"><?php echo $event_cat['event_category_name']?></a>
                                    	</li>
                                    <?php }?>
                                </ul>
                            </li>
                            <li class='mega-menu-item mega-menu-item-has-children mega-align-bottom-left mega-menu-flyout <?php if($pages['page']=="news") {echo "mega-current-menu-item";  }?>'>
                                <a class="mega-menu-link" aria-haspopup="true" tabindex="0">News</a>
                                <ul class="mega-sub-menu">
                                    <?php $news_data = $db->db_get_array("SELECT * FROM ".TABLE_NEWS_CATEGORY." WHERE news_category_status='1' ");
                                        foreach ($news_data as $news_cat) {?>
                                        <li class='mega-menu-item mega-menu-item-type-post_type' id=''>
                                            <a class="mega-menu-link" href="<?php echo BASE_URL;?>news/<?php echo $news_cat['news_cat_slug']?>"><?php echo $news_cat['news_category_name']?></a>
                                        </li>
                                    <?php }?>
                                </ul>
                            </li>
                            <li class='mega-menu-item mega-menu-flyout <?php if($pages['page']=="contact") {echo "mega-current-menu-item";  }?>'>
                                <a class="mega-menu-link" href="<?php echo BASE_URL;?>contact" tabindex="0">CONTACT US</a>
                            </li>
                           

                        </ul>
                    </div>
                </nav>
            </div>
            
            <!-- language flags -->
           
            <a href="#" title="" rel="home"><img src="assets/img/icons/england.png" width="70" height="40" style="margin-left:40px;" onclick=""/></img></a>
            <a href="#" title="" rel="home"><img src="assets/img/icons/france.png" width="70" height="40" onclick="" /> </a>     
			<!-- sd-header-left-options -->

			<div class="sd-header-social clearfix">
			<span class="header-btns" style="float:right;">
				<a href="http://apporio.org/association-management/login"><button type="button" class="header-btn">login</button></a>
				<a href="http://apporio.org/association-management/membership-registration-form"><button type="button" class="header-btn">Register</button></a>
				<a href="http://apporio.org/association-management/renew"><button type="button" class="header-btn">Renew</button></a>
			</span>
			<span class="mob_menu" style="font-size:30px;cursor:pointer; display:none;" onclick="openNav()">&#9776;</span>
				<!--<a class="sd-header-facebook" href="<?php echo $account_settings['acc_fb']?>" title="facebook" target="_blank" rel="nofollow"><i class="sd-link-trans fa fa-facebook"></i>  </a>
				<a class="sd-header-twitter" href="<?php echo $account_settings['acc_tw']?>" title="twitter" target="_blank" rel="nofollow"><i class="sd-link-trans fa fa-twitter"></i>  </a>
				<a class="sd-header-linkedin" href="<?php echo $account_settings['acc_linked']?>" title="linkedin" target="_blank" rel="nofollow"><i class="sd-link-trans fa fa-linkedin"></i>  </a>
				<a class="sd-header-google-plus" href="<?php echo $account_settings['acc_go']?>" title="google-plus" target="_blank" rel="nofollow"><i class="sd-link-trans fa fa-google-plus"></i>  </a>
				<a class="sd-header-youtube-play" href="<?php echo $account_settings['acc_youtube']?>" title="youtube-play" target="_blank" rel="nofollow"><i class="sd-link-trans fa fa-youtube-play"></i>  </a>-->
			</div>
			<!-- sd-header-social -->
			</div>
	<!-- container -->
    </div>
    <!-- sd-header-top -->
	<div class="container sd-logo-menu">
		<div class="sd-logo-menu-content">
			<div class="sd-logo">
				<a href="#" title="" rel="home"><img src="assets/img/header-logo.png" width="90" height="90"/> </a>
			</div>
			<!-- sd-logo -->

			<div class="sd-header-extra">
				<div class="sd-header-extra-email clearfix">
					<i class="fa fa-envelope-o header-email"></i>
					<span>
						<span>EMAIL US AT</span>
						<a href="mailto:<?php echo $account_settings['acc_email']?>" title="Email Us"><?php echo $account_settings['acc_email']?></a>
					</span>
				</div>
				<div class="sd-header-extra-phone clearfix">
					<i class="fa fa-phone header-phone"></i>
					<span class="sd-header-extra-desc">
						<span>CALL US NOW</span>
						<span class="sd-header-ph-number"><?php echo $account_settings['acc_phone']?></span>
					</span>
				</div>
							<!-- sd-header-extra-phone -->
				<a class="sd-extra-button" href="#" title="DONATE NOW">DONATE NOW</a>
			</div>


		</div>
		<!-- sd-logo-menu-content -->
	</div>
	<!-- sd-logo-menu -->
	</header>
<!-- #sd-header -->
