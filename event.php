<?php $event = $db->db_get_array("SELECT * FROM ".TABLE_EVENT." WHERE event_slug = '".$pages[1]."'");
foreach ($event as $events_s);

?>
<article id="" class="">
	<div class="entry-content">
		<div class="top_background" style="border-style: none;">
			<div class="container">
				<div class=""  style="font-family:'Montserrat';font-weight:700;">
					<p class="sd-text-bg-white">	
						<?php if ($pages[1]=="") {
							echo "EVENTS";
							}else{
							echo $events_s['event_name'];
						} ?>		
					</p>
				</div>
			</div>
		</div><!-- Row Backgrounds -->
	</div>
</article>				

<?php if($pages[1] == "") {?>
	<div class="sd-events-listing">
		<div class="container">
			<div class="row">
				<?php
				$eventcat = $db->db_get_array("SELECT event_category_id FROM ".TABLE_EVENT_CATEGORY." WHERE event_cat_slug = '".$pages[0]."'");
				$eventsa = $db->db_get_array("SELECT * FROM ".TABLE_EVENT." WHERE event_category_id=".$eventcat[0]['event_category_id']." AND event_status = 1");


	            foreach ($eventsa as $events) {
	            	$newDate = date("F d, Y", strtotime($events['event_start_date']));
	            ?>
	            	<div class="col-md-4 col-sm-4 ev-listing-item">
						<figure>
							<img src="<?php echo BASE_URL;?><?php echo $events['event_banner']?>" class="" alt="" data-id="130" />
						</figure>
						<div class="ev-listing-content">
							<h3 class="sd-entry-title">
								<a href="<?php echo BASE_URL;?>event/<?php echo $pages[0]?>/<?php echo $events['event_slug']?>" title="<?php echo $events['event_name']?>"><?php echo $events['event_name']?></a>
							</h3>
							<span class="ev-city"><?php echo $events['event_location']?></span>
							<p><?php echo substr($events['event_description'], 0,70) ?></p>
							<a class="sd-more sd-link-trans" href="<?php echo BASE_URL;?>event/<?php echo $pages[0]?>/<?php echo $events['event_slug']?>" title="<?php echo $events['event_name']?>">READ MORE</a>
						</div>
						<!-- ev-listing-content -->
						<span class="ev-listing-date"><?php echo $newDate?> @ <?php echo $events['event_start_time']?></span>
					</div>
	            <?php }?>
				<div class="clearfix"></div>
			</div>
			<!-- row -->
		</div>
		<!-- container -->
	</div>
<?php } else {?>
	<div class="container sd-blog-page sd-single-event">
		<div class="row">
			<!--left col-->
			<div class="col-md-8 ">
				<div class="sd-left-col">
					<div class="row">
						<div class="col-md-12 col-sm-12 sd-padding-right-none">
							<div class="sd-entry-thumb">
								<figure>
									<img src="<?php echo BASE_URL;?><?php echo $events_s['event_banner']?>" width="100%" style="height: auto;"/>
								</figure>
							</div>
						</div>
									<!-- col-md-9 -->
						<!-- <div class="col-md-3 col-sm-3 sd-padding-left-none">
							<div class="sd-event-data">
								<span class="sd-dov">
									<i class="fa fa-calendar"></i>sd-right-col
							    <?php echo $events_s['event_address_one']?>
								</span>
							</div>
						</div> -->
								<!-- col-md-3 -->
					</div>
								<!-- row -->

					<header>
						<h2 class="sd-entry-title"><?php echo $events_s['event_name']?></h2>
						<aside class="sd-entry-meta clearfix">
								<ul>
									<li class="sd-meta-author">
										<i class="fa fa-calendar"></i><?php echo date("F d, Y", strtotime($events_s['event_start_date']));?>		
									</li>
									<li class="sd-meta-author">
										<i class="fa fa-location-arrow"></i><?php echo $events_s['event_vanue']?>			
									</li>
									<!-- <li class="sd-meta-category">
										<i class="fa fa-folder"></i>
										<a href="#" rel="category tag">VOLUNTEERS</a>			
									</li>
					
									<li class="meta-tag"> <i class="fa fa-tag"></i> 
										<a href="../tag/africa/index.html" rel="tag">AFRICA</a> , 
										<a href="../tag/crisis/index.html" rel="tag">CRISIS</a>
									</li>				
									<li class="sd-meta-comments">
										<i class="fa fa-comment"></i>
										<a href="index.html#respond" class="comments-link" >0 comments</a>			
									</li> -->
								</ul>
							</aside>
					</header>

					<div class="sd-entry-content">
						<div class="vc_row wpb_row vc_row-fluid">
							<div class="wpb_column vc_column_container vc_col-sm-12">
								<div class="vc_column-inner vc_custom_1438122805308">
									<div class="wpb_wrapper">
										<div class="wpb_text_column wpb_content_element ">
											<div class="wpb_wrapper">
												<p><?php echo nl2br($events_s['event_description'])?></p>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>

					<div id="comments" class="sd-comments-wrapper">
						<p class="hidden">Comments are closed.</p>
					</div>							
				</div>
				<!-- sd-left-col -->
			</div>
			<!-- col-md-8 -->
			<div class="col-md-4">
					<!--right-col-->

				<div class="sd-right-col">
					<div id="edd_categories_tags_widget-2" class="widget widget_edd_categories_tags_widget">
						<h3 class="widget-title">CATEGORIES</h3>
						<ul class="edd-taxonomy-widget">
						<?php $event_names = $db->db_get_array("SELECT * FROM ".TABLE_EVENT_CATEGORY." WHERE event_category_status='1' ");
					    	foreach ($event_names as $event_names) {?>
					    	<li class="cat-item cat-item-14"><a href="<?php echo BASE_URL;?>event/<?php echo $event_cat['event_cat_slug']?>" ><?php echo $event_cat['event_category_name']?></a></li>

					    <?php }?>

						</ul>
					</div>
					<div id="sd_upcoming_events_widget-3" class="widget sd_upcoming_events_widget">
						<h3 class="widget-title">Related Events</h3>		
						<div class="ev-widget">
							<?php
							$eventcat1 = $db->db_get_array("SELECT event_category_id FROM ".TABLE_EVENT_CATEGORY." WHERE event_cat_slug = '".$pages[0]."'");

							$eventsa1 = $db->db_get_array("SELECT * FROM ".TABLE_EVENT." WHERE event_category_id=".$eventcat1[0]['event_category_id']." AND event_slug != '".$pages[1]."' AND event_status = 1 limit 0,2");


							foreach ($eventsa1 as $events2) {?>
								<div class="ev-listing-item">
									<figure>
										<img src="<?php echo BASE_URL;?><?php echo $events2['event_banner'];?>" class="attachment-sd-campaign-grid size-sd-campaign-grid wp-post-image" alt="" data-id="130" />							
									</figure>

									<div class="ev-listing-content">
										<h3 class="sd-entry-title">
											<a href="<?php echo BASE_URL;?><?php echo $events2['event_name'];?>" title="<?php echo BASE_URL;?><?php echo $events2['event_name'];?>" rel="bookmark">
												<?php echo $events2['event_name'];?>
											</a>
										</h3>
										<span class="ev-city"><?php echo $events2['event_location'];?></span>
										<p><?php echo substr($events2['event_description'], 0, 100);?></p>
										<a class="sd-more sd-link-trans" href="<?php echo BASE_URL;?>event/<?php echo $pages[0];?>/<?php echo $events2['event_slug'];?>" title="<?php echo $events2['event_name'];?>">READ MORE</a>
									</div>

									<span class="ev-listing-date"><?php echo  date("M  j, Y", strtotime($events2['event_start_date']));?>  @ <?php echo $events2['event_start_time'];?></span>
								</div>
							<?php } ?>
						</div>

					</div>	
				</div>
	<!-- sd-right-col --> 			
			</div>
		</div>
		<!-- row -->
	</div>
<?php }?>
