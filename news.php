<?php $news_single = $db->db_get_array("SELECT * FROM ".TABLE_NEWS." WHERE news_slug = '".$pages[1]."'");
foreach ($news_single as $news_singles);
// echo "<pre>";
// print_r($news_singles);
// echo "</pre>";
?>
<article id="" class="">
	<div class="entry-content">
		<div class="top_background" style="border-style: none;">
			<div class="container">
				<div class=""  style="font-family:'Montserrat';font-weight:700;">
					<p class="sd-text-bg-white">	
						<?php if ($pages[1]=="") {
							echo "NEWS";
							}else{
							echo $news_singles['news_name'];
						} ?>		
					</p>
				</div>
			</div>
		</div>
	</div>
</article>	
<?php if($pages[1] == "") {?>
	<div class="container">
		<div class="row">
			<br><br>
			<div class="sd-latest-blog-short">
				<div class="sd-latest-blog-carousel">
					<?php $news = $db->db_get_array("SELECT * FROM ".TABLE_NEWS." WHERE news_status = 1");
						foreach ($news as $newss) {?>
						<div class="col-md-3 col-sm-3 col-xs-12">
							<div class="sd-entry-wrapper clearfix">
								<div class="sd-entry-thumb">
									<figure>
											<img src="<?php echo BASE_URL;?><?php echo $newss['news_image']?>" class=""/>
									</figure>
								</div>
								<div class="sd-entry-content">
									<div class="sd-content-wrap">
										<h3 class="sd-entry-title"><a href="<?php echo BASE_URL;?>news/<?php echo $pages[0]?>/<?php echo $newss['news_slug']?>" title="Permalink la Gone to Ghana – volunteering in Africa" rel="bookmark">  <?php echo $newss['news_name']?></a>
										</h3>
										<span class="sd-latest-blog-date"><?php echo date("F d, Y", strtotime($newss['news_date']))?></span> 
										<span class="sd-latest-author-date"></span>
									</div>
								</div>
								<p class="sd-latest-blog-excerpt"><?php echo substr($newss['news_description'], 0,70) ?></p>
								<a class="sd-more sd-link-trans" href="<?php echo BASE_URL;?>news/<?php echo $pages[0]?>/<?php echo $newss['news_slug']?>" title="Gone to Ghana – volunteering in Africa">READ MORE</a>
							</div>
						</div>
					<?php }?>
				</div>
			</div>
		</div>
	</div>
<?php } else {?>
	
<div class="container sd-blog-page">
	<div class="row"> 
		<div class="col-md-8 ">
			<div class="sd-left-col">
				<article> 		
					<div class="sd-entry-wrapper clearfix">
						<div class="sd-entry-thumb">
							<figure>
								<img src="<?php echo BASE_URL;?><?php echo $news_singles['news_image']?>" alt="" width="100%" />	
							</figure>
						</div>
						<header>
							<h2 class="sd-entry-title"><?php echo $news_singles['news_name']?></h2>
							<aside class="sd-entry-meta clearfix">
								<ul>
									<li class="sd-meta-author">
										<i class="fa fa-calendar"></i><?php echo date("F d, Y", strtotime($news_singles['news_date']))?>		
									</li>
									<!-- <li class="sd-meta-author">
										<i class="fa fa-user"></i>skat			
									</li>
									<li class="sd-meta-category">
										<i class="fa fa-folder"></i>
										<a href="#" rel="category tag">VOLUNTEERS</a>			
									</li>
					
									<li class="meta-tag"> <i class="fa fa-tag"></i> 
										<a href="../tag/africa/index.html" rel="tag">AFRICA</a> , 
										<a href="../tag/crisis/index.html" rel="tag">CRISIS</a>
									</li>				
									<li class="sd-meta-comments">
										<i class="fa fa-comment"></i>
										<a href="index.html#respond" class="comments-link" >0 comments</a>			
									</li> -->
								</ul>
							</aside>
						</header>
						<div class="sd-entry-content">
							<p><?php echo nl2br($news_singles['news_description']);?></p>
						</div>
					</div>

					<div class="sd-related-posts">
						<h3><!-- RELATED POSTS --></h3>
						<div class="row"></div>
					</div>
				</article>
				<!-- <div class="sd-author-box clearfix">
					<div class="sd-author-photo">
						<img alt='' src='http://2.gravatar.com/avatar/edc9957973a9481907cce53d42b5bc1e?s=165&amp;d=mm&amp;r=g' srcset='http://2.gravatar.com/avatar/edc9957973a9481907cce53d42b5bc1e?s=330&amp;d=mm&amp;r=g 2x' class='avatar avatar-165 photo' height='165' width='165' />			
					</div>
					<div class="sd-author-bio">
					<h4>
						skat <br/>
					</h4>
					<ul>
						<li class="sd-author-website">
							<a class="sd-bg-trans sd-link-trans" href="http://skat.tf/" target="_blank" title="Website">
								<i class="fa fa-link"></i>
							</a>
						</li>
						<li class="sd-author-facebook">
							<a class="sd-link-trans" href="http://facebook.com/skatdesign" target="_blank" title="Facebook"><i class="fa fa-facebook"></i></a>
						</li>
						<li class="sd-author-twitter">
							<a class="sd-link-trans" href="http://twitter.com/skatdesign" target="_blank" title="Twitter"><i class="fa fa-twitter"></i></a>
						</li>
						<li class="sd-author-googleplus">
							<a class="sd-link-trans" href="https://plus.google.com/+SkatDesign/posts" target="_blank" title="Google Plus"><i class="fa fa-google-plus"></i> </a>
						</li>
									
					</ul>
					<p>In hac habitasse platea dictumst. Cras sit amet est eget dui viverra scelerisque. Duis semper pulvinar dui, nec mollis libero tincidunt quis.</p>
					<a class="sd-more sd-author-posts" href="../author/skat/index.html" title="All posts by author">VIEW ALL POSTS</a>
				</div>
			</div> -->
									<!-- You can start editing here. -->
<!-- <div id="comments" class="sd-comments-wrapper">


	<div id="respond" class="comment-respond">
		<h3 id="reply-title" class="comment-reply-title"><span>Leave a reply</span> <small><a rel="nofollow" id="cancel-comment-reply-link" href="index.html#respond" style="display:none;">Cancel reply</a></small></h3>			<form action="http://helpinghands1.skat.tf/wp-comments-post.php" method="post" id="commentform" class="comment-form">
				<p class="comment-notes"><span id="email-notes">Your email address will not be published.</span> Required fields are marked <span class="required">*</span></p><div class="sd-respond-textarea"><p><textarea id="comment" name="comment" aria-required="true" cols="58" rows="10" tabindex="4" placeholder="Comments*"></textarea></p></div><div class="sd-respond-inputs clearfix"><p><input name="author" type="text" placeholder="Name*" size="30" aria-required="true" /></p>
<p><input name="email" type="email" size="30" aria-required="true" placeholder="E-mail*"></p>
<p class="sd-last-input"><input name="url" type="url" size="30" placeholder="Website" /></p></div>
<p class="form-submit"><input name="submit" type="submit" id="submit" class="sd-submit-comments sd-opacity-trans" value="Submit Comment" /> <input type='hidden' name='comment_post_ID' value='233' id='comment_post_ID' />
<input type='hidden' name='comment_parent' id='comment_parent' value='0' />
</p><p style="display: none;"><input type="hidden" id="akismet_comment_nonce" name="akismet_comment_nonce" value="b27db31ab6" /></p><p style="display: none;"><input type="hidden" id="ak_js" name="ak_js" value="121"/></p>			</form>
			</div>
	</div> -->							
			</div>
		</div>
		<!-- col-md-8 --> 
		<div class="col-md-4">
				
			<div class="sd-right-col">
				

				<aside id="categories-2" class="sd-sidebar-widget clearfix widget_categories">
					<h3 class="sd-sidebar-widget-title">Categories</h3>		
					<ul>
						<?php $news_datas = $db->db_get_array("SELECT * FROM ".TABLE_NEWS_CATEGORY." WHERE news_category_status='1' ");
                            foreach ($news_datas as $news_cats) {?>
                            <li class="cat-item cat-item-6">
								<a href="<?php echo BASE_URL;?>news/<?php echo $news_cat['news_cat_slug']?>" ><?php echo $news_cat['news_category_name']?></a>
							</li>
                        <?php }?>
					</ul>
				</aside>
				<aside id="sd_recent_posts_widget-1" class="sd-sidebar-widget clearfix sd_recent_posts_widget">
					<h3 class="sd-sidebar-widget-title">Recent News</h3>
					<div class="sd-recent-posts-widget">
						<ul>
							<?php $news_red = $db->db_get_array("SELECT * FROM ".TABLE_NEWS." WHERE news_status = 1 AND news_slug !='".$pages[1]."' ORDER BY news_id ASC limit 2");
								foreach ($news_red as $news_reds) {?>
								<li class="clearfix"> 
									<div class="sd-recent-widget-thumb">
										<figure>
											<img src="<?php echo BASE_URL;?><?php echo $news_reds['news_image']?>" class="attachment-sd-recent-blog-widget size-sd-recent-blog-widget wp-post-image" alt="" />
										</figure>
									</div>
								
									<div class="sd-recent-posts-content">
										<h4><a href="<?php echo BASE_URL;?>news/<?php echo $pages[0]?>/<?php echo $news_reds['news_slug']?>" title="<?php echo $news_reds['news_name']?>">
										<?php echo $news_reds['news_name']?>					</a> </h4>
										<span class="sd-recent-date"> <i class="fa fa-calendar"></i> <?php echo date("F d, Y", strtotime($news_reds['news_date']))?> </span>
									</div>
								</li>
							<?php }?>
							
						</ul>
					</div>
				</aside>
			</div>		
		</div>
	</div>
	<!-- row -->
</div>
<?php }?>
<br><br>
	<!-- sd-full-width -->
